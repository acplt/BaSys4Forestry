from basyx.aas.model import Key, datatypes, KeyTypes, GlobalReference, Qualifier

# Namen
NOTIZ = "Notiz"

# Kontakt
KONTAKT = "Kontakt"
FIRMENNAME = "Firmenname"
ANREDE = "Anrede"
VORNAME = "Vorname"
NACHNAME = "Nachname"
ADRESSE = "Adresse"
UMSATZBESTEUERUNG = "Umsatzbesteuerung"
IBAN = "IBAN"
STEUERNUMMER = "Steuernummer"
TELEFONNUMMERN = "Telefonnummern"
TELEFON = "Telefon"
EMAILADRESSEN = "Emailadressen"
EMAIL = "Email"

# Geoklassen
UMRING = "Umring"
NAME = "Name"
BESCHREIBUNG = "Beschreibung"
KOORDINATEN = "Koordinaten"
STANDORT = "Standort"
KOORDINATE = "Koordinate"
X = "x"
Y = "y"
ALT = "alt"
CRS = "crs"

# Preismatrix
PREISMATRIX = "Preismatrix"
PREISMATRIXEINTRAG = "Preismatrixeintrag"
PREIS = "Preis"
MITTENDURCHMESSER_VON = "MittendurchmesserVon"
MITTENDURCHMESSER_BIS = "MittendurchmesserBis"
GUETEKLASSE = "Gueteklasse"
GUETEKLASSE_EINZELSTAMM = "GueteklasseEintzelstamm"
GUETEKLASSE_VON_PREISMATRIX = "GueteklasseVon"
GUETEKLASSE_BIS_PREISMATRIX = "GueteklasseBis"

# Kopfdaten
KOPFDATEN = "Kopfdaten"
PROJEKT = "Projekt"
HIEB = "Hieb"
ERNTE = "Ernte"
LIEFERANT = "Lieferant"
KAEUFER = "Kaeufer"
FUHRMANN = "Fuhrmann"

# Messergebnisse
RAUMMASS_ENERGIEHOLZ = "RaummassEnergieholz"
MENGE = "Menge"
STAMMLAENGE = "Stammlaenge"
RAUMMASS_INDUSTRIEHOLZ = "RaummassIndustrieholz"
STAMMANZAHL = "Stammanzahl"
KLAMMERSTAMMABSCHNITTSNUMMER = "Klammerstammabschnittsnummer"
EINZELSTAMM = "Einzelstamm"
EINZELSTAEMME = "Einzelstaemme"
MITTENDURCHMESSER = "Mittendurchmesser"
STAMMNUMMER = "Stammnummer"
EINZELSTAMM_LISTE = "EinzelstammListe"

# Polter
POLTER_LISTE = "PolterListe"
POLTER = "Polter"
POLTERNUMMER = "Polternummer"
POLTERSTATUS = "Polterstatus"
VERMESSUNGSVERFAHREN = "Vermessungsverfahren"
FOTOS = "Fotos"
FOTO = "Foto"
VIDEOS = "Videos"
VIDEO = "Video"
MESSERGEBNIS_EINZELSTAMMLISTE = "Messergebnis_EinzelstammListe"
MESSERGEBNIS_RAUMMASSINDUSTRIEHOLZ = "Messergebnis_RaummassIndustrieholz"
MESSERGEBNIS_RAUMMASSENERGIEHOLZ = "Messergebnis_RaummassEnergieholz"

# Stehenderbaum
STEHENDER_BAUM = "StehenderBaum"
BHD = "bhd"
HOEHE = "Hoehe"
SORTIMENTSTYP = "Sortimentstyp"
SORTIMENTSTYP_OPTIONAL = "Sortimentstyp"
STEHENDER_BAUM_BAUMART = "StehenderBaumBaumart"

BAUMART = "Baumart"

# Holzliste
HOLZLISTE = "Holzliste"
HOLZART = "Holzart"
SORTE = "Sorte"
SORTE_OPTIONAL = "Sorte"

# Verkaufslos
HOLZLISTEN = "Holzlisten"
HOLZLISTE_REF = "HolzlisteReference"
HOLZLISTE_REF_IN_HOLZLISTEN = "HolzlisteRefInHolzlisten"
VERKAUFSLOS = "Verkaufslos"
VERKAUFSSTATUS = "Verkaufsstatus"
ABRECHNUNGSMASS = "Abrechnungsmass"
BEREITSTELLUNGSART = "Bereitstellungsart"
VERFUEGBAR_AB = "VerfuegbarAb"
VERFUEGBAR_BIS = "VerfuegbarBis"
VERKAEUFER = "Verkaeufer"

# Holzpreisbereich
HOLZPREISBEREICH = "Holzpreisbereich"
PREIS_VON = "PreisVon"
PREIS_BIS = "PreisBis"
HOLZPREISBEREICH_BAUMART = "HolzpreisbereichBaumart"
HOLZPREISBEREICH_SORTE = "HolzpreisbereichSorte"
HOLZLISTE_REF_IN_HOLZPREISBEREICH = "HolzlisteRefInHolzpreisbereich"
DATUM = "Datum"
QUELLE = "Quelle"

# Sortiment
SORTIMENT = "Sortiment"
NUMMER = "Nummer"
GUETEKLASSE_VON = "GueteklasseVon"
GUETEKLASSE_BIS = "GueteklasseBis"
LAENGE_VON = "LaengeVon"
LAENGE_BIS = "LaengeBis"
MINDESTZOPF = "Mindestzopf"
MAX_DURCHMESSER = "MaxDurchmesser"
LAENGENZUGABE = "Laengenzugabe"
MENGENSCHAETZUNG = "Mengenschaetzung"
BEMERKUNG = "Bemerkung"
ERGEBNIS = "Ergebnis"
HOLZLISTE_REF_IN_ERGEBNIS = "HolzlisteReference"

# ZuFaellendeBaeume
ZU_FAELLENDE_BAEUME = "ZuFaellendeBaeume"
POSITION = "Position"
BAUMLISTE = "Baumliste"

# Holzpreisbereiche
HOLZPREISBEREICHE = "Holzpreisbereiche"

# Bestandesdaten
BESTANDESDATEN = "Bestandesdaten"
BAUMARTEN = "Baumarten"
BESTANDESDATEN_BAUMART = "BestandesdatenBaumart"

# Beobachtung
BEOBACHTUNG = "Beobachtung"

# Waldweg
WALDWEG = "Waldweg"
ERSTELLUNGSDATUM = "Erstellungsdatum"
AKTUALISIERUNGSDATUM = "Aktualisierungsdatum"
BREITE = "Breite"
WEGPUNKTE = "Wegpunkte"
WENDEPUNKTE = "Wendepunkte"

# Arbeitsauftrag
ARBEITSAUFTRAG = "Arbeitsauftrag"
HIEBSNUMMER = "Hiebsnummer"
KURZBESCHREIBUNG = "Kurzbeschreibung"
ANSPRECHPARTNER = "Ansprechpartner"
UNTERNEHMER_HOLZERNTE = "UnternehmerHolzernte"
UNTERNEHMER_HOLZRUECKUNG = "UnternehmerHolzrueckung"
WALDBESITZER = "Waldbesitzer"
SICHERHEITSHINWEISE = "Sicherheitshinweise"
NATURSCHUTZHINWEISE = "Naturschutzhinweise"
KARTE = "Karte"
AUFTRAGSSTATUS = "Auftragsstatus"
SORTIMENTE = "Sortimente"
ZU_FAELLENDE_BAEUME_REF = "ZuFaellendeBaeumeRefefence"

# DZ_WALD
DZ_WALD = "dz_wald"

# TODO: alle None semantic ids mit Werten setzen
# Semantic IDs

SEMANTIC_ID = "semantic_id"
DESCRIPTION = "description"
VALUE_TYPE = "value_type"
ID_SHORT = "id_short"
IDENTIFICATION = "identification"
QUALIFIER = "qualifier"

B4F_SEMANTIC_ID_BASE = "basys4forestry.de/"
ELDAT_SEMANTIC_ID_BASE = "eldatstandard.de/dokumentation/1.0.2/"


def b4f_semantic_id_ref(name: str):
    return GlobalReference(key=(Key(KeyTypes.GLOBAL_REFERENCE, f"{B4F_SEMANTIC_ID_BASE}{name}"),))


def eldat_semantic_id_ref(name: str):
    return GlobalReference(key=(Key(KeyTypes.GLOBAL_REFERENCE, f"{ELDAT_SEMANTIC_ID_BASE}{name}"),))


def eclass_semantic_id_ref(name: str):
    return GlobalReference(key=(Key(KeyTypes.GLOBAL_REFERENCE, f"{ELDAT_SEMANTIC_ID_BASE}{name}"),))


def mandatory_qualifier():
    return Qualifier(type_="Cardinality", value_type=str, value="One")
def optional_qualifier():
    return Qualifier(type_="Cardinality", value_type=str, value="ZeroToOne")
def iterable_mandatory_qualifier():
    return Qualifier(type_="Cardinality", value_type=str, value="OneToMany")
def iterable_optional_qualifier():
    return Qualifier(type_="Cardinality", value_type=str, value="ZeroToMany")

KWARGS = {
    KONTAKT: {
        # ID_SHORT will be that of the provided KONTAKTROLLE
        # ID_SHORT: KONTAKT,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([]),
    },
    FIRMENNAME: {
        ID_SHORT: FIRMENNAME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    ANREDE: {
        ID_SHORT: ANREDE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    VORNAME: {
        ID_SHORT: VORNAME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    NACHNAME: {
        ID_SHORT: NACHNAME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    ADRESSE: {
        ID_SHORT: ADRESSE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    TELEFONNUMMERN: {
        ID_SHORT: TELEFONNUMMERN,
        DESCRIPTION: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    TELEFON: {
        ID_SHORT: TELEFON,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    EMAILADRESSEN: {
        ID_SHORT: EMAILADRESSEN,
        DESCRIPTION: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    EMAIL: {
        ID_SHORT: EMAIL,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    UMSATZBESTEUERUNG: {
        ID_SHORT: UMSATZBESTEUERUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    IBAN: {
        ID_SHORT: IBAN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    STEUERNUMMER: {
        ID_SHORT: STEUERNUMMER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Geoklassen
    UMRING: {
        ID_SHORT: UMRING,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    NAME: {
        ID_SHORT: NAME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    BESCHREIBUNG: {
        ID_SHORT: BESCHREIBUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    KOORDINATEN: {
        ID_SHORT: KOORDINATEN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    STANDORT: {
        ID_SHORT: STANDORT,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    KOORDINATE: {
        ID_SHORT: KOORDINATE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_mandatory_qualifier()]),
    },
    X: {
        ID_SHORT: X,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    Y: {
        ID_SHORT: Y,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    ALT: {
        ID_SHORT: ALT,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    CRS: {
        ID_SHORT: CRS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    NOTIZ: {
        ID_SHORT: NOTIZ,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Preismatrix
    PREISMATRIXEINTRAG: {
        ID_SHORT: PREISMATRIXEINTRAG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    PREIS: {
        ID_SHORT: PREIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    MITTENDURCHMESSER_VON: {
        ID_SHORT: MITTENDURCHMESSER_VON,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    MITTENDURCHMESSER_BIS: {
        ID_SHORT: MITTENDURCHMESSER_BIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    GUETEKLASSE: {
        ID_SHORT: GUETEKLASSE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    GUETEKLASSE_EINZELSTAMM: {
        ID_SHORT: GUETEKLASSE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    GUETEKLASSE_VON_PREISMATRIX: {
        ID_SHORT: GUETEKLASSE_VON_PREISMATRIX,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    GUETEKLASSE_BIS_PREISMATRIX: {
        ID_SHORT: GUETEKLASSE_BIS_PREISMATRIX,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    PREISMATRIX: {
        ID_SHORT: PREISMATRIX,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Kopfdaten
    KOPFDATEN: {
        ID_SHORT: KOPFDATEN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    PROJEKT: {
        ID_SHORT: PROJEKT,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    HIEB: {
        ID_SHORT: HIEB,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    ERNTE: {
        ID_SHORT: ERNTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: datatypes.Date,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    LIEFERANT: {
        ID_SHORT: LIEFERANT,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    KAEUFER: {
        ID_SHORT: KAEUFER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    FUHRMANN: {
        ID_SHORT: FUHRMANN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Messergebnisse
    RAUMMASS_ENERGIEHOLZ: {
        ID_SHORT: RAUMMASS_ENERGIEHOLZ,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    RAUMMASS_INDUSTRIEHOLZ: {
        ID_SHORT: RAUMMASS_INDUSTRIEHOLZ,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    MENGE: {
        ID_SHORT: MENGE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    STAMMLAENGE: {
        ID_SHORT: STAMMLAENGE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    STAMMANZAHL: {
        ID_SHORT: STAMMANZAHL,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: int,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    KLAMMERSTAMMABSCHNITTSNUMMER: {
        ID_SHORT: KLAMMERSTAMMABSCHNITTSNUMMER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    MITTENDURCHMESSER: {
        ID_SHORT: MITTENDURCHMESSER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    EINZELSTAEMME: {
        ID_SHORT: EINZELSTAEMME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    EINZELSTAMM: {
        ID_SHORT: EINZELSTAMM,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_mandatory_qualifier()]),
    },
    STAMMNUMMER: {
        ID_SHORT: STAMMNUMMER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    EINZELSTAMM_LISTE: {
        ID_SHORT: EINZELSTAMM_LISTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Polter
    POLTER_LISTE: {
        ID_SHORT: POLTER_LISTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    POLTER: {
        ID_SHORT: POLTER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_mandatory_qualifier()]),
    },
    POLTERNUMMER: {
        ID_SHORT: POLTERNUMMER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    POLTERSTATUS: {
        ID_SHORT: POLTERSTATUS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    VERMESSUNGSVERFAHREN: {
        ID_SHORT: VERMESSUNGSVERFAHREN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    FOTOS: {
        ID_SHORT: FOTOS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    FOTO: {
        ID_SHORT: FOTO,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    VIDEOS: {
        ID_SHORT: VIDEOS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    VIDEO: {
        ID_SHORT: VIDEO,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },

    # Stehenderbaum
    STEHENDER_BAUM: {
        ID_SHORT: STEHENDER_BAUM,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    BHD: {
        ID_SHORT: BHD,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    HOEHE: {
        ID_SHORT: HOEHE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    STEHENDER_BAUM_BAUMART: {
        ID_SHORT: BAUMART,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    SORTIMENTSTYP: {
        ID_SHORT: SORTIMENTSTYP,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    SORTIMENTSTYP_OPTIONAL: {
        ID_SHORT: SORTIMENTSTYP,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Holzliste
    HOLZLISTE: {
        ID_SHORT: HOLZLISTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },
    HOLZART: {
        ID_SHORT: HOLZART,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    SORTE: {
        ID_SHORT: SORTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    SORTE_OPTIONAL: {
        ID_SHORT: SORTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    HOLZPREISBEREICH_BAUMART: {
        ID_SHORT: BAUMART,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    HOLZPREISBEREICH_SORTE: {
        ID_SHORT: SORTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Verkaufslos
    VERKAUFSLOS: {
        ID_SHORT: VERKAUFSLOS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },
    HOLZLISTEN: {
        ID_SHORT: HOLZLISTEN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    HOLZLISTE_REF_IN_HOLZLISTEN: {
        ID_SHORT: HOLZLISTE_REF,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    VERKAUFSSTATUS: {
        ID_SHORT: VERKAUFSSTATUS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: int,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    ABRECHNUNGSMASS: {
        ID_SHORT: ABRECHNUNGSMASS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    BEREITSTELLUNGSART: {
        ID_SHORT: BEREITSTELLUNGSART,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    VERFUEGBAR_AB: {
        ID_SHORT: VERFUEGBAR_AB,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: datatypes.Date,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    VERFUEGBAR_BIS: {
        ID_SHORT: VERFUEGBAR_BIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: datatypes.Date,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    VERKAEUFER: {
        ID_SHORT: VERKAEUFER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Holzpreisbereich
    HOLZPREISBEREICH: {
        ID_SHORT: HOLZPREISBEREICH,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    PREIS_VON: {
        ID_SHORT: PREIS_VON,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    PREIS_BIS: {
        ID_SHORT: PREIS_BIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    HOLZLISTE_REF_IN_HOLZPREISBEREICH: {
        ID_SHORT: HOLZLISTE_REF,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    DATUM: {
        ID_SHORT: DATUM,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: datatypes.Date,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    QUELLE: {
        ID_SHORT: QUELLE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Sortiment
    SORTIMENT: {
        ID_SHORT: SORTIMENT,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    NUMMER: {
        ID_SHORT: NUMMER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: int,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    GUETEKLASSE_VON: {
        ID_SHORT: GUETEKLASSE_VON,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    GUETEKLASSE_BIS: {
        ID_SHORT: GUETEKLASSE_BIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    LAENGE_VON: {
        ID_SHORT: LAENGE_VON,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    LAENGE_BIS: {
        ID_SHORT: LAENGE_BIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    MINDESTZOPF: {
        ID_SHORT: MINDESTZOPF,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    MAX_DURCHMESSER: {
        ID_SHORT: MAX_DURCHMESSER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    LAENGENZUGABE: {
        ID_SHORT: LAENGENZUGABE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    MENGENSCHAETZUNG: {
        ID_SHORT: MENGENSCHAETZUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    BEMERKUNG: {
        ID_SHORT: BEMERKUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    ERGEBNIS: {
        ID_SHORT: ERGEBNIS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    HOLZLISTE_REF_IN_ERGEBNIS: {
        ID_SHORT: HOLZLISTE_REF,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },

    # ZuFaellendeBaeume
    ZU_FAELLENDE_BAEUME: {
        ID_SHORT: ZU_FAELLENDE_BAEUME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },
    POSITION: {
        ID_SHORT: POSITION,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    BAUMLISTE: {
        ID_SHORT: BAUMLISTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Holzpreisbereiche
    HOLZPREISBEREICHE: {
        ID_SHORT: HOLZPREISBEREICHE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },

    # Bestandesdaten
    BESTANDESDATEN: {
        ID_SHORT: BESTANDESDATEN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },
    BAUMARTEN: {
        ID_SHORT: BAUMARTEN,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    BESTANDESDATEN_BAUMART: {
        ID_SHORT: BAUMART,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([iterable_optional_qualifier()]),
    },
    WALDBESITZER: {
        ID_SHORT: WALDBESITZER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Beobachtung
    BEOBACHTUNG: {
        ID_SHORT: BEOBACHTUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },

    # Waldweg
    WALDWEG: {
        ID_SHORT: WALDWEG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },
    ERSTELLUNGSDATUM: {
        ID_SHORT: ERSTELLUNGSDATUM,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: datatypes.Date,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    AKTUALISIERUNGSDATUM: {
        ID_SHORT: AKTUALISIERUNGSDATUM,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: datatypes.Date,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    BREITE: {
        ID_SHORT: BREITE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: float,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    WEGPUNKTE: {
        ID_SHORT: WEGPUNKTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    WENDEPUNKTE: {
        ID_SHORT: WENDEPUNKTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },

    # Arbeitsauftrag
    ARBEITSAUFTRAG: {
        ID_SHORT: ARBEITSAUFTRAG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
    },
    HIEBSNUMMER: {
        ID_SHORT: HIEBSNUMMER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    KURZBESCHREIBUNG: {
        ID_SHORT: KURZBESCHREIBUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    ANSPRECHPARTNER: {
        ID_SHORT: ANSPRECHPARTNER,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([mandatory_qualifier()]),
    },
    UNTERNEHMER_HOLZERNTE: {
        ID_SHORT: UNTERNEHMER_HOLZERNTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    UNTERNEHMER_HOLZRUECKUNG: {
        ID_SHORT: UNTERNEHMER_HOLZRUECKUNG,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    SICHERHEITSHINWEISE: {
        ID_SHORT: SICHERHEITSHINWEISE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    NATURSCHUTZHINWEISE: {
        ID_SHORT: NATURSCHUTZHINWEISE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    KARTE: {
        ID_SHORT: KARTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    AUFTRAGSSTATUS: {
        ID_SHORT: AUFTRAGSSTATUS,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        VALUE_TYPE: str,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    SORTIMENTE: {
        ID_SHORT: SORTIMENTE,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    ZU_FAELLENDE_BAEUME_REF: {
        ID_SHORT: ZU_FAELLENDE_BAEUME,
        DESCRIPTION: None,
        SEMANTIC_ID: None,
        QUALIFIER: tuple([optional_qualifier()]),
    },
    DZ_WALD: {
        ID_SHORT: DZ_WALD,
        DESCRIPTION: None,
    },
}
