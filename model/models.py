from copy import deepcopy
from typing import Optional, Iterable, Tuple
from abc import ABC, abstractmethod
from datetime import date
import os, sys, inspect

from basyx.aas.model import Reference, ModelReference, SubmodelElementList, SubmodelElementCollection, Property, Range, Submodel, \
    Identifier, ReferenceElement, AssetAdministrationShell, AssetInformation

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from model import enums
from model.configs import *
from model.utils import find_next_free_id_short, init_items_and_add_to_se_list, init_se_list, \
    add_items_to_se_list

SubmodelElementCollectionUnordered = SubmodelElementCollection


class Kontakt(SubmodelElementCollection):
    def __init__(self,
                 kontaktrolle: enums.Kontaktrolle,
                 firmenname: Optional[str] = None,
                 anrede: Optional[str] = None,
                 vorname: Optional[str] = None,
                 nachname: Optional[str] = None,
                 adresse: Optional[str] = None,
                 telefonnummern: Optional[Iterable[str]] = None,
                 emailadressen: Optional[Iterable[str]] = None,
                 umsatzbesteuerung: Optional[enums.Umsatzbesteuerung] = enums.OptionalEnum,
                 iban: Optional[str] = None,
                 steuernummer: Optional[str] = None
                 ):
        super().__init__(**deepcopy(KWARGS[kontaktrolle.value]))

        for i in (
                Property(value=firmenname, **deepcopy(KWARGS[FIRMENNAME])),
                Property(value=anrede, **deepcopy(KWARGS[ANREDE])),
                Property(value=vorname, **deepcopy(KWARGS[VORNAME])),
                Property(value=nachname, **deepcopy(KWARGS[NACHNAME])),
                Property(value=adresse, **deepcopy(KWARGS[ADRESSE])),
                Property(value=umsatzbesteuerung.value, **deepcopy(KWARGS[UMSATZBESTEUERUNG])),
                Property(value=iban, **deepcopy(KWARGS[IBAN])),
                Property(value=steuernummer, **deepcopy(KWARGS[STEUERNUMMER])),
                init_se_list(se_kwargs=deepcopy(KWARGS[TELEFONNUMMERN]), item_kwargs=deepcopy(KWARGS[TELEFON]), type_value_list_element=Property),
                init_se_list(se_kwargs=deepcopy(KWARGS[EMAILADRESSEN]), item_kwargs=deepcopy(KWARGS[EMAIL]), type_value_list_element=Property)
        ):
            self.add_referable(i)
        self.add_telefone(telefonnummern)
        self.add_emails(emailadressen)

    def add_telefone(self, values: Iterable[str]):
        se_list = self.get_referable(deepcopy(KWARGS[TELEFONNUMMERN])[ID_SHORT])
        init_items_and_add_to_se_list(se_list=se_list, values_of_items=values, item_kwargs=deepcopy(KWARGS[TELEFON]),
                                      item_type=Property)

    def add_emails(self, values: Iterable[str]):
        se_list = self.get_referable(deepcopy(KWARGS[EMAILADRESSEN])[ID_SHORT])
        init_items_and_add_to_se_list(se_list=se_list, values_of_items=values, item_kwargs=deepcopy(KWARGS[EMAIL]),
                                      item_type=Property)


class Koordinate(SubmodelElementCollection):
    def __init__(self,
                 x: Optional[float] = None,
                 y: Optional[float] = None,
                 alt: Optional[float] = None,
                 crs: Optional[enums.CRS] = None,
                 ):
        super().__init__(**deepcopy(KWARGS[KOORDINATE]))
        for i in (
                Property(value=x, **deepcopy(KWARGS[X])),
                Property(value=y, **deepcopy(KWARGS[Y])),
                Property(value=alt, **deepcopy(KWARGS[ALT])),
                Property(value=crs, **deepcopy(KWARGS[CRS])),
        ):
            self.add_referable(i)


class Standort(SubmodelElementCollection):
    def __init__(self,
                 koordinate: Koordinate,
                 notiz: Optional[str] = None,
                 ):
        super().__init__(**deepcopy(KWARGS[STANDORT]))
        self.add_referable(koordinate)
        self.add_referable(Property(value=notiz, **deepcopy(KWARGS[NOTIZ])))


class Umring(SubmodelElementCollection):
    def __init__(self,
                 name: str,
                 beschreibung: str,
                 koordinaten: Iterable[Koordinate],
                 ):
        super().__init__(**deepcopy(KWARGS[UMRING]))
        if len(koordinaten) < 3:
            raise Exception(f"At least 3 coordinates are needed. Only {len(koordinaten)} provided.")
        self.add_referable(Property(value=name, **deepcopy(KWARGS[NAME])))
        self.add_referable(Property(value=beschreibung, **deepcopy(KWARGS[BESCHREIBUNG])))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[KOORDINATEN]), item_kwargs=deepcopy(KWARGS[KOORDINATE]), type_value_list_element=SubmodelElementCollection))
        self.add_koordinaten(koordinaten)

    def add_koordinaten(self, values: Iterable[Koordinate]):
        se_list: SubmodelElementList = self.get_referable(deepcopy(KWARGS[KOORDINATEN])[ID_SHORT])
        for i, val in enumerate(values):
            val.id_short = find_next_free_id_short(collection=se_list,
                                                   id_short_base=deepcopy(KWARGS[KOORDINATE])[ID_SHORT])
            se_list.add_referable(val)


class Preismatrixeintrag(SubmodelElementCollection):
    def __init__(self,
                 preis: float,
                 mittendurchmesser_von: Optional[float] = None,
                 mittendurchmesser_bis: Optional[float] = None,
                 gueteklasse_von: Optional[enums.Gueteklasse] = enums.OptionalEnum,
                 gueteklasse_bis: Optional[enums.Gueteklasse] = enums.OptionalEnum,
                 sortimentstyp: Optional[enums.Sortimentstyp] = enums.OptionalEnum,
                 sorte: Optional[enums.Sorte] = enums.OptionalEnum
                 ):
        super().__init__(**deepcopy(KWARGS[PREISMATRIXEINTRAG]))
        self.add_referable(Property(value=preis, **deepcopy(KWARGS[PREIS])))
        self.add_referable(Property(value=mittendurchmesser_von, **deepcopy(KWARGS[MITTENDURCHMESSER_VON])))
        self.add_referable(Property(value=mittendurchmesser_bis, **deepcopy(KWARGS[MITTENDURCHMESSER_BIS])))
        self.add_referable(Property(value=gueteklasse_von.value, **deepcopy(KWARGS[GUETEKLASSE_VON_PREISMATRIX])))
        self.add_referable(Property(value=gueteklasse_bis.value, **deepcopy(KWARGS[GUETEKLASSE_BIS_PREISMATRIX])))
        self.add_referable(Property(value=sortimentstyp.value, **deepcopy(KWARGS[SORTIMENTSTYP_OPTIONAL])))
        self.add_referable(Property(value=sorte.value, **deepcopy(KWARGS[SORTE_OPTIONAL])))


class Kopfdaten(SubmodelElementCollection):
    def __init__(self,
                 projekt: Optional[str] = None,
                 hieb: Optional[enums.Hieb] = enums.OptionalEnum,
                 ernte: Optional[Tuple[date, date]] = None,
                 lieferant: Optional[Kontakt] = None,
                 kaeufer: Optional[Kontakt] = None,
                 fuhrmann: Optional[Kontakt] = None,
                 notiz: Optional[str] = None
                 ):
        super().__init__(**deepcopy(KWARGS[KOPFDATEN]))
        if ernte:
            if len(ernte) != 2:
                raise Exception("Expected a tuple of size 2 for 'ernte'.")
            self.add_referable(Range(min=ernte[0], max=ernte[1], **deepcopy(KWARGS[ERNTE])))
        else:
            self.add_referable(Range(**deepcopy(KWARGS[ERNTE])))
        if lieferant:
            lieferant.id_short = LIEFERANT
            self.add_referable(lieferant)
        if kaeufer:
            kaeufer.id_short = KAEUFER
            self.add_referable(kaeufer)
        if fuhrmann:
            fuhrmann.id_short = FUHRMANN
            self.add_referable(fuhrmann)
        self.add_referable(Property(value=projekt, **deepcopy(KWARGS[PROJEKT])))
        self.add_referable(Property(value=hieb.value, **deepcopy(KWARGS[HIEB])))
        self.add_referable(Property(value=notiz, **deepcopy(KWARGS[NOTIZ])))


class RaummassEnergieholz(SubmodelElementCollection):
    def __init__(self,
                 menge: float,
                 stammlaenge: float,
                 ):
        super().__init__(**deepcopy(KWARGS[RAUMMASS_ENERGIEHOLZ]))
        self.add_referable(Property(value=menge, **deepcopy(KWARGS[MENGE])))
        self.add_referable(Property(value=stammlaenge, **deepcopy(KWARGS[STAMMLAENGE])))


class RaummassIndusrieeholz(SubmodelElementCollection):
    def __init__(self,
                 menge: float,
                 stammlaenge: float,
                 stammanzahl: Optional[int] = None,
                 ):
        super().__init__(**deepcopy(KWARGS[RAUMMASS_INDUSTRIEHOLZ]))
        self.add_referable(Property(value=menge, **deepcopy(KWARGS[MENGE])))
        self.add_referable(Property(value=stammlaenge, **deepcopy(KWARGS[STAMMLAENGE])))
        self.add_referable(Property(value=stammanzahl, **deepcopy(KWARGS[STAMMANZAHL])))


class Einzelstamm(SubmodelElementCollection):
    def __init__(self,
                 menge: float,
                 stammlaenge: float,
                 stammnummer: str,
                 mittendurchmesser: float,
                 gueteklasse: Optional[enums.Gueteklasse] = enums.OptionalEnum,
                 klammerstammabschnittsnummer: Optional[str] = None,
                 ):
        super().__init__(**deepcopy(KWARGS[EINZELSTAMM]))
        self.add_referable(Property(value=menge, **deepcopy(KWARGS[MENGE])))
        self.add_referable(Property(value=stammlaenge, **deepcopy(KWARGS[STAMMLAENGE])))
        self.add_referable(Property(value=stammnummer, **deepcopy(KWARGS[STAMMNUMMER])))
        self.add_referable(Property(value=mittendurchmesser, **deepcopy(KWARGS[MITTENDURCHMESSER])))
        self.add_referable(Property(value=gueteklasse.value, **deepcopy(KWARGS[GUETEKLASSE_EINZELSTAMM])))
        self.add_referable(Property(value=klammerstammabschnittsnummer, **deepcopy(KWARGS[KLAMMERSTAMMABSCHNITTSNUMMER])))


class EinzelstammListe(SubmodelElementCollection):
    def __init__(self,
                 einzelstaeme: Iterable[Einzelstamm],
                 ):
        super().__init__(**deepcopy(KWARGS[EINZELSTAMM_LISTE]))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[EINZELSTAEMME]), item_kwargs=deepcopy(KWARGS[EINZELSTAMM]), type_value_list_element=SubmodelElementCollection))
        self.add_einzelstaemme(einzelstaeme)

    def add_einzelstaemme(self, values: Iterable[Einzelstamm]):
        se_list = self.get_referable(deepcopy(KWARGS[EINZELSTAEMME])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)


class Polter(SubmodelElementCollection):
    def __init__(self,
                 polternummer: str,
                 vermessungsverfahren: enums.Vermessungsverfahren,
                 polterstatus: Optional[enums.Polterstatus] = enums.OptionalEnum,
                 messergebnis_einzelstammliste: Optional[EinzelstammListe] = None,
                 messergebnis_raummassindustrieholz: Optional[RaummassIndusrieeholz] = None,
                 messergebnis_raummassenergieholz: Optional[RaummassEnergieholz] = None,
                 standort: Optional[Standort] = None,
                 fotos: Optional[Iterable[str]] = None,
                 videos: Optional[Iterable[str]] = None
                 ):
        super().__init__(**deepcopy(KWARGS[POLTER]))
        self.add_referable(Property(value=polternummer, **deepcopy(KWARGS[POLTERNUMMER])))
        self.add_referable(Property(value=vermessungsverfahren.value, **deepcopy(KWARGS[VERMESSUNGSVERFAHREN])))
        self.add_referable(Property(value=polterstatus.value, **deepcopy(KWARGS[POLTERSTATUS])))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[FOTOS]), item_kwargs=deepcopy(KWARGS[FOTO]), type_value_list_element=Property))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[VIDEOS]), item_kwargs=deepcopy(KWARGS[VIDEO]), type_value_list_element=Property))

        if messergebnis_einzelstammliste:
            messergebnis_einzelstammliste.id_short = MESSERGEBNIS_EINZELSTAMMLISTE
            self.add_referable(messergebnis_einzelstammliste)
        if messergebnis_raummassindustrieholz:
            messergebnis_raummassenergieholz.id_short = MESSERGEBNIS_RAUMMASSINDUSTRIEHOLZ
            self.add_referable(messergebnis_raummassindustrieholz)
        if messergebnis_raummassenergieholz:
            messergebnis_raummassenergieholz.id_short = MESSERGEBNIS_RAUMMASSENERGIEHOLZ
            self.add_referable(messergebnis_raummassenergieholz)
        if standort:
            standort.id_short = STANDORT
            self.add_referable(standort)
        self.add_fotos(fotos)
        self.add_videos(videos)

    def add_fotos(self, values: Iterable[str]):
        se_list = self.get_referable(deepcopy(KWARGS[FOTOS])[ID_SHORT])
        init_items_and_add_to_se_list(se_list=se_list, values_of_items=values, item_kwargs=deepcopy(KWARGS[FOTO]),
                                      item_type=Property)

    def add_videos(self, values: Iterable[str]):
        se_list = self.get_referable(deepcopy(KWARGS[VIDEOS])[ID_SHORT])
        init_items_and_add_to_se_list(se_list=se_list, values_of_items=values, item_kwargs=deepcopy(KWARGS[VIDEO]),
                                      item_type=Property)


class StehenderBaum(SubmodelElementCollection):
    def __init__(self,
                 hoehe: Optional[float] = None,
                 bhd: Optional[float] = None,
                 baumart: Optional[enums.Holzart] = enums.OptionalEnum,
                 position: Optional[Standort] = None
                 ):
        super().__init__(**deepcopy(KWARGS[STEHENDER_BAUM]))
        self.add_referable(Property(value=hoehe, **deepcopy(KWARGS[HOEHE])))
        self.add_referable(Property(value=bhd, **deepcopy(KWARGS[BHD])))
        self.add_referable(Property(value=baumart.value, **deepcopy(KWARGS[STEHENDER_BAUM_BAUMART])))
        if position:
            position.id_short = POSITION
            self.add_referable(position)


class Holzliste(Submodel):
    def __init__(self,
                 id: Identifier,
                 kopfdaten: Kopfdaten,
                 polterliste: Iterable[Polter],
                 sortimentstyp: enums.Sortimentstyp,
                 sorte: enums.Sorte,
                 holzart: enums.Holzart,
                 preismatrix: Optional[Iterable[Preismatrixeintrag]] = None,
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[HOLZLISTE]))
        self.add_referable(kopfdaten)
        self.add_referable(Property(value=sortimentstyp.value, **deepcopy(KWARGS[SORTIMENTSTYP])))
        self.add_referable(Property(value=sorte.value, **deepcopy(KWARGS[SORTE])))
        self.add_referable(Property(value=holzart.value, **deepcopy(KWARGS[HOLZART])))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[POLTER_LISTE]), item_kwargs=deepcopy(KWARGS[POLTER]), type_value_list_element=SubmodelElementCollection))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[PREISMATRIX]), item_kwargs=deepcopy(KWARGS[PREISMATRIXEINTRAG]), type_value_list_element=SubmodelElementCollection))

        self.add_polterliste(polterliste)
        self.add_preismatrix(preismatrix)

    def add_polterliste(self, values: Iterable[Polter]):
        if not values:
            raise Exception("'polter' must include at least a single item.")
        se_list = self.get_referable(deepcopy(KWARGS[POLTER_LISTE])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)

    def add_preismatrix(self, values: Iterable[Preismatrixeintrag]):
        se_list = self.get_referable(deepcopy(KWARGS[PREISMATRIX])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)


class Verkaufslos(Submodel):
    def __init__(self,
                 id: Identifier,
                 verkaufsstatus: enums.Verkaufsstatus,
                 abrechnungsmass: enums.Abrechnungsmass,
                 bereitstellungsart: enums.Bereitstellungsart,
                 verfuegbar_ab: date,
                 verfuegbar_bis: Optional[date] = None,
                 verkauefer: Optional[Kontakt] = None,
                 notiz: Optional[str] = None,
                 holzlisten: Optional[Iterable[Reference]] = None,
                 preismatrix: Optional[Iterable[Preismatrixeintrag]] = None
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[VERKAUFSLOS]))
        self.add_referable(Property(value=verkaufsstatus.value, **deepcopy(KWARGS[VERKAUFSSTATUS])))
        self.add_referable(Property(value=abrechnungsmass.value, **deepcopy(KWARGS[ABRECHNUNGSMASS])))
        self.add_referable(Property(value=bereitstellungsart.value, **deepcopy(KWARGS[BEREITSTELLUNGSART])))
        self.add_referable(Property(value=verfuegbar_ab, **deepcopy(KWARGS[VERFUEGBAR_AB])))
        self.add_referable(Property(value=verfuegbar_bis, **deepcopy(KWARGS[VERFUEGBAR_BIS])))
        if verkauefer:
            verkauefer.id_short = VERKAEUFER
            self.add_referable(verkauefer)
        self.add_referable(Property(value=notiz, **deepcopy(KWARGS[NOTIZ])))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[HOLZLISTEN]), item_kwargs=deepcopy(KWARGS[HOLZLISTE_REF_IN_HOLZLISTEN]), type_value_list_element=ReferenceElement))
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[PREISMATRIX]), item_kwargs=deepcopy(KWARGS[PREISMATRIXEINTRAG]), type_value_list_element=SubmodelElementCollection))

        self.add_holzlisten(holzlisten)
        self.add_preismatrix(preismatrix)

    def add_holzlisten(self, values: Iterable[Reference]):
        se_list = self.get_referable(deepcopy(KWARGS[HOLZLISTEN])[ID_SHORT])
        values = [ReferenceElement(value=i, **deepcopy(KWARGS[HOLZLISTE_REF_IN_HOLZLISTEN])) for i in values]
        add_items_to_se_list(se_list=se_list, items=values)

    def add_preismatrix(self, values: Iterable[Preismatrixeintrag]):
        se_list = self.get_referable(deepcopy(KWARGS[PREISMATRIX])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)


class Holzpreisbereich(SubmodelElementCollection):
    def __init__(self,
                 baumart: enums.Holzart,
                 preis_von: float,
                 preis_bis: float,
                 sorte: Optional[enums.Sorte] = enums.OptionalEnum,
                 gueteklasse: Optional[enums.Gueteklasse] = enums.OptionalEnum,
                 mittendurchmesser_von: Optional[float] = None,
                 mittendurchmesser_bis: Optional[float] = None,
                 holzliste: Optional[Reference] = None,
                 datum: Optional[date] = None,
                 quelle: Optional[str] = None
                 ):
        super().__init__(**deepcopy(KWARGS[HOLZPREISBEREICH]))
        self.add_referable(Property(value=baumart.value, **deepcopy(KWARGS[HOLZPREISBEREICH_BAUMART])))
        self.add_referable(Property(value=preis_von, **deepcopy(KWARGS[PREIS_VON])))
        self.add_referable(Property(value=preis_bis, **deepcopy(KWARGS[PREIS_BIS])))
        self.add_referable(Property(value=sorte.value, **deepcopy(KWARGS[HOLZPREISBEREICH_SORTE])))
        self.add_referable(Property(value=gueteklasse.value, **deepcopy(KWARGS[GUETEKLASSE])))
        self.add_referable(Property(value=mittendurchmesser_von, **deepcopy(KWARGS[MITTENDURCHMESSER_VON])))
        self.add_referable(Property(value=mittendurchmesser_bis, **deepcopy(KWARGS[MITTENDURCHMESSER_BIS])))
        self.add_referable(ReferenceElement(value=holzliste, **deepcopy(KWARGS[HOLZLISTE_REF_IN_HOLZPREISBEREICH])))
        self.add_referable(Property(value=datum, **deepcopy(KWARGS[DATUM])))
        self.add_referable(Property(value=quelle, **deepcopy(KWARGS[QUELLE])))


class Sortiment(SubmodelElementCollection):
    def __init__(self,
                 nummer: int,
                 sortimentstyp: enums.Sortimentstyp,
                 sorte: enums.Sorte,
                 holzart: enums.Holzart,
                 gueteklasseVon: enums.Gueteklasse,
                 gueteklasseBis: enums.Gueteklasse,
                 laenge_von: float,
                 laenge_bis: float,
                 mindestzopf: float,
                 max_durchmesser: float,
                 laengenzugabe: float,
                 mengenschaetzung: float,
                 kaeufer: Optional[Kontakt] = enums.OptionalEnum,
                 bemerkung: Optional[str] = None,
                 ergebnis: Optional[Iterable[Reference]] = None
                 ):
        super().__init__(**deepcopy(KWARGS[SORTIMENT]))
        for i in (
                Property(value=nummer, **deepcopy(KWARGS[NUMMER])),
                Property(value=sortimentstyp.value, **deepcopy(KWARGS[SORTIMENTSTYP])),
                Property(value=sorte.value, **deepcopy(KWARGS[SORTE])),
                Property(value=holzart.value, **deepcopy(KWARGS[HOLZART])),
                Property(value=gueteklasseVon.value, **deepcopy(KWARGS[GUETEKLASSE_VON])),
                Property(value=gueteklasseBis.value, **deepcopy(KWARGS[GUETEKLASSE_BIS])),
                Property(value=laenge_von, **deepcopy(KWARGS[LAENGE_VON])),
                Property(value=laenge_bis, **deepcopy(KWARGS[LAENGE_BIS])),
                Property(value=mindestzopf, **deepcopy(KWARGS[MINDESTZOPF])),
                Property(value=max_durchmesser, **deepcopy(KWARGS[MAX_DURCHMESSER])),
                Property(value=laengenzugabe, **deepcopy(KWARGS[LAENGENZUGABE])),
                Property(value=mengenschaetzung, **deepcopy(KWARGS[MENGENSCHAETZUNG])),
                Property(value=bemerkung, **deepcopy(KWARGS[BEMERKUNG])),
                init_se_list(se_kwargs=deepcopy(KWARGS[ERGEBNIS]), item_kwargs=deepcopy(KWARGS[HOLZLISTE_REF_IN_ERGEBNIS]), type_value_list_element=ReferenceElement),
        ):
            self.add_referable(i)
        if kaeufer:
            kaeufer.id_short = KAEUFER
            self.add_referable(kaeufer)
        self.add_ergebnis(ergebnis)

    def add_ergebnis(self, values: Iterable[Reference]):
        se_list = self.get_referable(deepcopy(KWARGS[ERGEBNIS])[ID_SHORT])
        values = [ReferenceElement(value=i, **deepcopy(KWARGS[HOLZLISTE_REF_IN_ERGEBNIS])) for i in values]
        add_items_to_se_list(se_list=se_list, items=values)


class ZuFaellendeBaeume(Submodel):
    def __init__(self,
                 id: Identifier,
                 beschreibung: str,
                 umring: Umring,
                 position: Optional[Standort] = None,
                 baumliste: Optional[Iterable[StehenderBaum]] = None,
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[ZU_FAELLENDE_BAEUME]))
        self.add_referable(Property(value=beschreibung, **deepcopy(KWARGS[BESCHREIBUNG])))
        self.add_referable(umring)
        if position:
            position.id_short = POSITION
            self.add_referable(position)
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[BAUMLISTE]), item_kwargs=deepcopy(KWARGS[STEHENDER_BAUM]), type_value_list_element=SubmodelElementCollection))
        self.add_baumliste(baumliste)

    def add_baumliste(self, values: Iterable[StehenderBaum]):
        se_list = self.get_referable(deepcopy(KWARGS[BAUMLISTE])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)


class Holzpreisbereiche(Submodel):
    def __init__(self,
                 id: Identifier,
                 holzpreisbereiche: Optional[Iterable[Holzpreisbereich]] = None,
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[HOLZPREISBEREICHE]))
        self.add_holzpreisbereiche(holzpreisbereiche)

    def add_holzpreisbereiche(self, holzpreisbereiche: Iterable[StehenderBaum]):
        if holzpreisbereiche:
            for i, holzpreisbereich in enumerate(holzpreisbereiche):
                id_short = find_next_free_id_short(collection=self, id_short_base=HOLZPREISBEREICH, start=1 + i)
                holzpreisbereich.id_short = id_short
                self.add_referable(holzpreisbereich)


class Bestandesdaten(Submodel):
    def __init__(self,
                 id: Identifier,
                 umring: Optional[Umring] = None,
                 waldbesitzer: Optional[Kontakt] = None,
                 baumarten: Optional[Iterable[enums.Holzart]] = None,
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[BESTANDESDATEN]))
        if waldbesitzer:
            waldbesitzer.id_short = WALDBESITZER
        for i in (
                umring,
                waldbesitzer
        ):
            if i:
                self.add_referable(i)
        self.add_referable(init_se_list(se_kwargs=deepcopy(KWARGS[BAUMARTEN]), item_kwargs=deepcopy(KWARGS[BESTANDESDATEN_BAUMART]), type_value_list_element=Property))

        self.add_baumarten(baumarten)

    def add_baumarten(self, values: Iterable[enums.Holzart]):
        se_list = self.get_referable(deepcopy(KWARGS[BAUMARTEN])[ID_SHORT])
        init_items_and_add_to_se_list(se_list=se_list, values_of_items=values, item_kwargs=deepcopy(KWARGS[BESTANDESDATEN_BAUMART]),
                                      item_type=Property)


class Beobachtung(Submodel):
    def __init__(self,
                 id: Identifier,
                 name: str,
                 beschreibung: str,
                 umring: Umring,
                 position: Optional[Standort] = None
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[BEOBACHTUNG]))
        for i in (
                Property(value=name, **deepcopy(KWARGS[NAME])),
                Property(value=beschreibung, **deepcopy(KWARGS[BESCHREIBUNG])),
                umring,
        ):
            self.add_referable(i)
        if position:
            position.id_short = POSITION
            self.add_referable(position)


class Waldweg(Submodel):
    def __init__(self,
                 id: Identifier,
                 beschreibung: str,
                 erstellungsdatum: Optional[date] = None,
                 aktualisierungsdatum: Optional[date] = None,
                 breite: Optional[float] = None,
                 wegpunkte: Optional[Iterable[Standort]] = None,
                 wendepunkte: Optional[Iterable[Standort]] = None
                 ):
        super().__init__(id_=id, **deepcopy(KWARGS[WALDWEG]))
        for i in (
                Property(value=beschreibung, **deepcopy(KWARGS[BESCHREIBUNG])),
                Property(value=erstellungsdatum, **deepcopy(KWARGS[ERSTELLUNGSDATUM])),
                Property(value=aktualisierungsdatum, **deepcopy(KWARGS[AKTUALISIERUNGSDATUM])),
                Property(value=breite, **deepcopy(KWARGS[BREITE])),
                init_se_list(se_kwargs=deepcopy(KWARGS[WEGPUNKTE]), item_kwargs=deepcopy(KWARGS[STANDORT]), type_value_list_element=SubmodelElementCollection),
                init_se_list(se_kwargs=deepcopy(KWARGS[WENDEPUNKTE]), item_kwargs=deepcopy(KWARGS[STANDORT]), type_value_list_element=SubmodelElementCollection),
        ):
            if i:
                self.add_referable(i)
        self.add_wegpunkte(wegpunkte)
        self.add_wendepunkte(wendepunkte)

    def add_wegpunkte(self, values: Iterable[Standort]):
        se_list = self.get_referable(deepcopy(KWARGS[WEGPUNKTE])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)

    def add_wendepunkte(self, values: Iterable[Standort]):
        se_list = self.get_referable(deepcopy(KWARGS[WENDEPUNKTE])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)


class Arbeitsauftrag(Submodel):
    def __init__(self,
                 auftrag_id: Identifier,
                 hiebsnummer: str,
                 kurzbeschreibung: str,
                 ansprechpartner: Kontakt,
                 unternehmer_holzernte: Optional[Kontakt] = None,
                 unternehmer_holzrueckung: Optional[Kontakt] = None,
                 sicherheitshinweise: Optional[str] = None,
                 naturschutzhinweise: Optional[str] = None,
                 sortimente: Optional[Iterable[Sortiment]] = None,
                 karte: str = None,
                 auftragsstatus: Optional[enums.Auftragsstatus] = enums.OptionalEnum,
                 zu_faellende_baeume: Optional[Reference] = None,
                 ):
        super().__init__(id_=auftrag_id, **deepcopy(KWARGS[ARBEITSAUFTRAG]))
        ansprechpartner.id_short = ANSPRECHPARTNER
        for i in (
                Property(value=hiebsnummer, **deepcopy(KWARGS[HIEBSNUMMER])),
                Property(value=kurzbeschreibung, **deepcopy(KWARGS[KURZBESCHREIBUNG])),
                Property(value=sicherheitshinweise, **deepcopy(KWARGS[SICHERHEITSHINWEISE])),
                Property(value=naturschutzhinweise, **deepcopy(KWARGS[NATURSCHUTZHINWEISE])),
                Property(value=karte, **deepcopy(KWARGS[KARTE])),
                Property(value=auftragsstatus.value, **deepcopy(KWARGS[AUFTRAGSSTATUS])),
                init_se_list(se_kwargs=deepcopy(KWARGS[SORTIMENTE]), item_kwargs=deepcopy(KWARGS[SORTIMENT]), type_value_list_element=SubmodelElementCollection),
                ansprechpartner
        ):
            self.add_referable(i)
        if unternehmer_holzrueckung:
            unternehmer_holzrueckung.id_short = UNTERNEHMER_HOLZRUECKUNG
            self.add_referable(unternehmer_holzrueckung)
        if unternehmer_holzernte:
            unternehmer_holzernte.id_short = UNTERNEHMER_HOLZERNTE
            self.add_referable(unternehmer_holzernte)
        if zu_faellende_baeume:
            self.add_referable(ReferenceElement(value=zu_faellende_baeume, **deepcopy(KWARGS[ZU_FAELLENDE_BAEUME_REF])))
        self.add_sortimente(sortimente)

    def add_sortimente(self, values: Iterable[Sortiment]):
        se_list = self.get_referable(deepcopy(KWARGS[SORTIMENTE])[ID_SHORT])
        add_items_to_se_list(se_list=se_list, items=values)


class DZWald(AssetAdministrationShell):
    def __init__(self,
                 dzwald_id: Identifier,
                 asset: AssetInformation,
                 holzlisten: Optional[Iterable[Reference]] = None,  # Optional[Iterable[Reference[Holzliste]]]
                 arbeitsauftraege: Optional[Iterable[Reference]] = None,  # Optional[Iterable[Reference[Arbeitsauftrag]]]
                 bestandesdaten: Optional[Reference] = None,  # Optional[Reference[Bestandesdaten]]
                 verkaufslose: Optional[Iterable[Reference]] = None,  # Optional[Iterable[Reference[Verkaufslos]]]
                 beobachtungen: Optional[Iterable[Reference]] = None,  # Optional[Iterable[Reference[Beobachtung]]]
                 waldwege: Optional[Iterable[Reference]] = None,  # Optional[Iterable[Reference[Waldweg]]]
                 zu_faellende_baeume: Optional[Reference] = None,  # Optional[Reference[ZuFaellendeBaeume]]
                 holzpreisbereiche: Optional[Reference] = None,  # Optional[Reference[Holzpreisbereiche]]
                 ):
        super().__init__(id_=dzwald_id, asset_information=asset, **deepcopy(KWARGS[DZ_WALD]))

        # TODO: Implement, Should the AAS contain the submodels or only reference them?

        for i in (
                holzlisten,
                arbeitsauftraege,
                verkaufslose,
                beobachtungen,
                waldwege
        ):
            self.add_submodels(i)
        for i in (
                bestandesdaten,
                zu_faellende_baeume,
                holzpreisbereiche
        ):
            if i: self.add_submodels([i])

    # Temporarily, add only references
    def add_submodels(self, submodels: Iterable[Reference]):
        if submodels:
            self.submodel.update(submodels)
