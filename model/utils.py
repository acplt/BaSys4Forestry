from copy import deepcopy
from enum import Enum
from typing import Any, Iterable

from basyx.aas.model import SubmodelElementCollection, Property, SubmodelElement, \
    SubmodelElementList, Submodel, Range

from model.configs import ID_SHORT, VALUE_TYPE, SEMANTIC_ID


def find_next_free_id_short(collection: SubmodelElementCollection, id_short_base: str,
                            start: int = 1):
    n = start
    while True:
        try:
            collection.get_referable(f"{id_short_base}{n}")
            n += 1
        except KeyError:
            return f"{id_short_base}{n}"


def init_se_list(se_kwargs, item_kwargs, type_value_list_element=Property):
    if issubclass(type_value_list_element, (Property, Range)):
        se_list = SubmodelElementList(type_value_list_element=type_value_list_element,
                                      value_type_list_element=item_kwargs[VALUE_TYPE],
                                      semantic_id_list_element=item_kwargs[SEMANTIC_ID],
                                      **se_kwargs)
    else:
        se_list = SubmodelElementList(type_value_list_element=type_value_list_element,
                                      semantic_id_list_element=item_kwargs[SEMANTIC_ID],
                                      **se_kwargs)
    return se_list


def add_items_to_se_list(se_list, items: Iterable[SubmodelElement]):
    if items:
        for n, item in enumerate(items):
            item.id_short = find_next_free_id_short(collection=se_list,
                                                    id_short_base=item.id_short,
                                                    start=1 + n)
            se_list.add_referable(item)


def init_items_and_add_to_se_list(se_list, values_of_items: Iterable[Any], item_kwargs,
                                  item_type=Property):
    if values_of_items:
        for n, val in enumerate(values_of_items):
            if isinstance(val, Enum):
                val = val.value
            # create item e.g. Property(value="example@email.de", value_type=str, ...)
            item = item_type(value=val, **deepcopy(item_kwargs))
            # find free id_short in SubmodelElementList and set item's id_short to it
            item.id_short = find_next_free_id_short(collection=se_list,
                                                    id_short_base=item_kwargs[ID_SHORT],
                                                    start=1 + n)
            se_list.add_referable(item)
