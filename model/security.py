import base64
import json
from enum import Enum, IntEnum
from typing import Iterable, Optional
from basyx.aas import model
from basyx.aas.model import Submodel, SubmodelElementCollection, SubmodelElementList
from model import utils 


class PermissionKind(IntEnum):
    ALLOW = 0,
    DENY = 1,
    IN_APPLICABLE = 3,
    UNDEFINED = 4


class Permission(str, Enum):
    READ = "READ",
    WRITE = "WRITE"


class AccessPermissionRule(SubmodelElementCollection):

    def __init__(self,
                 user_id: str,
                 permission_kind: PermissionKind,
                 permission: Permission):
      super().__init__(id_short="AccessPermissionRule")
      self.add_referable(model.Property(id_short="User", value_type=model.datatypes.String, value=user_id))
      self.add_referable(model.Property(id_short="PermissionKind", value_type=model.datatypes.Integer, value=permission_kind.value))
      self.add_referable(model.Property(id_short="Permission", value_type=model.datatypes.String, value=permission.value))


class AccessPermissionCollection(SubmodelElementCollection):

    def __init__(self,
                 target: model.ModelReference,
                 rules: Optional[Iterable[AccessPermissionRule]] = None):
        super().__init__(
            id_short="AccessPermissionCollection", 
        )
        self.add_referable(model.ReferenceElement(id_short="Target", value=target))
        rules_se_list = model.SubmodelElementList(id_short="Rules", type_value_list_element=SubmodelElementCollection)
        utils.add_items_to_se_list(rules_se_list, rules)
        self.add_referable(rules_se_list)
    
    def add_rules(self,
                  rules: Iterable[AccessPermissionRule]):
        rules_se_list: model.SubmodelElementList = self.get_referable("Rules")
        for i in rules:
            rules_se_list.add_referable(i)


class AccessControl(SubmodelElementList):

    def __init__(self,
                 permissions: Optional[Iterable[AccessPermissionCollection]] = None):
        super().__init__(id_short="AccessControl", type_value_list_element=SubmodelElementCollection)
        utils.add_items_to_se_list(self, permissions)


class Security(Submodel):

     def __init__(self,
                 id: model.Identifier,
                 access_control: Optional[AccessControl] = None
                 ):
        super().__init__(id_=id, id_short="Security")
        if access_control:
            self.add_referable(access_control)

"""
def id_short_path_from_ref(ref: model.ModelReference):
    arr = []
    for i, key in enumerate(ref.key):
        if key.value.isnumeric():
            arr[i-1] = f"{arr[i-1]}[{key.value}]"
        elif key.type == model.KeyTypes.SUBMODEL:
            arr.append(base64.urlsafe_b64encode(key.value.encode()).decode())
        elif key.type == model.KeyTypes.ASSET_ADMINISTRATION_SHELL:
            arr.append("aas")
        else:
            arr.append(key.value)
    return ".".join(arr)
"""

def id_short_path_from_ref(ref: model.ModelReference):
    arr = ["aas"]
    for i, key in enumerate(ref.key):
        if key.value.isnumeric():
            arr[i-1] = f"{arr[i-1]}[{key.value}]"
        elif key.type == model.KeyTypes.SUBMODEL:
            arr.append(base64.urlsafe_b64encode(key.value.encode()).decode())
        elif key.type == model.KeyTypes.ASSET_ADMINISTRATION_SHELL:
            continue
        else:
            arr.append(key.value)
    return ".".join(arr)

def get_dic_from_security_submodel(security_sm: Security):
    dic = {}
    access_control: AccessControl = security_sm.get_referable("AccessControl")
    for permission in access_control.value:
        permission: AccessPermissionCollection = permission
        target: model.ReferenceElement = permission.get_referable("Target")
        id_short_path = id_short_path_from_ref(target.value)
        rules: model.SubmodelElementList = permission.get_referable("Rules")
        for rule in rules:
            user: model.Property = rule.get_referable("User")
            grant: model.Property = rule.get_referable("Permission")
            if not dic.get(id_short_path):
                dic[id_short_path] = {}
                dic[id_short_path][Permission.READ.value] = []
                dic[id_short_path][Permission.WRITE.value] = []
            dic[id_short_path][grant.value].append(user.value)
    return dic
    



