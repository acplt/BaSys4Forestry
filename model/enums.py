from enum import Enum, unique


class OptionalEnum():
    value = None

@unique
class Sortimentstyp(Enum):
    # note top: https://eldatstandard.de/dokumentation/1.0.2/grade(teilweise!)
    Stammholz = "st"
    Energieholz = "en"
    Industrieholz = "in"
    Sondersortimente = "so"

@unique
class Sorte(Enum):
    # note top: https://eldatstandard.de/dokumentation/1.0.2/grade(teilweise!)
    lang = "lg"
    kurz = "kz"
    Abschnitte = "ab"
    Hackschnitzel = "hs"

@unique
class Holzart(Enum):
    Keine = "xy"
    Nadelholz = "ndh"
    Fichte = "fi"
    Gemeine_Fichte = "gfi"
    Omorikafichte = "ofi"
    Sitkafichte = "sfi"
    Schwarzfichte = "swfi"
    Engelmannsfichte = "efi"
    Blaufichte_Stechfichte = "bfi"
    Weissfichte = "wfi"
    Sonstige_Fichten = "sofi"
    Kiefer = "kie"
    Gemeine_Kiefer = "ki"
    Bergkiefer = "bki"
    Schwarzkiefer = "ski"
    Rumelische_Kiefer = "rki"
    Zirbelkiefer = "zki"
    Weymouthskiefer = "wki"
    Murraykiefer = "mki"
    Gelbkiefer = "gki"
    Sonstige_Kiefer = "soki"
    Tanne = "ta"
    Weisstanne = "wta"
    Amerikanische_Edeltanne = "ata"
    Coloradotanne = "cta"
    Kuestentanne = "kta"
    Nikkotanne = "nita"
    Nordmannstanne = "nota"
    Veitchtanne = "vta"
    Sonstige_Tannen = "sota"
    Douglasie = "dgl"
    Laerche = "la"
    Europaeische_Laerche = "ela"
    Japanische_Laerche_und_Hybrid = "jla"
    Sonstige_Laerchen = "sla"
    Sonstige_Nadelbaeume = "sonb"
    Lebensbaum = "lb"
    Hemlockstanne = "ht"
    Mammutbaum = "mam"
    Eibe = "eib"
    Lawsonszypresse = "sz"
    Buche = "bu"
    Stieleiche = "sei"
    Traubeneiche = "tei"
    Roteiche = "rei"
    Zerreiche = "zei"
    Sumpfeiche = "suei"
    Eiche = "ei"
    sonstige_Eichen = "que"
    Esche = "es"
    Gemeine_Esche = "ges"
    Weissesche = "wes"
    Sonstige_Eschen = "fra"
    Hainbuche_bzw_Weissbuche = "hbu"
    Ahorn = "ah"
    Bergahorn = "bah"
    Spitzahorn = "sah"
    Feldahorn = "fah"
    Eschenblaettriger_Ahorn = "eah"
    Silberahorn = "siah"
    Sonstige_Ahorne = "ace"
    Linde = "li"
    Winterlinde = "wli"
    Sommerlinde = "sli"
    Sonstige_Linden = "til"
    Robinie = "rob"
    Akazie = "akz"
    Ulme = "ul"
    Bergulme = "bul"
    Feldulme = "ful"
    Flatterulme = "flu"
    Sonstige_Ulmen = "ulm"
    Rosskastanie = "rka"
    Edelkastanie = "eka"
    Kastanie = "ka"
    Weisser_Maulbeerbaum = "mau"
    Nussbaum = "nus"
    Walnuss = "wnu"
    Schwarznuss_und_Hybriden = "snu"
    Sonstige_Nussbaeume = "jug"
    Stechpalme = "ste"
    Platane = "pla"
    Ahornblaettrige_Platane = "apl"
    Sonstige_Laubbaeume_mit_hoher_Lebensdauer = "solh"
    Gemeine_Birke = "gbi"
    Moorbirke_und_Karpatenbirke = "mbi"
    Birke = "bi"
    Erle = "erl"
    Schwarzerle = "ser"
    Weisserle_Grauerle = "wer"
    Gruenerle = "ger"
    Sonstige_Erlen = "aln"
    Pappel = "pap"
    Aspe_Zitterpappel = "zpa"
    Europaeische_Schwarzpappel = "spa"
    Schwarzpappel_Hypriden = "spah"
    Graupappel_und_Hybriden = "gpa"
    Silberpappel_Weisspappel = "wpa"
    Balsampappel = "bpa"
    Balsampappel_Hybriden = "bpah"
    Sonstige_Pappeln = "pop"
    Sorbusarten = "sor"
    Sonstige_Sorbusarten = "sso"
    Vogelbeere = "vb"
    Elsbeere = "els"
    Speierling = "spe"
    Echte_Mehlbeere = "meb"
    Weide = "wei"
    Salweide = "swei"
    Kirsche = "kir"
    Gewoehnliche_Traubenkirsche = "gtk"
    Vogelkirsche = "vk"
    Spaetbluehende_Traubenkirsche = "stk"
    Sonstige_Kirschen = "pru"
    Zwetschge = "zwe"
    Hickory = "hic"
    Sonstige_Laubbaeume_mit_niedriger_Lebensdauer = "soln"
    Gemeiner_Faulbaum_Pulverholz = "fau"
    Wildobst_unbestimmt = "wob"
    Holzapfel_Wildapfel = "wap"
    Holzbirne_Wildbirne = "wbi"
    Baumhasel = "has"
    Gemeiner_Goetterbaum = "got"
    Sonstiges_Hartlaubholz = "slbh"
    Laubholz = "lbh"
    Sonstiges_Weichlaubholz = "slbw"
    Strauch_unbestimmt = "str"
    Mischsortiment_Fichte_bzw_Tanne = "fita"

# note top: https://eldatstandard.de/dokumentation/1.0.2/species

@unique
class Vermessungsverfahren(Enum):
    # note top: https://eldatstandard.de/dokumentation/1.0.2/measuring_medium(
    # "Vermessungsart: Verfahren, das zur Vermessung verwendet wurde (gegebenenfalls nicht abrechnungsrelevant)"

    Mittenstaerkenvermessung = "mit"
    Mittenstaerkenstichprobenverfahren = "mis"
    Raummassverfahren_allgemein = "arm"
    Sektionsraummassverfahren = "srm"
    Konventionelles_Raummassverfahren = "krm"
    Stirnflaechenverfahren = "stf"
    Zaehlung = "zae"
    Schaetzung = "stz"
    Werkvermessung = "wev"
    Gewichtsmassermittlung = "gwm"
    Gewichtsmassermittlung_atro = "gwa"
    Gewichtsmassermittlung_lutro = "gwl"
    Stangenvermessung = "stg"
    Zopfstaerkenvermessung = "zsv"
    Fremdvermessung = "fra"
    Laufmeter = "lfm"
    Sonstiges = "son"
    Harvestermass = "hvm"
    optische_Poltervermessung = "opv"
    Waldmass = "wam"
    Keins = "xy"

@unique
class Gueteklasse(Enum):
# note top: https://eldatstandard.de/dokumentation/1.0.2/qual_type

    Normale_Qualitaet = "in"
    Fehlerhafte_Qualitaet = "if"
    Krank = "ik"
    Qualitaet_fehlerhaft_bzw_krank = "fk"
    Qualitaet_normal_bzw_fehlerhaft = "nf"
    Qualitaet_normal_bzw_fehlerhaft_bzw_krank = "nfk"
    Qualitaet_B = "b"
    Qualitaet_B_Rotkern = "bk"
    Qualitaet_C = "c"
    Qualitaet_D = "d"
    Ohne_Qualitaetsausscheidung = "oa"
    B_C_Mischqualitaet = "bc"
    C_D_Mischqualitaet = "cd"
    B_C_D_Mischqualitaet = "bcd"
    Qualitaet_A = "a"
    Qualitaet_A_Rotkern = "ak"

    #' keine Anwendung in Deutschland:
    #'a_fu - Furnierholz (ÖNORM L 2021)
    #'a_is - Schleifholz (ÖNORM L 2021)
    #'a_if - Faserholz (ÖNORM L 2021)
    #'a_i2 - Sekunda (ÖNORM L 2021)
    #'a_id - Industrieduennholz (ÖNORM L 2021)
    #'a_im - Manipulationsholz (ÖNORM L 2021)
    #'a_sp - Splitterholz (ÖNORM L 2021)
    #'a_y - Braunbloche (ÖNORM L 2021)
    #'a_x - C-Kreuz (noch saegefaehiger Ausschuss) (ÖNORM L 2021)
    #'a_z - Ausschuss (ÖNORM L 2021)
    #'a_bh - Brennholz (ÖNORM L 2021)
    #'ch_k - Qualitaet Kaeferholz
    #'ch_ab - Qualitaet AB
    #'ch_bc - Qualitaet BC
    #'ch_cd - Qualitaet CD
    #'ch_abc - Qualitaet ABC
    #'ch_r - Qualitaet Rotholz
    #'ch_1 - Qualitaet 1. Klasse
    #'ch_2 - Qualitaet 2. Klasse
    #'ch_bk - Braunkern
    #'ch_sk - Spritzkern

@unique
class CRS(Enum):
    # note top: https://epsg.io /
    # TODO: implement
    pass
    #EPSG: 25832 - ETRS89 / UTM zone 32N

@unique
class Verkaufsstatus(Enum):
    # note top: Forstify - Definitionen
    Offen = 10
    Angeboten = 20
    Verkauft_Holzuebernahme = 30
    Bezahlt = 40
    Logistikauftrag = 50
    Vergeben = 60
    Archiviert = 70


@unique
class Auftragsstatus(Enum):
    # note top: https://eldatstandard.de/dokumentation/1.0.2/statusid
    Erstellt = 10
    Geaendert = 20
    Storniert = 30
    Gesendet = 40
    Angenommen = 50
    Abgelehnt = 60
    Disponiert = 70
    Auftragsbeginn = 80
    Unterbrochen = 90
    Abgebrochen = 100
    Fahre_ins_Revier = 110
    Lieferschein_erstellt = 120
    Verlasse_Revier = 130
    Am_Lieferort_angekommen = 140
    Auftragsende = 150

@unique
class Hieb(Enum):
    # note top  # FF0000: TODO Eigendefinition?
    Sammelhieb = "sh"
    Durchforstung = "df"
    Kalamitaet = "kl"

@unique
class Kontaktrolle(Enum):
    # note top: Eigendefinition
    Waldbesitzer = "Waldbesitzer"
    Lieferant = "Lieferant"
    Verkaeufer = "Verkaeufer"
    Kaeufer = "Kaeufer"
    Fuhrmann = "Fuhrmann"
    UnternehmerHolzernte = "UnternehmerHolzernte"
    UnternehmerHolzrueckung = "UnternehmerHolzrueckung"

@unique
class Bereitstellungsart(Enum):
    # note top: https://eldatstandard.de/dokumentation/1.0.2/delivery_term
    Unfrei_Waldstrasse = "uws"
    Unfrei_Werk = "uwe"
    Unfrei_Zwischenlager = "uzw"
    Unfrei_Waggon = "uwa"
    Unfrei_Schiff = "usc"
    Frei_Stock_bzw_Ab_Werk_incoterm = "exw"
    Frei_Waldstrasse_bzw_Frei_Frachtfuehrer_incoterm = "fca"
    Frei_Schiff_bzw_Waggon_bzw_Frei_an_Bord_incoterm = "fob"
    Frei_Zwischenlager_bzw_Geliefert_frei_Terminal_incoterm = "dat"
    Frei_Werk_bzw_Geliefert_verzollt_incoterm = "ddp"
    Frachtfrei_bis_incoterm = "cpt"
    Frachtfrei_versichert_bis_incoterm = "cip"
    Geliefert_benannter_Ort_incoterm = "dap"
    Frei_laengsseits_Schiff_incoterm = "fas"
    Kosten_und_Fracht_incoterm = "cfr"
    Frachtfrei_incoterm = "cif"

@unique
class Abrechnungsmass(Enum):
    # note top: https://eldatstandard.de/dokumentation/1.0.2/measurement_method(
    #    "Abrechnungsrelevantes Vermessungsverfahren: Angabe des Vermessungsverfahrens nach dem abgerechnet wird")
    Mittenstaerkenvermessung = "mit"
    Mittenstaerkenstichprobenverfahren = "mis"
    Raummassverfahren_allgemein = "arm"
    Sektionsraummassverfahren = "srm"
    Konventionelles_Raummassverfahren = "krm"
    Stirnflaechenverfahren = "stf"
    Zaehlung = "zae"
    Schaetzung = "stz"
    Werkvermessung = "wev"
    Gewichtsmassermittlung = "gwm"
    Gewichtsmassermittlung_atro = "gwa"
    Gewichtsmassermittlung_lutro = "gwl"
    Stangenvermessung = "stg"
    Zopfstaerkenvermessung = "zsv"
    Fremdvermessung = "fra"
    Laufmeter = "lfm"
    Sonstiges = "son"
    Harvestermass = "hvm"
    optische_Poltervermessung = "opv"
    Waldmass = "wam"
    Keins = "xy"

@unique
class Umsatzbesteuerung(Enum):
    # note top: Eigendefinition
    regelbesteuert = "rb"
    pauschalbesteuert = "pb"
    sortimentsbezogen = "sb"

@unique
class Polterstatus(Enum):
    # note top: Eigendefinition
    Offen = "Offen"
    FertigGepoltert = "FertigGepoltert"
    Vermessen = "Vermessen"
    Bereitgestellt = "Bereitgestellt"
    Abgefahren = "Abgefahren"

@unique
class Holzlistenstatus(Enum):
    # note top: Eigendefinition
    Offen = "Offen"
    Verkaufsbereit = "Verkaufsbereit"
