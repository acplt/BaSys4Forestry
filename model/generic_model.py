from typing import *
import inspect
from basyx.aas.model import *
from basyx.aas.model.datatypes import *


class Arbeitsauftrag(Submodel):
    class Hiebsnummer(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Hiebsnummer",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Hiebsnummer",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Kurzbeschreibung(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Kurzbeschreibung",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Kurzbeschreibung",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Sicherheitshinweise(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Sicherheitshinweise",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sicherheitshinweise",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Naturschutzhinweise(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Naturschutzhinweise",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Naturschutzhinweise",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Karte(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Karte",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Karte",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Auftragsstatus(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Auftragsstatus",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Auftragsstatus",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Sortimente(SubmodelElementList):
        class Sortiment(SubmodelElementCollection):
            class Nummer(Property):
                def __init__(
                    self,
                    value: int,
                    id_short: str = "Nummer",
                    value_type: DataTypeDefXsd = int,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Nummer",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Sortimentstyp(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Sortimentstyp",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Sortimentstyp",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Sorte(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Sorte",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Sorte",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Holzart(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Holzart",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Holzart",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class GueteklasseVon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "GueteklasseVon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/GueteklasseVon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class GueteklasseBis(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "GueteklasseBis",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/GueteklasseBis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class LaengeVon(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "LaengeVon",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/LaengeVon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class LaengeBis(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "LaengeBis",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/LaengeBis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Mindestzopf(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "Mindestzopf",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Mindestzopf",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class MaxDurchmesser(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "MaxDurchmesser",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/MaxDurchmesser",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Laengenzugabe(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "Laengenzugabe",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Laengenzugabe",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Mengenschaetzung(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "Mengenschaetzung",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Mengenschaetzung",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Bemerkung(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Bemerkung",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Bemerkung",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Ergebnis(SubmodelElementList):
                class HolzlisteReference(ReferenceElement):
                    def __init__(
                        self,
                        value: Reference,
                        id_short: str = "HolzlisteReference",
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Ergebnis/HolzlisteReference",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    holzlisteReference: Optional[
                        Iterable[Union[Reference, HolzlisteReference]]
                    ] = None,
                    id_short: str = "Ergebnis",
                    type_value_list_element: SubmodelElement = ReferenceElement,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = None,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Ergebnis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if holzlisteReference and all(
                        [isinstance(i, Reference) for i in holzlisteReference]
                    ):
                        holzlisteReference = [
                            self.HolzlisteReference(i) for i in holzlisteReference
                        ]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [holzlisteReference]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Kaeufer(SubmodelElementCollection):
                class Firmenname(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Firmenname",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Firmenname",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Anrede(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Anrede",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Anrede",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Vorname(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Vorname",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Vorname",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Nachname(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Nachname",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Nachname",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Adresse(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Adresse",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Adresse",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Umsatzbesteuerung(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Umsatzbesteuerung",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Umsatzbesteuerung",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class IBAN(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "IBAN",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/IBAN",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Steuernummer(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Steuernummer",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Steuernummer",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Telefonnummern(SubmodelElementList):
                    class Telefon(Property):
                        def __init__(
                            self,
                            value: str,
                            id_short: str = "Telefon",
                            value_type: DataTypeDefXsd = str,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Telefonnummern/Telefon",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToMany",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    def __init__(
                        self,
                        telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                        id_short: str = "Telefonnummern",
                        type_value_list_element: SubmodelElement = Property,
                        semantic_id_list_element: Optional[Reference] = None,
                        value_type_list_element: Optional[DataTypeDefXsd] = str,
                        order_relevant: bool = True,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Telefonnummern",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        # Build a list of submodel elements if a raw values were passed in the argument
                        if telefon and all([isinstance(i, str) for i in telefon]):
                            telefon = [self.Telefon(i) for i in telefon]

                        # Add all passed/initialized submodel elements to a single list
                        embedded_submodel_elements = []
                        for se_arg in [telefon]:
                            if se_arg is None:
                                continue
                            elif isinstance(se_arg, SubmodelElement):
                                embedded_submodel_elements.append(se_arg)
                            elif isinstance(se_arg, Iterable):
                                for n, element in enumerate(se_arg):
                                    element.id_short = f"{element.id_short}{n}"
                                    embedded_submodel_elements.append(element)
                            else:
                                raise TypeError(
                                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                                )

                        super().__init__(
                            value=embedded_submodel_elements,
                            id_short=id_short,
                            type_value_list_element=type_value_list_element,
                            semantic_id_list_element=semantic_id_list_element,
                            value_type_list_element=value_type_list_element,
                            order_relevant=order_relevant,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Emailadressen(SubmodelElementList):
                    class Email(Property):
                        def __init__(
                            self,
                            value: str,
                            id_short: str = "Email",
                            value_type: DataTypeDefXsd = str,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Emailadressen/Email",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToMany",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    def __init__(
                        self,
                        email: Optional[Iterable[Union[str, Email]]] = None,
                        id_short: str = "Emailadressen",
                        type_value_list_element: SubmodelElement = Property,
                        semantic_id_list_element: Optional[Reference] = None,
                        value_type_list_element: Optional[DataTypeDefXsd] = str,
                        order_relevant: bool = True,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Emailadressen",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        # Build a list of submodel elements if a raw values were passed in the argument
                        if email and all([isinstance(i, str) for i in email]):
                            email = [self.Email(i) for i in email]

                        # Add all passed/initialized submodel elements to a single list
                        embedded_submodel_elements = []
                        for se_arg in [email]:
                            if se_arg is None:
                                continue
                            elif isinstance(se_arg, SubmodelElement):
                                embedded_submodel_elements.append(se_arg)
                            elif isinstance(se_arg, Iterable):
                                for n, element in enumerate(se_arg):
                                    element.id_short = f"{element.id_short}{n}"
                                    embedded_submodel_elements.append(element)
                            else:
                                raise TypeError(
                                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                                )

                        super().__init__(
                            value=embedded_submodel_elements,
                            id_short=id_short,
                            type_value_list_element=type_value_list_element,
                            semantic_id_list_element=semantic_id_list_element,
                            value_type_list_element=value_type_list_element,
                            order_relevant=order_relevant,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    firmenname: Optional[Union[str, Firmenname]] = None,
                    anrede: Optional[Union[str, Anrede]] = None,
                    vorname: Optional[Union[str, Vorname]] = None,
                    nachname: Optional[Union[str, Nachname]] = None,
                    adresse: Optional[Union[str, Adresse]] = None,
                    umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
                    iBAN: Optional[Union[str, IBAN]] = None,
                    steuernummer: Optional[Union[str, Steuernummer]] = None,
                    telefonnummern: Optional[
                        Union[Iterable[str], Telefonnummern]
                    ] = None,
                    emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
                    id_short: str = "Kaeufer",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if firmenname and not isinstance(firmenname, SubmodelElement):
                        firmenname = self.Firmenname(firmenname)

                    # Build a submodel element if a raw value was passed in the argument
                    if anrede and not isinstance(anrede, SubmodelElement):
                        anrede = self.Anrede(anrede)

                    # Build a submodel element if a raw value was passed in the argument
                    if vorname and not isinstance(vorname, SubmodelElement):
                        vorname = self.Vorname(vorname)

                    # Build a submodel element if a raw value was passed in the argument
                    if nachname and not isinstance(nachname, SubmodelElement):
                        nachname = self.Nachname(nachname)

                    # Build a submodel element if a raw value was passed in the argument
                    if adresse and not isinstance(adresse, SubmodelElement):
                        adresse = self.Adresse(adresse)

                    # Build a submodel element if a raw value was passed in the argument
                    if umsatzbesteuerung and not isinstance(
                        umsatzbesteuerung, SubmodelElement
                    ):
                        umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

                    # Build a submodel element if a raw value was passed in the argument
                    if iBAN and not isinstance(iBAN, SubmodelElement):
                        iBAN = self.IBAN(iBAN)

                    # Build a submodel element if a raw value was passed in the argument
                    if steuernummer and not isinstance(steuernummer, SubmodelElement):
                        steuernummer = self.Steuernummer(steuernummer)

                    # Build a submodel element if a raw value was passed in the argument
                    if telefonnummern and not isinstance(
                        telefonnummern, SubmodelElement
                    ):
                        telefonnummern = self.Telefonnummern(telefonnummern)

                    # Build a submodel element if a raw value was passed in the argument
                    if emailadressen and not isinstance(emailadressen, SubmodelElement):
                        emailadressen = self.Emailadressen(emailadressen)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [
                        firmenname,
                        anrede,
                        vorname,
                        nachname,
                        adresse,
                        umsatzbesteuerung,
                        iBAN,
                        steuernummer,
                        telefonnummern,
                        emailadressen,
                    ]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                nummer: Union[int, Nummer],
                sorte: Union[str, Sorte],
                holzart: Union[str, Holzart],
                laengeVon: Union[float, LaengeVon],
                laengeBis: Union[float, LaengeBis],
                mindestzopf: Union[float, Mindestzopf],
                maxDurchmesser: Union[float, MaxDurchmesser],
                laengenzugabe: Union[float, Laengenzugabe],
                mengenschaetzung: Union[float, Mengenschaetzung],
                sortimentstyp: Optional[Union[str, Sortimentstyp]] = None,
                gueteklasseVon: Optional[Union[str, GueteklasseVon]] = None,
                gueteklasseBis: Optional[Union[str, GueteklasseBis]] = None,
                bemerkung: Optional[Union[str, Bemerkung]] = None,
                ergebnis: Optional[Ergebnis] = None,
                kaeufer: Optional[Kaeufer] = None,
                id_short: str = "Sortiment",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if nummer and not isinstance(nummer, SubmodelElement):
                    nummer = self.Nummer(nummer)

                # Build a submodel element if a raw value was passed in the argument
                if sortimentstyp and not isinstance(sortimentstyp, SubmodelElement):
                    sortimentstyp = self.Sortimentstyp(sortimentstyp)

                # Build a submodel element if a raw value was passed in the argument
                if sorte and not isinstance(sorte, SubmodelElement):
                    sorte = self.Sorte(sorte)

                # Build a submodel element if a raw value was passed in the argument
                if holzart and not isinstance(holzart, SubmodelElement):
                    holzart = self.Holzart(holzart)

                # Build a submodel element if a raw value was passed in the argument
                if gueteklasseVon and not isinstance(gueteklasseVon, SubmodelElement):
                    gueteklasseVon = self.GueteklasseVon(gueteklasseVon)

                # Build a submodel element if a raw value was passed in the argument
                if gueteklasseBis and not isinstance(gueteklasseBis, SubmodelElement):
                    gueteklasseBis = self.GueteklasseBis(gueteklasseBis)

                # Build a submodel element if a raw value was passed in the argument
                if laengeVon and not isinstance(laengeVon, SubmodelElement):
                    laengeVon = self.LaengeVon(laengeVon)

                # Build a submodel element if a raw value was passed in the argument
                if laengeBis and not isinstance(laengeBis, SubmodelElement):
                    laengeBis = self.LaengeBis(laengeBis)

                # Build a submodel element if a raw value was passed in the argument
                if mindestzopf and not isinstance(mindestzopf, SubmodelElement):
                    mindestzopf = self.Mindestzopf(mindestzopf)

                # Build a submodel element if a raw value was passed in the argument
                if maxDurchmesser and not isinstance(maxDurchmesser, SubmodelElement):
                    maxDurchmesser = self.MaxDurchmesser(maxDurchmesser)

                # Build a submodel element if a raw value was passed in the argument
                if laengenzugabe and not isinstance(laengenzugabe, SubmodelElement):
                    laengenzugabe = self.Laengenzugabe(laengenzugabe)

                # Build a submodel element if a raw value was passed in the argument
                if mengenschaetzung and not isinstance(
                    mengenschaetzung, SubmodelElement
                ):
                    mengenschaetzung = self.Mengenschaetzung(mengenschaetzung)

                # Build a submodel element if a raw value was passed in the argument
                if bemerkung and not isinstance(bemerkung, SubmodelElement):
                    bemerkung = self.Bemerkung(bemerkung)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    nummer,
                    sortimentstyp,
                    sorte,
                    holzart,
                    gueteklasseVon,
                    gueteklasseBis,
                    laengeVon,
                    laengeBis,
                    mindestzopf,
                    maxDurchmesser,
                    laengenzugabe,
                    mengenschaetzung,
                    bemerkung,
                    ergebnis,
                    kaeufer,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            sortiment: Optional[Iterable[Sortiment]] = None,
            id_short: str = "Sortimente",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [sortiment]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Ansprechpartner(SubmodelElementCollection):
        class Firmenname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Firmenname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Firmenname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Anrede(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Anrede",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Anrede",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Vorname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Vorname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Vorname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Nachname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Nachname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Nachname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Adresse(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Adresse",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Adresse",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Umsatzbesteuerung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Umsatzbesteuerung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Umsatzbesteuerung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class IBAN(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "IBAN",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/IBAN",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Steuernummer(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Steuernummer",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Steuernummer",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Telefonnummern(SubmodelElementList):
            class Telefon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Telefon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Telefonnummern/Telefon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                id_short: str = "Telefonnummern",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Telefonnummern",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if telefon and all([isinstance(i, str) for i in telefon]):
                    telefon = [self.Telefon(i) for i in telefon]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [telefon]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Emailadressen(SubmodelElementList):
            class Email(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Email",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Emailadressen/Email",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                email: Optional[Iterable[Union[str, Email]]] = None,
                id_short: str = "Emailadressen",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Emailadressen",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if email and all([isinstance(i, str) for i in email]):
                    email = [self.Email(i) for i in email]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [email]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            firmenname: Optional[Union[str, Firmenname]] = None,
            anrede: Optional[Union[str, Anrede]] = None,
            vorname: Optional[Union[str, Vorname]] = None,
            nachname: Optional[Union[str, Nachname]] = None,
            adresse: Optional[Union[str, Adresse]] = None,
            umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
            iBAN: Optional[Union[str, IBAN]] = None,
            steuernummer: Optional[Union[str, Steuernummer]] = None,
            telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
            emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
            id_short: str = "Ansprechpartner",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if firmenname and not isinstance(firmenname, SubmodelElement):
                firmenname = self.Firmenname(firmenname)

            # Build a submodel element if a raw value was passed in the argument
            if anrede and not isinstance(anrede, SubmodelElement):
                anrede = self.Anrede(anrede)

            # Build a submodel element if a raw value was passed in the argument
            if vorname and not isinstance(vorname, SubmodelElement):
                vorname = self.Vorname(vorname)

            # Build a submodel element if a raw value was passed in the argument
            if nachname and not isinstance(nachname, SubmodelElement):
                nachname = self.Nachname(nachname)

            # Build a submodel element if a raw value was passed in the argument
            if adresse and not isinstance(adresse, SubmodelElement):
                adresse = self.Adresse(adresse)

            # Build a submodel element if a raw value was passed in the argument
            if umsatzbesteuerung and not isinstance(umsatzbesteuerung, SubmodelElement):
                umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

            # Build a submodel element if a raw value was passed in the argument
            if iBAN and not isinstance(iBAN, SubmodelElement):
                iBAN = self.IBAN(iBAN)

            # Build a submodel element if a raw value was passed in the argument
            if steuernummer and not isinstance(steuernummer, SubmodelElement):
                steuernummer = self.Steuernummer(steuernummer)

            # Build a submodel element if a raw value was passed in the argument
            if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                telefonnummern = self.Telefonnummern(telefonnummern)

            # Build a submodel element if a raw value was passed in the argument
            if emailadressen and not isinstance(emailadressen, SubmodelElement):
                emailadressen = self.Emailadressen(emailadressen)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [
                firmenname,
                anrede,
                vorname,
                nachname,
                adresse,
                umsatzbesteuerung,
                iBAN,
                steuernummer,
                telefonnummern,
                emailadressen,
            ]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class UnternehmerHolzrueckung(SubmodelElementCollection):
        class Firmenname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Firmenname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Firmenname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Anrede(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Anrede",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Anrede",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Vorname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Vorname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Vorname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Nachname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Nachname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Nachname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Adresse(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Adresse",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Adresse",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Umsatzbesteuerung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Umsatzbesteuerung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Umsatzbesteuerung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class IBAN(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "IBAN",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/IBAN",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Steuernummer(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Steuernummer",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Steuernummer",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Telefonnummern(SubmodelElementList):
            class Telefon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Telefon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Telefonnummern/Telefon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                id_short: str = "Telefonnummern",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Telefonnummern",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if telefon and all([isinstance(i, str) for i in telefon]):
                    telefon = [self.Telefon(i) for i in telefon]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [telefon]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Emailadressen(SubmodelElementList):
            class Email(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Email",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Emailadressen/Email",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                email: Optional[Iterable[Union[str, Email]]] = None,
                id_short: str = "Emailadressen",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Emailadressen",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if email and all([isinstance(i, str) for i in email]):
                    email = [self.Email(i) for i in email]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [email]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            firmenname: Optional[Union[str, Firmenname]] = None,
            anrede: Optional[Union[str, Anrede]] = None,
            vorname: Optional[Union[str, Vorname]] = None,
            nachname: Optional[Union[str, Nachname]] = None,
            adresse: Optional[Union[str, Adresse]] = None,
            umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
            iBAN: Optional[Union[str, IBAN]] = None,
            steuernummer: Optional[Union[str, Steuernummer]] = None,
            telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
            emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
            id_short: str = "UnternehmerHolzrueckung",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if firmenname and not isinstance(firmenname, SubmodelElement):
                firmenname = self.Firmenname(firmenname)

            # Build a submodel element if a raw value was passed in the argument
            if anrede and not isinstance(anrede, SubmodelElement):
                anrede = self.Anrede(anrede)

            # Build a submodel element if a raw value was passed in the argument
            if vorname and not isinstance(vorname, SubmodelElement):
                vorname = self.Vorname(vorname)

            # Build a submodel element if a raw value was passed in the argument
            if nachname and not isinstance(nachname, SubmodelElement):
                nachname = self.Nachname(nachname)

            # Build a submodel element if a raw value was passed in the argument
            if adresse and not isinstance(adresse, SubmodelElement):
                adresse = self.Adresse(adresse)

            # Build a submodel element if a raw value was passed in the argument
            if umsatzbesteuerung and not isinstance(umsatzbesteuerung, SubmodelElement):
                umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

            # Build a submodel element if a raw value was passed in the argument
            if iBAN and not isinstance(iBAN, SubmodelElement):
                iBAN = self.IBAN(iBAN)

            # Build a submodel element if a raw value was passed in the argument
            if steuernummer and not isinstance(steuernummer, SubmodelElement):
                steuernummer = self.Steuernummer(steuernummer)

            # Build a submodel element if a raw value was passed in the argument
            if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                telefonnummern = self.Telefonnummern(telefonnummern)

            # Build a submodel element if a raw value was passed in the argument
            if emailadressen and not isinstance(emailadressen, SubmodelElement):
                emailadressen = self.Emailadressen(emailadressen)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [
                firmenname,
                anrede,
                vorname,
                nachname,
                adresse,
                umsatzbesteuerung,
                iBAN,
                steuernummer,
                telefonnummern,
                emailadressen,
            ]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class UnternehmerHolzernte(SubmodelElementCollection):
        class Firmenname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Firmenname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Firmenname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Anrede(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Anrede",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Anrede",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Vorname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Vorname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Vorname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Nachname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Nachname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Nachname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Adresse(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Adresse",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Adresse",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Umsatzbesteuerung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Umsatzbesteuerung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Umsatzbesteuerung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class IBAN(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "IBAN",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/IBAN",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Steuernummer(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Steuernummer",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Steuernummer",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Telefonnummern(SubmodelElementList):
            class Telefon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Telefon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Telefonnummern/Telefon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                id_short: str = "Telefonnummern",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Telefonnummern",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if telefon and all([isinstance(i, str) for i in telefon]):
                    telefon = [self.Telefon(i) for i in telefon]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [telefon]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Emailadressen(SubmodelElementList):
            class Email(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Email",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Emailadressen/Email",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                email: Optional[Iterable[Union[str, Email]]] = None,
                id_short: str = "Emailadressen",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Emailadressen",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if email and all([isinstance(i, str) for i in email]):
                    email = [self.Email(i) for i in email]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [email]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            firmenname: Optional[Union[str, Firmenname]] = None,
            anrede: Optional[Union[str, Anrede]] = None,
            vorname: Optional[Union[str, Vorname]] = None,
            nachname: Optional[Union[str, Nachname]] = None,
            adresse: Optional[Union[str, Adresse]] = None,
            umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
            iBAN: Optional[Union[str, IBAN]] = None,
            steuernummer: Optional[Union[str, Steuernummer]] = None,
            telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
            emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
            id_short: str = "UnternehmerHolzernte",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if firmenname and not isinstance(firmenname, SubmodelElement):
                firmenname = self.Firmenname(firmenname)

            # Build a submodel element if a raw value was passed in the argument
            if anrede and not isinstance(anrede, SubmodelElement):
                anrede = self.Anrede(anrede)

            # Build a submodel element if a raw value was passed in the argument
            if vorname and not isinstance(vorname, SubmodelElement):
                vorname = self.Vorname(vorname)

            # Build a submodel element if a raw value was passed in the argument
            if nachname and not isinstance(nachname, SubmodelElement):
                nachname = self.Nachname(nachname)

            # Build a submodel element if a raw value was passed in the argument
            if adresse and not isinstance(adresse, SubmodelElement):
                adresse = self.Adresse(adresse)

            # Build a submodel element if a raw value was passed in the argument
            if umsatzbesteuerung and not isinstance(umsatzbesteuerung, SubmodelElement):
                umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

            # Build a submodel element if a raw value was passed in the argument
            if iBAN and not isinstance(iBAN, SubmodelElement):
                iBAN = self.IBAN(iBAN)

            # Build a submodel element if a raw value was passed in the argument
            if steuernummer and not isinstance(steuernummer, SubmodelElement):
                steuernummer = self.Steuernummer(steuernummer)

            # Build a submodel element if a raw value was passed in the argument
            if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                telefonnummern = self.Telefonnummern(telefonnummern)

            # Build a submodel element if a raw value was passed in the argument
            if emailadressen and not isinstance(emailadressen, SubmodelElement):
                emailadressen = self.Emailadressen(emailadressen)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [
                firmenname,
                anrede,
                vorname,
                nachname,
                adresse,
                umsatzbesteuerung,
                iBAN,
                steuernummer,
                telefonnummern,
                emailadressen,
            ]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class ZuFaellendeBaeume(ReferenceElement):
        def __init__(
            self,
            value: Reference,
            id_short: str = "ZuFaellendeBaeume",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/ZuFaellendeBaeume",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        hiebsnummer: Union[str, Hiebsnummer],
        kurzbeschreibung: Union[str, Kurzbeschreibung],
        sicherheitshinweise: Optional[Union[str, Sicherheitshinweise]] = None,
        naturschutzhinweise: Optional[Union[str, Naturschutzhinweise]] = None,
        karte: Optional[Union[str, Karte]] = None,
        auftragsstatus: Optional[Union[str, Auftragsstatus]] = None,
        sortimente: Optional[Sortimente] = None,
        ansprechpartner: Optional[Ansprechpartner] = None,
        unternehmerHolzrueckung: Optional[UnternehmerHolzrueckung] = None,
        unternehmerHolzernte: Optional[UnternehmerHolzernte] = None,
        zuFaellendeBaeume: Optional[Union[Reference, ZuFaellendeBaeume]] = None,
        id_short: str = "Arbeitsauftrag",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if hiebsnummer and not isinstance(hiebsnummer, SubmodelElement):
            hiebsnummer = self.Hiebsnummer(hiebsnummer)

        # Build a submodel element if a raw value was passed in the argument
        if kurzbeschreibung and not isinstance(kurzbeschreibung, SubmodelElement):
            kurzbeschreibung = self.Kurzbeschreibung(kurzbeschreibung)

        # Build a submodel element if a raw value was passed in the argument
        if sicherheitshinweise and not isinstance(sicherheitshinweise, SubmodelElement):
            sicherheitshinweise = self.Sicherheitshinweise(sicherheitshinweise)

        # Build a submodel element if a raw value was passed in the argument
        if naturschutzhinweise and not isinstance(naturschutzhinweise, SubmodelElement):
            naturschutzhinweise = self.Naturschutzhinweise(naturschutzhinweise)

        # Build a submodel element if a raw value was passed in the argument
        if karte and not isinstance(karte, SubmodelElement):
            karte = self.Karte(karte)

        # Build a submodel element if a raw value was passed in the argument
        if auftragsstatus and not isinstance(auftragsstatus, SubmodelElement):
            auftragsstatus = self.Auftragsstatus(auftragsstatus)

        # Build a submodel element if a raw value was passed in the argument
        if zuFaellendeBaeume and not isinstance(zuFaellendeBaeume, SubmodelElement):
            zuFaellendeBaeume = self.ZuFaellendeBaeume(zuFaellendeBaeume)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [
            hiebsnummer,
            kurzbeschreibung,
            sicherheitshinweise,
            naturschutzhinweise,
            karte,
            auftragsstatus,
            sortimente,
            ansprechpartner,
            unternehmerHolzrueckung,
            unternehmerHolzernte,
            zuFaellendeBaeume,
        ]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class Beobachtung(Submodel):
    class Name(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Name",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Name",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Beschreibung(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Beschreibung",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Beschreibung",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Umring(SubmodelElementCollection):
        class Name(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Name",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Name",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Beschreibung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Beschreibung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Beschreibung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Koordinaten(SubmodelElementList):
            class Koordinate(SubmodelElementCollection):
                class X(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "x",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/x",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Y(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "y",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/y",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Alt(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "alt",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/alt",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Crs(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "crs",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/crs",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    x: Optional[Union[float, X]] = None,
                    y: Optional[Union[float, Y]] = None,
                    alt: Optional[Union[float, Alt]] = None,
                    crs: Optional[Union[str, Crs]] = None,
                    id_short: str = "Koordinate",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="OneToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if x and not isinstance(x, SubmodelElement):
                        x = self.X(x)

                    # Build a submodel element if a raw value was passed in the argument
                    if y and not isinstance(y, SubmodelElement):
                        y = self.Y(y)

                    # Build a submodel element if a raw value was passed in the argument
                    if alt and not isinstance(alt, SubmodelElement):
                        alt = self.Alt(alt)

                    # Build a submodel element if a raw value was passed in the argument
                    if crs and not isinstance(crs, SubmodelElement):
                        crs = self.Crs(crs)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [x, y, alt, crs]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                koordinate: Iterable[Koordinate],
                id_short: str = "Koordinaten",
                type_value_list_element: SubmodelElement = SubmodelElementCollection,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = None,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [koordinate]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            name: Union[str, Name],
            beschreibung: Union[str, Beschreibung],
            koordinaten: Koordinaten,
            id_short: str = "Umring",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if name and not isinstance(name, SubmodelElement):
                name = self.Name(name)

            # Build a submodel element if a raw value was passed in the argument
            if beschreibung and not isinstance(beschreibung, SubmodelElement):
                beschreibung = self.Beschreibung(beschreibung)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [name, beschreibung, koordinaten]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Position(SubmodelElementCollection):
        class Koordinate(SubmodelElementCollection):
            class X(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "x",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/x",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Y(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "y",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/y",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Alt(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "alt",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/alt",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Crs(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "crs",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/crs",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                x: Optional[Union[float, X]] = None,
                y: Optional[Union[float, Y]] = None,
                alt: Optional[Union[float, Alt]] = None,
                crs: Optional[Union[str, Crs]] = None,
                id_short: str = "Koordinate",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="OneToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if x and not isinstance(x, SubmodelElement):
                    x = self.X(x)

                # Build a submodel element if a raw value was passed in the argument
                if y and not isinstance(y, SubmodelElement):
                    y = self.Y(y)

                # Build a submodel element if a raw value was passed in the argument
                if alt and not isinstance(alt, SubmodelElement):
                    alt = self.Alt(alt)

                # Build a submodel element if a raw value was passed in the argument
                if crs and not isinstance(crs, SubmodelElement):
                    crs = self.Crs(crs)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [x, y, alt, crs]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Notiz(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Notiz",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Notiz",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            koordinate: Iterable[Koordinate],
            notiz: Optional[Union[str, Notiz]] = None,
            id_short: str = "Position",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if notiz and not isinstance(notiz, SubmodelElement):
                notiz = self.Notiz(notiz)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [koordinate, notiz]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        name: Union[str, Name],
        beschreibung: Union[str, Beschreibung],
        umring: Optional[Umring] = None,
        position: Optional[Position] = None,
        id_short: str = "Beobachtung",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if name and not isinstance(name, SubmodelElement):
            name = self.Name(name)

        # Build a submodel element if a raw value was passed in the argument
        if beschreibung and not isinstance(beschreibung, SubmodelElement):
            beschreibung = self.Beschreibung(beschreibung)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [name, beschreibung, umring, position]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class Bestandesdaten(Submodel):
    class Umring(SubmodelElementCollection):
        class Name(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Name",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Name",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Beschreibung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Beschreibung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Beschreibung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Koordinaten(SubmodelElementList):
            class Koordinate(SubmodelElementCollection):
                class X(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "x",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/x",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Y(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "y",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/y",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Alt(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "alt",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/alt",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Crs(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "crs",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/crs",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    x: Optional[Union[float, X]] = None,
                    y: Optional[Union[float, Y]] = None,
                    alt: Optional[Union[float, Alt]] = None,
                    crs: Optional[Union[str, Crs]] = None,
                    id_short: str = "Koordinate",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="OneToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if x and not isinstance(x, SubmodelElement):
                        x = self.X(x)

                    # Build a submodel element if a raw value was passed in the argument
                    if y and not isinstance(y, SubmodelElement):
                        y = self.Y(y)

                    # Build a submodel element if a raw value was passed in the argument
                    if alt and not isinstance(alt, SubmodelElement):
                        alt = self.Alt(alt)

                    # Build a submodel element if a raw value was passed in the argument
                    if crs and not isinstance(crs, SubmodelElement):
                        crs = self.Crs(crs)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [x, y, alt, crs]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                koordinate: Iterable[Koordinate],
                id_short: str = "Koordinaten",
                type_value_list_element: SubmodelElement = SubmodelElementCollection,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = None,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [koordinate]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            name: Union[str, Name],
            beschreibung: Union[str, Beschreibung],
            koordinaten: Koordinaten,
            id_short: str = "Umring",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if name and not isinstance(name, SubmodelElement):
                name = self.Name(name)

            # Build a submodel element if a raw value was passed in the argument
            if beschreibung and not isinstance(beschreibung, SubmodelElement):
                beschreibung = self.Beschreibung(beschreibung)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [name, beschreibung, koordinaten]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Waldbesitzer(SubmodelElementCollection):
        class Firmenname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Firmenname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Firmenname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Anrede(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Anrede",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Anrede",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Vorname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Vorname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Vorname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Nachname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Nachname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Nachname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Adresse(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Adresse",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Adresse",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Umsatzbesteuerung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Umsatzbesteuerung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Umsatzbesteuerung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class IBAN(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "IBAN",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/IBAN",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Steuernummer(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Steuernummer",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Steuernummer",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Telefonnummern(SubmodelElementList):
            class Telefon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Telefon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Telefonnummern/Telefon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                id_short: str = "Telefonnummern",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Telefonnummern",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if telefon and all([isinstance(i, str) for i in telefon]):
                    telefon = [self.Telefon(i) for i in telefon]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [telefon]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Emailadressen(SubmodelElementList):
            class Email(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Email",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Emailadressen/Email",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                email: Optional[Iterable[Union[str, Email]]] = None,
                id_short: str = "Emailadressen",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Emailadressen",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if email and all([isinstance(i, str) for i in email]):
                    email = [self.Email(i) for i in email]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [email]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            firmenname: Optional[Union[str, Firmenname]] = None,
            anrede: Optional[Union[str, Anrede]] = None,
            vorname: Optional[Union[str, Vorname]] = None,
            nachname: Optional[Union[str, Nachname]] = None,
            adresse: Optional[Union[str, Adresse]] = None,
            umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
            iBAN: Optional[Union[str, IBAN]] = None,
            steuernummer: Optional[Union[str, Steuernummer]] = None,
            telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
            emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
            id_short: str = "Waldbesitzer",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if firmenname and not isinstance(firmenname, SubmodelElement):
                firmenname = self.Firmenname(firmenname)

            # Build a submodel element if a raw value was passed in the argument
            if anrede and not isinstance(anrede, SubmodelElement):
                anrede = self.Anrede(anrede)

            # Build a submodel element if a raw value was passed in the argument
            if vorname and not isinstance(vorname, SubmodelElement):
                vorname = self.Vorname(vorname)

            # Build a submodel element if a raw value was passed in the argument
            if nachname and not isinstance(nachname, SubmodelElement):
                nachname = self.Nachname(nachname)

            # Build a submodel element if a raw value was passed in the argument
            if adresse and not isinstance(adresse, SubmodelElement):
                adresse = self.Adresse(adresse)

            # Build a submodel element if a raw value was passed in the argument
            if umsatzbesteuerung and not isinstance(umsatzbesteuerung, SubmodelElement):
                umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

            # Build a submodel element if a raw value was passed in the argument
            if iBAN and not isinstance(iBAN, SubmodelElement):
                iBAN = self.IBAN(iBAN)

            # Build a submodel element if a raw value was passed in the argument
            if steuernummer and not isinstance(steuernummer, SubmodelElement):
                steuernummer = self.Steuernummer(steuernummer)

            # Build a submodel element if a raw value was passed in the argument
            if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                telefonnummern = self.Telefonnummern(telefonnummern)

            # Build a submodel element if a raw value was passed in the argument
            if emailadressen and not isinstance(emailadressen, SubmodelElement):
                emailadressen = self.Emailadressen(emailadressen)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [
                firmenname,
                anrede,
                vorname,
                nachname,
                adresse,
                umsatzbesteuerung,
                iBAN,
                steuernummer,
                telefonnummern,
                emailadressen,
            ]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Baumarten(SubmodelElementList):
        class Baumart(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Baumart",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten/Baumart",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            baumart: Optional[Iterable[Union[str, Baumart]]] = None,
            id_short: str = "Baumarten",
            type_value_list_element: SubmodelElement = Property,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = str,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a list of submodel elements if a raw values were passed in the argument
            if baumart and all([isinstance(i, str) for i in baumart]):
                baumart = [self.Baumart(i) for i in baumart]

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [baumart]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        umring: Optional[Umring] = None,
        waldbesitzer: Optional[Waldbesitzer] = None,
        baumarten: Optional[Union[Iterable[str], Baumarten]] = None,
        id_short: str = "Bestandesdaten",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if baumarten and not isinstance(baumarten, SubmodelElement):
            baumarten = self.Baumarten(baumarten)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [umring, waldbesitzer, baumarten]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class Holzliste(Submodel):
    class Kopfdaten(SubmodelElementCollection):
        class Ernte(Range):
            def __init__(
                self,
                min: Date,
                max: Date,
                id_short: str = "Ernte",
                value_type: DataTypeDefXsd = Date,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Ernte",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    min=min,
                    max=max,
                    id_short=id_short,
                    value_type=value_type,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Lieferant(SubmodelElementCollection):
            class Firmenname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Firmenname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Firmenname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Anrede(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Anrede",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Anrede",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Vorname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Vorname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Vorname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Nachname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Nachname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Nachname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Adresse(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Adresse",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Adresse",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Umsatzbesteuerung(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Umsatzbesteuerung",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Umsatzbesteuerung",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class IBAN(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "IBAN",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/IBAN",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Steuernummer(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Steuernummer",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Steuernummer",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Telefonnummern(SubmodelElementList):
                class Telefon(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Telefon",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Telefonnummern/Telefon",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                    id_short: str = "Telefonnummern",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Telefonnummern",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if telefon and all([isinstance(i, str) for i in telefon]):
                        telefon = [self.Telefon(i) for i in telefon]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [telefon]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Emailadressen(SubmodelElementList):
                class Email(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Email",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Emailadressen/Email",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    email: Optional[Iterable[Union[str, Email]]] = None,
                    id_short: str = "Emailadressen",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Emailadressen",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if email and all([isinstance(i, str) for i in email]):
                        email = [self.Email(i) for i in email]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [email]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                firmenname: Optional[Union[str, Firmenname]] = None,
                anrede: Optional[Union[str, Anrede]] = None,
                vorname: Optional[Union[str, Vorname]] = None,
                nachname: Optional[Union[str, Nachname]] = None,
                adresse: Optional[Union[str, Adresse]] = None,
                umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
                iBAN: Optional[Union[str, IBAN]] = None,
                steuernummer: Optional[Union[str, Steuernummer]] = None,
                telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
                emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
                id_short: str = "Lieferant",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if firmenname and not isinstance(firmenname, SubmodelElement):
                    firmenname = self.Firmenname(firmenname)

                # Build a submodel element if a raw value was passed in the argument
                if anrede and not isinstance(anrede, SubmodelElement):
                    anrede = self.Anrede(anrede)

                # Build a submodel element if a raw value was passed in the argument
                if vorname and not isinstance(vorname, SubmodelElement):
                    vorname = self.Vorname(vorname)

                # Build a submodel element if a raw value was passed in the argument
                if nachname and not isinstance(nachname, SubmodelElement):
                    nachname = self.Nachname(nachname)

                # Build a submodel element if a raw value was passed in the argument
                if adresse and not isinstance(adresse, SubmodelElement):
                    adresse = self.Adresse(adresse)

                # Build a submodel element if a raw value was passed in the argument
                if umsatzbesteuerung and not isinstance(
                    umsatzbesteuerung, SubmodelElement
                ):
                    umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

                # Build a submodel element if a raw value was passed in the argument
                if iBAN and not isinstance(iBAN, SubmodelElement):
                    iBAN = self.IBAN(iBAN)

                # Build a submodel element if a raw value was passed in the argument
                if steuernummer and not isinstance(steuernummer, SubmodelElement):
                    steuernummer = self.Steuernummer(steuernummer)

                # Build a submodel element if a raw value was passed in the argument
                if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                    telefonnummern = self.Telefonnummern(telefonnummern)

                # Build a submodel element if a raw value was passed in the argument
                if emailadressen and not isinstance(emailadressen, SubmodelElement):
                    emailadressen = self.Emailadressen(emailadressen)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    firmenname,
                    anrede,
                    vorname,
                    nachname,
                    adresse,
                    umsatzbesteuerung,
                    iBAN,
                    steuernummer,
                    telefonnummern,
                    emailadressen,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Kaeufer(SubmodelElementCollection):
            class Firmenname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Firmenname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Firmenname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Anrede(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Anrede",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Anrede",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Vorname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Vorname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Vorname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Nachname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Nachname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Nachname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Adresse(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Adresse",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Adresse",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Umsatzbesteuerung(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Umsatzbesteuerung",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Umsatzbesteuerung",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class IBAN(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "IBAN",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/IBAN",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Steuernummer(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Steuernummer",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Steuernummer",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Telefonnummern(SubmodelElementList):
                class Telefon(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Telefon",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Telefonnummern/Telefon",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                    id_short: str = "Telefonnummern",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Telefonnummern",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if telefon and all([isinstance(i, str) for i in telefon]):
                        telefon = [self.Telefon(i) for i in telefon]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [telefon]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Emailadressen(SubmodelElementList):
                class Email(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Email",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Emailadressen/Email",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    email: Optional[Iterable[Union[str, Email]]] = None,
                    id_short: str = "Emailadressen",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Emailadressen",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if email and all([isinstance(i, str) for i in email]):
                        email = [self.Email(i) for i in email]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [email]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                firmenname: Optional[Union[str, Firmenname]] = None,
                anrede: Optional[Union[str, Anrede]] = None,
                vorname: Optional[Union[str, Vorname]] = None,
                nachname: Optional[Union[str, Nachname]] = None,
                adresse: Optional[Union[str, Adresse]] = None,
                umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
                iBAN: Optional[Union[str, IBAN]] = None,
                steuernummer: Optional[Union[str, Steuernummer]] = None,
                telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
                emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
                id_short: str = "Kaeufer",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if firmenname and not isinstance(firmenname, SubmodelElement):
                    firmenname = self.Firmenname(firmenname)

                # Build a submodel element if a raw value was passed in the argument
                if anrede and not isinstance(anrede, SubmodelElement):
                    anrede = self.Anrede(anrede)

                # Build a submodel element if a raw value was passed in the argument
                if vorname and not isinstance(vorname, SubmodelElement):
                    vorname = self.Vorname(vorname)

                # Build a submodel element if a raw value was passed in the argument
                if nachname and not isinstance(nachname, SubmodelElement):
                    nachname = self.Nachname(nachname)

                # Build a submodel element if a raw value was passed in the argument
                if adresse and not isinstance(adresse, SubmodelElement):
                    adresse = self.Adresse(adresse)

                # Build a submodel element if a raw value was passed in the argument
                if umsatzbesteuerung and not isinstance(
                    umsatzbesteuerung, SubmodelElement
                ):
                    umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

                # Build a submodel element if a raw value was passed in the argument
                if iBAN and not isinstance(iBAN, SubmodelElement):
                    iBAN = self.IBAN(iBAN)

                # Build a submodel element if a raw value was passed in the argument
                if steuernummer and not isinstance(steuernummer, SubmodelElement):
                    steuernummer = self.Steuernummer(steuernummer)

                # Build a submodel element if a raw value was passed in the argument
                if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                    telefonnummern = self.Telefonnummern(telefonnummern)

                # Build a submodel element if a raw value was passed in the argument
                if emailadressen and not isinstance(emailadressen, SubmodelElement):
                    emailadressen = self.Emailadressen(emailadressen)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    firmenname,
                    anrede,
                    vorname,
                    nachname,
                    adresse,
                    umsatzbesteuerung,
                    iBAN,
                    steuernummer,
                    telefonnummern,
                    emailadressen,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Fuhrmann(SubmodelElementCollection):
            class Firmenname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Firmenname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Firmenname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Anrede(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Anrede",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Anrede",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Vorname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Vorname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Vorname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Nachname(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Nachname",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Nachname",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Adresse(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Adresse",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Adresse",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Umsatzbesteuerung(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Umsatzbesteuerung",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Umsatzbesteuerung",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class IBAN(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "IBAN",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/IBAN",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Steuernummer(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Steuernummer",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Steuernummer",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Telefonnummern(SubmodelElementList):
                class Telefon(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Telefon",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Telefonnummern/Telefon",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                    id_short: str = "Telefonnummern",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Telefonnummern",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if telefon and all([isinstance(i, str) for i in telefon]):
                        telefon = [self.Telefon(i) for i in telefon]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [telefon]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Emailadressen(SubmodelElementList):
                class Email(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Email",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Emailadressen/Email",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    email: Optional[Iterable[Union[str, Email]]] = None,
                    id_short: str = "Emailadressen",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Emailadressen",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if email and all([isinstance(i, str) for i in email]):
                        email = [self.Email(i) for i in email]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [email]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                firmenname: Optional[Union[str, Firmenname]] = None,
                anrede: Optional[Union[str, Anrede]] = None,
                vorname: Optional[Union[str, Vorname]] = None,
                nachname: Optional[Union[str, Nachname]] = None,
                adresse: Optional[Union[str, Adresse]] = None,
                umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
                iBAN: Optional[Union[str, IBAN]] = None,
                steuernummer: Optional[Union[str, Steuernummer]] = None,
                telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
                emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
                id_short: str = "Fuhrmann",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if firmenname and not isinstance(firmenname, SubmodelElement):
                    firmenname = self.Firmenname(firmenname)

                # Build a submodel element if a raw value was passed in the argument
                if anrede and not isinstance(anrede, SubmodelElement):
                    anrede = self.Anrede(anrede)

                # Build a submodel element if a raw value was passed in the argument
                if vorname and not isinstance(vorname, SubmodelElement):
                    vorname = self.Vorname(vorname)

                # Build a submodel element if a raw value was passed in the argument
                if nachname and not isinstance(nachname, SubmodelElement):
                    nachname = self.Nachname(nachname)

                # Build a submodel element if a raw value was passed in the argument
                if adresse and not isinstance(adresse, SubmodelElement):
                    adresse = self.Adresse(adresse)

                # Build a submodel element if a raw value was passed in the argument
                if umsatzbesteuerung and not isinstance(
                    umsatzbesteuerung, SubmodelElement
                ):
                    umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

                # Build a submodel element if a raw value was passed in the argument
                if iBAN and not isinstance(iBAN, SubmodelElement):
                    iBAN = self.IBAN(iBAN)

                # Build a submodel element if a raw value was passed in the argument
                if steuernummer and not isinstance(steuernummer, SubmodelElement):
                    steuernummer = self.Steuernummer(steuernummer)

                # Build a submodel element if a raw value was passed in the argument
                if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                    telefonnummern = self.Telefonnummern(telefonnummern)

                # Build a submodel element if a raw value was passed in the argument
                if emailadressen and not isinstance(emailadressen, SubmodelElement):
                    emailadressen = self.Emailadressen(emailadressen)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    firmenname,
                    anrede,
                    vorname,
                    nachname,
                    adresse,
                    umsatzbesteuerung,
                    iBAN,
                    steuernummer,
                    telefonnummern,
                    emailadressen,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Projekt(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Projekt",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Projekt",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Hieb(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Hieb",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Hieb",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Notiz(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Notiz",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Notiz",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            ernte: Optional[Union[Tuple[Date, Date], Ernte]] = None,
            lieferant: Optional[Lieferant] = None,
            kaeufer: Optional[Kaeufer] = None,
            fuhrmann: Optional[Fuhrmann] = None,
            projekt: Optional[Union[str, Projekt]] = None,
            hieb: Optional[Union[str, Hieb]] = None,
            notiz: Optional[Union[str, Notiz]] = None,
            id_short: str = "Kopfdaten",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if ernte and not isinstance(ernte, SubmodelElement):
                ernte = self.Ernte(ernte)

            # Build a submodel element if a raw value was passed in the argument
            if projekt and not isinstance(projekt, SubmodelElement):
                projekt = self.Projekt(projekt)

            # Build a submodel element if a raw value was passed in the argument
            if hieb and not isinstance(hieb, SubmodelElement):
                hieb = self.Hieb(hieb)

            # Build a submodel element if a raw value was passed in the argument
            if notiz and not isinstance(notiz, SubmodelElement):
                notiz = self.Notiz(notiz)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [ernte, lieferant, kaeufer, fuhrmann, projekt, hieb, notiz]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Sortimentstyp(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Sortimentstyp",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Sortimentstyp",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Sorte(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Sorte",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Sorte",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Holzart(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Holzart",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Holzart",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class PolterListe(SubmodelElementList):
        class Polter(SubmodelElementCollection):
            class Polternummer(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Polternummer",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Polternummer",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Vermessungsverfahren(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Vermessungsverfahren",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Vermessungsverfahren",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Polterstatus(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Polterstatus",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Polterstatus",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Fotos(SubmodelElementList):
                class Foto(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Foto",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Fotos/Foto",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    foto: Optional[Iterable[Union[str, Foto]]] = None,
                    id_short: str = "Fotos",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Fotos",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if foto and all([isinstance(i, str) for i in foto]):
                        foto = [self.Foto(i) for i in foto]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [foto]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Videos(SubmodelElementList):
                class Video(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Video",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Videos/Video",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    video: Optional[Iterable[Union[str, Video]]] = None,
                    id_short: str = "Videos",
                    type_value_list_element: SubmodelElement = Property,
                    semantic_id_list_element: Optional[Reference] = None,
                    value_type_list_element: Optional[DataTypeDefXsd] = str,
                    order_relevant: bool = True,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Videos",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a list of submodel elements if a raw values were passed in the argument
                    if video and all([isinstance(i, str) for i in video]):
                        video = [self.Video(i) for i in video]

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [video]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        type_value_list_element=type_value_list_element,
                        semantic_id_list_element=semantic_id_list_element,
                        value_type_list_element=value_type_list_element,
                        order_relevant=order_relevant,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Messergebnis_EinzelstammListe(SubmodelElementCollection):
                class Einzelstaemme(SubmodelElementList):
                    class Einzelstamm(SubmodelElementCollection):
                        class Menge(Property):
                            def __init__(
                                self,
                                value: float,
                                id_short: str = "Menge",
                                value_type: DataTypeDefXsd = float,
                                value_id: Optional[Reference] = None,
                                display_name: Optional[LangStringSet] = None,
                                category: Optional[str] = None,
                                description: Optional[LangStringSet] = None,
                                semantic_id: Optional[Reference] = GlobalReference(
                                    key=(
                                        Key(
                                            type_=KeyTypes.GLOBAL_REFERENCE,
                                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Menge",
                                        ),
                                    ),
                                    referred_semantic_id=None,
                                ),
                                qualifier: Iterable[Qualifier] = None,
                                kind: ModelingKind = ModelingKind.INSTANCE,
                                extension: Iterable[Extension] = (),
                                supplemental_semantic_id: Iterable[Reference] = (),
                                embedded_data_specifications: Iterable[
                                    EmbeddedDataSpecification
                                ] = None,
                            ):
                                if qualifier is None:
                                    qualifier = (
                                        Qualifier(
                                            type_="Cardinality",
                                            value_type=str,
                                            value="One",
                                            value_id=None,
                                            kind=QualifierKind.CONCEPT_QUALIFIER,
                                            semantic_id=None,
                                            supplemental_semantic_id=(),
                                        ),
                                    )

                                if embedded_data_specifications is None:
                                    embedded_data_specifications = []

                                super().__init__(
                                    value=value,
                                    id_short=id_short,
                                    value_type=value_type,
                                    value_id=value_id,
                                    display_name=display_name,
                                    category=category,
                                    description=description,
                                    semantic_id=semantic_id,
                                    qualifier=qualifier,
                                    kind=kind,
                                    extension=extension,
                                    supplemental_semantic_id=supplemental_semantic_id,
                                    embedded_data_specifications=embedded_data_specifications,
                                )

                        class Stammlaenge(Property):
                            def __init__(
                                self,
                                value: float,
                                id_short: str = "Stammlaenge",
                                value_type: DataTypeDefXsd = float,
                                value_id: Optional[Reference] = None,
                                display_name: Optional[LangStringSet] = None,
                                category: Optional[str] = None,
                                description: Optional[LangStringSet] = None,
                                semantic_id: Optional[Reference] = GlobalReference(
                                    key=(
                                        Key(
                                            type_=KeyTypes.GLOBAL_REFERENCE,
                                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Stammlaenge",
                                        ),
                                    ),
                                    referred_semantic_id=None,
                                ),
                                qualifier: Iterable[Qualifier] = None,
                                kind: ModelingKind = ModelingKind.INSTANCE,
                                extension: Iterable[Extension] = (),
                                supplemental_semantic_id: Iterable[Reference] = (),
                                embedded_data_specifications: Iterable[
                                    EmbeddedDataSpecification
                                ] = None,
                            ):
                                if qualifier is None:
                                    qualifier = (
                                        Qualifier(
                                            type_="Cardinality",
                                            value_type=str,
                                            value="One",
                                            value_id=None,
                                            kind=QualifierKind.CONCEPT_QUALIFIER,
                                            semantic_id=None,
                                            supplemental_semantic_id=(),
                                        ),
                                    )

                                if embedded_data_specifications is None:
                                    embedded_data_specifications = []

                                super().__init__(
                                    value=value,
                                    id_short=id_short,
                                    value_type=value_type,
                                    value_id=value_id,
                                    display_name=display_name,
                                    category=category,
                                    description=description,
                                    semantic_id=semantic_id,
                                    qualifier=qualifier,
                                    kind=kind,
                                    extension=extension,
                                    supplemental_semantic_id=supplemental_semantic_id,
                                    embedded_data_specifications=embedded_data_specifications,
                                )

                        class Stammnummer(Property):
                            def __init__(
                                self,
                                value: str,
                                id_short: str = "Stammnummer",
                                value_type: DataTypeDefXsd = str,
                                value_id: Optional[Reference] = None,
                                display_name: Optional[LangStringSet] = None,
                                category: Optional[str] = None,
                                description: Optional[LangStringSet] = None,
                                semantic_id: Optional[Reference] = GlobalReference(
                                    key=(
                                        Key(
                                            type_=KeyTypes.GLOBAL_REFERENCE,
                                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Stammnummer",
                                        ),
                                    ),
                                    referred_semantic_id=None,
                                ),
                                qualifier: Iterable[Qualifier] = None,
                                kind: ModelingKind = ModelingKind.INSTANCE,
                                extension: Iterable[Extension] = (),
                                supplemental_semantic_id: Iterable[Reference] = (),
                                embedded_data_specifications: Iterable[
                                    EmbeddedDataSpecification
                                ] = None,
                            ):
                                if qualifier is None:
                                    qualifier = (
                                        Qualifier(
                                            type_="Cardinality",
                                            value_type=str,
                                            value="One",
                                            value_id=None,
                                            kind=QualifierKind.CONCEPT_QUALIFIER,
                                            semantic_id=None,
                                            supplemental_semantic_id=(),
                                        ),
                                    )

                                if embedded_data_specifications is None:
                                    embedded_data_specifications = []

                                super().__init__(
                                    value=value,
                                    id_short=id_short,
                                    value_type=value_type,
                                    value_id=value_id,
                                    display_name=display_name,
                                    category=category,
                                    description=description,
                                    semantic_id=semantic_id,
                                    qualifier=qualifier,
                                    kind=kind,
                                    extension=extension,
                                    supplemental_semantic_id=supplemental_semantic_id,
                                    embedded_data_specifications=embedded_data_specifications,
                                )

                        class Mittendurchmesser(Property):
                            def __init__(
                                self,
                                value: float,
                                id_short: str = "Mittendurchmesser",
                                value_type: DataTypeDefXsd = float,
                                value_id: Optional[Reference] = None,
                                display_name: Optional[LangStringSet] = None,
                                category: Optional[str] = None,
                                description: Optional[LangStringSet] = None,
                                semantic_id: Optional[Reference] = GlobalReference(
                                    key=(
                                        Key(
                                            type_=KeyTypes.GLOBAL_REFERENCE,
                                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Mittendurchmesser",
                                        ),
                                    ),
                                    referred_semantic_id=None,
                                ),
                                qualifier: Iterable[Qualifier] = None,
                                kind: ModelingKind = ModelingKind.INSTANCE,
                                extension: Iterable[Extension] = (),
                                supplemental_semantic_id: Iterable[Reference] = (),
                                embedded_data_specifications: Iterable[
                                    EmbeddedDataSpecification
                                ] = None,
                            ):
                                if qualifier is None:
                                    qualifier = (
                                        Qualifier(
                                            type_="Cardinality",
                                            value_type=str,
                                            value="One",
                                            value_id=None,
                                            kind=QualifierKind.CONCEPT_QUALIFIER,
                                            semantic_id=None,
                                            supplemental_semantic_id=(),
                                        ),
                                    )

                                if embedded_data_specifications is None:
                                    embedded_data_specifications = []

                                super().__init__(
                                    value=value,
                                    id_short=id_short,
                                    value_type=value_type,
                                    value_id=value_id,
                                    display_name=display_name,
                                    category=category,
                                    description=description,
                                    semantic_id=semantic_id,
                                    qualifier=qualifier,
                                    kind=kind,
                                    extension=extension,
                                    supplemental_semantic_id=supplemental_semantic_id,
                                    embedded_data_specifications=embedded_data_specifications,
                                )

                        class Gueteklasse(Property):
                            def __init__(
                                self,
                                value: str,
                                id_short: str = "Gueteklasse",
                                value_type: DataTypeDefXsd = str,
                                value_id: Optional[Reference] = None,
                                display_name: Optional[LangStringSet] = None,
                                category: Optional[str] = None,
                                description: Optional[LangStringSet] = None,
                                semantic_id: Optional[Reference] = GlobalReference(
                                    key=(
                                        Key(
                                            type_=KeyTypes.GLOBAL_REFERENCE,
                                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Gueteklasse",
                                        ),
                                    ),
                                    referred_semantic_id=None,
                                ),
                                qualifier: Iterable[Qualifier] = None,
                                kind: ModelingKind = ModelingKind.INSTANCE,
                                extension: Iterable[Extension] = (),
                                supplemental_semantic_id: Iterable[Reference] = (),
                                embedded_data_specifications: Iterable[
                                    EmbeddedDataSpecification
                                ] = None,
                            ):
                                if qualifier is None:
                                    qualifier = (
                                        Qualifier(
                                            type_="Cardinality",
                                            value_type=str,
                                            value="One",
                                            value_id=None,
                                            kind=QualifierKind.CONCEPT_QUALIFIER,
                                            semantic_id=None,
                                            supplemental_semantic_id=(),
                                        ),
                                    )

                                if embedded_data_specifications is None:
                                    embedded_data_specifications = []

                                super().__init__(
                                    value=value,
                                    id_short=id_short,
                                    value_type=value_type,
                                    value_id=value_id,
                                    display_name=display_name,
                                    category=category,
                                    description=description,
                                    semantic_id=semantic_id,
                                    qualifier=qualifier,
                                    kind=kind,
                                    extension=extension,
                                    supplemental_semantic_id=supplemental_semantic_id,
                                    embedded_data_specifications=embedded_data_specifications,
                                )

                        class Klammerstammabschnittsnummer(Property):
                            def __init__(
                                self,
                                value: str,
                                id_short: str = "Klammerstammabschnittsnummer",
                                value_type: DataTypeDefXsd = str,
                                value_id: Optional[Reference] = None,
                                display_name: Optional[LangStringSet] = None,
                                category: Optional[str] = None,
                                description: Optional[LangStringSet] = None,
                                semantic_id: Optional[Reference] = GlobalReference(
                                    key=(
                                        Key(
                                            type_=KeyTypes.GLOBAL_REFERENCE,
                                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Klammerstammabschnittsnummer",
                                        ),
                                    ),
                                    referred_semantic_id=None,
                                ),
                                qualifier: Iterable[Qualifier] = None,
                                kind: ModelingKind = ModelingKind.INSTANCE,
                                extension: Iterable[Extension] = (),
                                supplemental_semantic_id: Iterable[Reference] = (),
                                embedded_data_specifications: Iterable[
                                    EmbeddedDataSpecification
                                ] = None,
                            ):
                                if qualifier is None:
                                    qualifier = (
                                        Qualifier(
                                            type_="Cardinality",
                                            value_type=str,
                                            value="ZeroToOne",
                                            value_id=None,
                                            kind=QualifierKind.CONCEPT_QUALIFIER,
                                            semantic_id=None,
                                            supplemental_semantic_id=(),
                                        ),
                                    )

                                if embedded_data_specifications is None:
                                    embedded_data_specifications = []

                                super().__init__(
                                    value=value,
                                    id_short=id_short,
                                    value_type=value_type,
                                    value_id=value_id,
                                    display_name=display_name,
                                    category=category,
                                    description=description,
                                    semantic_id=semantic_id,
                                    qualifier=qualifier,
                                    kind=kind,
                                    extension=extension,
                                    supplemental_semantic_id=supplemental_semantic_id,
                                    embedded_data_specifications=embedded_data_specifications,
                                )

                        def __init__(
                            self,
                            menge: Union[float, Menge],
                            stammlaenge: Union[float, Stammlaenge],
                            stammnummer: Union[str, Stammnummer],
                            mittendurchmesser: Union[float, Mittendurchmesser],
                            gueteklasse: Union[str, Gueteklasse],
                            klammerstammabschnittsnummer: Optional[
                                Union[str, Klammerstammabschnittsnummer]
                            ] = None,
                            id_short: str = "Einzelstamm",
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="OneToMany",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            # Build a submodel element if a raw value was passed in the argument
                            if menge and not isinstance(menge, SubmodelElement):
                                menge = self.Menge(menge)

                            # Build a submodel element if a raw value was passed in the argument
                            if stammlaenge and not isinstance(
                                stammlaenge, SubmodelElement
                            ):
                                stammlaenge = self.Stammlaenge(stammlaenge)

                            # Build a submodel element if a raw value was passed in the argument
                            if stammnummer and not isinstance(
                                stammnummer, SubmodelElement
                            ):
                                stammnummer = self.Stammnummer(stammnummer)

                            # Build a submodel element if a raw value was passed in the argument
                            if mittendurchmesser and not isinstance(
                                mittendurchmesser, SubmodelElement
                            ):
                                mittendurchmesser = self.Mittendurchmesser(
                                    mittendurchmesser
                                )

                            # Build a submodel element if a raw value was passed in the argument
                            if gueteklasse and not isinstance(
                                gueteklasse, SubmodelElement
                            ):
                                gueteklasse = self.Gueteklasse(gueteklasse)

                            # Build a submodel element if a raw value was passed in the argument
                            if klammerstammabschnittsnummer and not isinstance(
                                klammerstammabschnittsnummer, SubmodelElement
                            ):
                                klammerstammabschnittsnummer = (
                                    self.Klammerstammabschnittsnummer(
                                        klammerstammabschnittsnummer
                                    )
                                )

                            # Add all passed/initialized submodel elements to a single list
                            embedded_submodel_elements = []
                            for se_arg in [
                                menge,
                                stammlaenge,
                                stammnummer,
                                mittendurchmesser,
                                gueteklasse,
                                klammerstammabschnittsnummer,
                            ]:
                                if se_arg is None:
                                    continue
                                elif isinstance(se_arg, SubmodelElement):
                                    embedded_submodel_elements.append(se_arg)
                                elif isinstance(se_arg, Iterable):
                                    for n, element in enumerate(se_arg):
                                        element.id_short = f"{element.id_short}{n}"
                                        embedded_submodel_elements.append(element)
                                else:
                                    raise TypeError(
                                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                                    )

                            super().__init__(
                                value=embedded_submodel_elements,
                                id_short=id_short,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    def __init__(
                        self,
                        einzelstamm: Iterable[Einzelstamm],
                        id_short: str = "Einzelstaemme",
                        type_value_list_element: SubmodelElement = SubmodelElementCollection,
                        semantic_id_list_element: Optional[Reference] = None,
                        value_type_list_element: Optional[DataTypeDefXsd] = None,
                        order_relevant: bool = True,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="One",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        # Add all passed/initialized submodel elements to a single list
                        embedded_submodel_elements = []
                        for se_arg in [einzelstamm]:
                            if se_arg is None:
                                continue
                            elif isinstance(se_arg, SubmodelElement):
                                embedded_submodel_elements.append(se_arg)
                            elif isinstance(se_arg, Iterable):
                                for n, element in enumerate(se_arg):
                                    element.id_short = f"{element.id_short}{n}"
                                    embedded_submodel_elements.append(element)
                            else:
                                raise TypeError(
                                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                                )

                        super().__init__(
                            value=embedded_submodel_elements,
                            id_short=id_short,
                            type_value_list_element=type_value_list_element,
                            semantic_id_list_element=semantic_id_list_element,
                            value_type_list_element=value_type_list_element,
                            order_relevant=order_relevant,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    einzelstaemme: Einzelstaemme,
                    id_short: str = "Messergebnis_EinzelstammListe",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [einzelstaemme]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Standort(SubmodelElementCollection):
                class Koordinate(SubmodelElementCollection):
                    class X(Property):
                        def __init__(
                            self,
                            value: float,
                            id_short: str = "x",
                            value_type: DataTypeDefXsd = float,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/x",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    class Y(Property):
                        def __init__(
                            self,
                            value: float,
                            id_short: str = "y",
                            value_type: DataTypeDefXsd = float,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/y",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    class Alt(Property):
                        def __init__(
                            self,
                            value: float,
                            id_short: str = "alt",
                            value_type: DataTypeDefXsd = float,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/alt",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    class Crs(Property):
                        def __init__(
                            self,
                            value: str,
                            id_short: str = "crs",
                            value_type: DataTypeDefXsd = str,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/crs",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    def __init__(
                        self,
                        x: Optional[Union[float, X]] = None,
                        y: Optional[Union[float, Y]] = None,
                        alt: Optional[Union[float, Alt]] = None,
                        crs: Optional[Union[str, Crs]] = None,
                        id_short: str = "Koordinate",
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="OneToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        # Build a submodel element if a raw value was passed in the argument
                        if x and not isinstance(x, SubmodelElement):
                            x = self.X(x)

                        # Build a submodel element if a raw value was passed in the argument
                        if y and not isinstance(y, SubmodelElement):
                            y = self.Y(y)

                        # Build a submodel element if a raw value was passed in the argument
                        if alt and not isinstance(alt, SubmodelElement):
                            alt = self.Alt(alt)

                        # Build a submodel element if a raw value was passed in the argument
                        if crs and not isinstance(crs, SubmodelElement):
                            crs = self.Crs(crs)

                        # Add all passed/initialized submodel elements to a single list
                        embedded_submodel_elements = []
                        for se_arg in [x, y, alt, crs]:
                            if se_arg is None:
                                continue
                            elif isinstance(se_arg, SubmodelElement):
                                embedded_submodel_elements.append(se_arg)
                            elif isinstance(se_arg, Iterable):
                                for n, element in enumerate(se_arg):
                                    element.id_short = f"{element.id_short}{n}"
                                    embedded_submodel_elements.append(element)
                            else:
                                raise TypeError(
                                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                                )

                        super().__init__(
                            value=embedded_submodel_elements,
                            id_short=id_short,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Notiz(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Notiz",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Notiz",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    koordinate: Iterable[Koordinate],
                    notiz: Optional[Union[str, Notiz]] = None,
                    id_short: str = "Standort",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if notiz and not isinstance(notiz, SubmodelElement):
                        notiz = self.Notiz(notiz)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [koordinate, notiz]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                polternummer: Union[str, Polternummer],
                vermessungsverfahren: Union[str, Vermessungsverfahren],
                polterstatus: Union[str, Polterstatus],
                fotos: Optional[Union[Iterable[str], Fotos]] = None,
                videos: Optional[Union[Iterable[str], Videos]] = None,
                messergebnis_EinzelstammListe: Optional[
                    Messergebnis_EinzelstammListe
                ] = None,
                standort: Optional[Standort] = None,
                id_short: str = "Polter",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="OneToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if polternummer and not isinstance(polternummer, SubmodelElement):
                    polternummer = self.Polternummer(polternummer)

                # Build a submodel element if a raw value was passed in the argument
                if vermessungsverfahren and not isinstance(
                    vermessungsverfahren, SubmodelElement
                ):
                    vermessungsverfahren = self.Vermessungsverfahren(
                        vermessungsverfahren
                    )

                # Build a submodel element if a raw value was passed in the argument
                if polterstatus and not isinstance(polterstatus, SubmodelElement):
                    polterstatus = self.Polterstatus(polterstatus)

                # Build a submodel element if a raw value was passed in the argument
                if fotos and not isinstance(fotos, SubmodelElement):
                    fotos = self.Fotos(fotos)

                # Build a submodel element if a raw value was passed in the argument
                if videos and not isinstance(videos, SubmodelElement):
                    videos = self.Videos(videos)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    polternummer,
                    vermessungsverfahren,
                    polterstatus,
                    fotos,
                    videos,
                    messergebnis_EinzelstammListe,
                    standort,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            polter: Iterable[Polter],
            id_short: str = "PolterListe",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [polter]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Preismatrix(SubmodelElementList):
        class Preismatrixeintrag(SubmodelElementCollection):
            class Preis(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "Preis",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/Preis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class MittendurchmesserVon(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "MittendurchmesserVon",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserVon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class MittendurchmesserBis(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "MittendurchmesserBis",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserBis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class GueteklasseVon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "GueteklasseVon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/GueteklasseVon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class GueteklasseBis(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "GueteklasseBis",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/GueteklasseBis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Sortimentstyp(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Sortimentstyp",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/Sortimentstyp",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Sorte(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Sorte",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag/Sorte",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                preis: Union[float, Preis],
                sorte: Union[str, Sorte],
                mittendurchmesserVon: Optional[
                    Union[float, MittendurchmesserVon]
                ] = None,
                mittendurchmesserBis: Optional[
                    Union[float, MittendurchmesserBis]
                ] = None,
                gueteklasseVon: Optional[Union[str, GueteklasseVon]] = None,
                gueteklasseBis: Optional[Union[str, GueteklasseBis]] = None,
                sortimentstyp: Optional[Union[str, Sortimentstyp]] = None,
                id_short: str = "Preismatrixeintrag",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix/Preismatrixeintrag",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if preis and not isinstance(preis, SubmodelElement):
                    preis = self.Preis(preis)

                # Build a submodel element if a raw value was passed in the argument
                if mittendurchmesserVon and not isinstance(
                    mittendurchmesserVon, SubmodelElement
                ):
                    mittendurchmesserVon = self.MittendurchmesserVon(
                        mittendurchmesserVon
                    )

                # Build a submodel element if a raw value was passed in the argument
                if mittendurchmesserBis and not isinstance(
                    mittendurchmesserBis, SubmodelElement
                ):
                    mittendurchmesserBis = self.MittendurchmesserBis(
                        mittendurchmesserBis
                    )

                # Build a submodel element if a raw value was passed in the argument
                if gueteklasseVon and not isinstance(gueteklasseVon, SubmodelElement):
                    gueteklasseVon = self.GueteklasseVon(gueteklasseVon)

                # Build a submodel element if a raw value was passed in the argument
                if gueteklasseBis and not isinstance(gueteklasseBis, SubmodelElement):
                    gueteklasseBis = self.GueteklasseBis(gueteklasseBis)

                # Build a submodel element if a raw value was passed in the argument
                if sortimentstyp and not isinstance(sortimentstyp, SubmodelElement):
                    sortimentstyp = self.Sortimentstyp(sortimentstyp)

                # Build a submodel element if a raw value was passed in the argument
                if sorte and not isinstance(sorte, SubmodelElement):
                    sorte = self.Sorte(sorte)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    preis,
                    mittendurchmesserVon,
                    mittendurchmesserBis,
                    gueteklasseVon,
                    gueteklasseBis,
                    sortimentstyp,
                    sorte,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            preismatrixeintrag: Optional[Iterable[Preismatrixeintrag]] = None,
            id_short: str = "Preismatrix",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Preismatrix",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [preismatrixeintrag]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        kopfdaten: Kopfdaten,
        sorte: Union[str, Sorte],
        holzart: Union[str, Holzart],
        sortimentstyp: Optional[Union[str, Sortimentstyp]] = None,
        polterListe: Optional[PolterListe] = None,
        preismatrix: Optional[Preismatrix] = None,
        id_short: str = "Holzliste",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Holzliste/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if sortimentstyp and not isinstance(sortimentstyp, SubmodelElement):
            sortimentstyp = self.Sortimentstyp(sortimentstyp)

        # Build a submodel element if a raw value was passed in the argument
        if sorte and not isinstance(sorte, SubmodelElement):
            sorte = self.Sorte(sorte)

        # Build a submodel element if a raw value was passed in the argument
        if holzart and not isinstance(holzart, SubmodelElement):
            holzart = self.Holzart(holzart)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [
            kopfdaten,
            sortimentstyp,
            sorte,
            holzart,
            polterListe,
            preismatrix,
        ]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class Holzpreisbereiche(Submodel):
    class Holzpreisbereich(SubmodelElementCollection):
        class Baumart(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Baumart",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/Baumart",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class PreisVon(Property):
            def __init__(
                self,
                value: float,
                id_short: str = "PreisVon",
                value_type: DataTypeDefXsd = float,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/PreisVon",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class PreisBis(Property):
            def __init__(
                self,
                value: float,
                id_short: str = "PreisBis",
                value_type: DataTypeDefXsd = float,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/PreisBis",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Sorte(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Sorte",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/Sorte",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Gueteklasse(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Gueteklasse",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/Gueteklasse",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class MittendurchmesserVon(Property):
            def __init__(
                self,
                value: float,
                id_short: str = "MittendurchmesserVon",
                value_type: DataTypeDefXsd = float,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/MittendurchmesserVon",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class MittendurchmesserBis(Property):
            def __init__(
                self,
                value: float,
                id_short: str = "MittendurchmesserBis",
                value_type: DataTypeDefXsd = float,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/MittendurchmesserBis",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class HolzlisteReference(ReferenceElement):
            def __init__(
                self,
                value: Reference,
                id_short: str = "HolzlisteReference",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/HolzlisteReference",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Datum(Property):
            def __init__(
                self,
                value: Date,
                id_short: str = "Datum",
                value_type: DataTypeDefXsd = Date,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/Datum",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Quelle(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Quelle",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich/Quelle",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            baumart: Union[str, Baumart],
            preisVon: Union[float, PreisVon],
            preisBis: Union[float, PreisBis],
            sorte: Optional[Union[str, Sorte]] = None,
            gueteklasse: Optional[Union[str, Gueteklasse]] = None,
            mittendurchmesserVon: Optional[Union[float, MittendurchmesserVon]] = None,
            mittendurchmesserBis: Optional[Union[float, MittendurchmesserBis]] = None,
            holzlisteReference: Optional[Union[Reference, HolzlisteReference]] = None,
            datum: Optional[Union[Date, Datum]] = None,
            quelle: Optional[Union[str, Quelle]] = None,
            id_short: str = "Holzpreisbereich",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereich",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToMany",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if baumart and not isinstance(baumart, SubmodelElement):
                baumart = self.Baumart(baumart)

            # Build a submodel element if a raw value was passed in the argument
            if preisVon and not isinstance(preisVon, SubmodelElement):
                preisVon = self.PreisVon(preisVon)

            # Build a submodel element if a raw value was passed in the argument
            if preisBis and not isinstance(preisBis, SubmodelElement):
                preisBis = self.PreisBis(preisBis)

            # Build a submodel element if a raw value was passed in the argument
            if sorte and not isinstance(sorte, SubmodelElement):
                sorte = self.Sorte(sorte)

            # Build a submodel element if a raw value was passed in the argument
            if gueteklasse and not isinstance(gueteklasse, SubmodelElement):
                gueteklasse = self.Gueteklasse(gueteklasse)

            # Build a submodel element if a raw value was passed in the argument
            if mittendurchmesserVon and not isinstance(
                mittendurchmesserVon, SubmodelElement
            ):
                mittendurchmesserVon = self.MittendurchmesserVon(mittendurchmesserVon)

            # Build a submodel element if a raw value was passed in the argument
            if mittendurchmesserBis and not isinstance(
                mittendurchmesserBis, SubmodelElement
            ):
                mittendurchmesserBis = self.MittendurchmesserBis(mittendurchmesserBis)

            # Build a submodel element if a raw value was passed in the argument
            if holzlisteReference and not isinstance(
                holzlisteReference, SubmodelElement
            ):
                holzlisteReference = self.HolzlisteReference(holzlisteReference)

            # Build a submodel element if a raw value was passed in the argument
            if datum and not isinstance(datum, SubmodelElement):
                datum = self.Datum(datum)

            # Build a submodel element if a raw value was passed in the argument
            if quelle and not isinstance(quelle, SubmodelElement):
                quelle = self.Quelle(quelle)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [
                baumart,
                preisVon,
                preisBis,
                sorte,
                gueteklasse,
                mittendurchmesserVon,
                mittendurchmesserBis,
                holzlisteReference,
                datum,
                quelle,
            ]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        holzpreisbereich: Optional[Iterable[Holzpreisbereich]] = None,
        id_short: str = "Holzpreisbereiche",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [holzpreisbereich]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class Verkaufslos(Submodel):
    class Verkaufsstatus(Property):
        def __init__(
            self,
            value: int,
            id_short: str = "Verkaufsstatus",
            value_type: DataTypeDefXsd = int,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaufsstatus",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Abrechnungsmass(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Abrechnungsmass",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Abrechnungsmass",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Bereitstellungsart(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Bereitstellungsart",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Bereitstellungsart",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class VerfuegbarAb(Property):
        def __init__(
            self,
            value: Date,
            id_short: str = "VerfuegbarAb",
            value_type: DataTypeDefXsd = Date,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/VerfuegbarAb",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class VerfuegbarBis(Property):
        def __init__(
            self,
            value: Date,
            id_short: str = "VerfuegbarBis",
            value_type: DataTypeDefXsd = Date,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/VerfuegbarBis",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Verkaeufer(SubmodelElementCollection):
        class Firmenname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Firmenname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Firmenname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Anrede(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Anrede",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Anrede",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Vorname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Vorname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Vorname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Nachname(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Nachname",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Nachname",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Adresse(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Adresse",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Adresse",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Umsatzbesteuerung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Umsatzbesteuerung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Umsatzbesteuerung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class IBAN(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "IBAN",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/IBAN",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Steuernummer(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Steuernummer",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Steuernummer",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Telefonnummern(SubmodelElementList):
            class Telefon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Telefon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Telefonnummern/Telefon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                telefon: Optional[Iterable[Union[str, Telefon]]] = None,
                id_short: str = "Telefonnummern",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Telefonnummern",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if telefon and all([isinstance(i, str) for i in telefon]):
                    telefon = [self.Telefon(i) for i in telefon]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [telefon]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Emailadressen(SubmodelElementList):
            class Email(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Email",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Emailadressen/Email",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                email: Optional[Iterable[Union[str, Email]]] = None,
                id_short: str = "Emailadressen",
                type_value_list_element: SubmodelElement = Property,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = str,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Emailadressen",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a list of submodel elements if a raw values were passed in the argument
                if email and all([isinstance(i, str) for i in email]):
                    email = [self.Email(i) for i in email]

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [email]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            firmenname: Optional[Union[str, Firmenname]] = None,
            anrede: Optional[Union[str, Anrede]] = None,
            vorname: Optional[Union[str, Vorname]] = None,
            nachname: Optional[Union[str, Nachname]] = None,
            adresse: Optional[Union[str, Adresse]] = None,
            umsatzbesteuerung: Optional[Union[str, Umsatzbesteuerung]] = None,
            iBAN: Optional[Union[str, IBAN]] = None,
            steuernummer: Optional[Union[str, Steuernummer]] = None,
            telefonnummern: Optional[Union[Iterable[str], Telefonnummern]] = None,
            emailadressen: Optional[Union[Iterable[str], Emailadressen]] = None,
            id_short: str = "Verkaeufer",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if firmenname and not isinstance(firmenname, SubmodelElement):
                firmenname = self.Firmenname(firmenname)

            # Build a submodel element if a raw value was passed in the argument
            if anrede and not isinstance(anrede, SubmodelElement):
                anrede = self.Anrede(anrede)

            # Build a submodel element if a raw value was passed in the argument
            if vorname and not isinstance(vorname, SubmodelElement):
                vorname = self.Vorname(vorname)

            # Build a submodel element if a raw value was passed in the argument
            if nachname and not isinstance(nachname, SubmodelElement):
                nachname = self.Nachname(nachname)

            # Build a submodel element if a raw value was passed in the argument
            if adresse and not isinstance(adresse, SubmodelElement):
                adresse = self.Adresse(adresse)

            # Build a submodel element if a raw value was passed in the argument
            if umsatzbesteuerung and not isinstance(umsatzbesteuerung, SubmodelElement):
                umsatzbesteuerung = self.Umsatzbesteuerung(umsatzbesteuerung)

            # Build a submodel element if a raw value was passed in the argument
            if iBAN and not isinstance(iBAN, SubmodelElement):
                iBAN = self.IBAN(iBAN)

            # Build a submodel element if a raw value was passed in the argument
            if steuernummer and not isinstance(steuernummer, SubmodelElement):
                steuernummer = self.Steuernummer(steuernummer)

            # Build a submodel element if a raw value was passed in the argument
            if telefonnummern and not isinstance(telefonnummern, SubmodelElement):
                telefonnummern = self.Telefonnummern(telefonnummern)

            # Build a submodel element if a raw value was passed in the argument
            if emailadressen and not isinstance(emailadressen, SubmodelElement):
                emailadressen = self.Emailadressen(emailadressen)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [
                firmenname,
                anrede,
                vorname,
                nachname,
                adresse,
                umsatzbesteuerung,
                iBAN,
                steuernummer,
                telefonnummern,
                emailadressen,
            ]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Notiz(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Notiz",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Notiz",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Holzlisten(SubmodelElementList):
        class HolzlisteReference(ReferenceElement):
            def __init__(
                self,
                value: Reference,
                id_short: str = "HolzlisteReference",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Holzlisten/HolzlisteReference",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            holzlisteReference: Optional[
                Iterable[Union[Reference, HolzlisteReference]]
            ] = None,
            id_short: str = "Holzlisten",
            type_value_list_element: SubmodelElement = ReferenceElement,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Holzlisten",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a list of submodel elements if a raw values were passed in the argument
            if holzlisteReference and all(
                [isinstance(i, Reference) for i in holzlisteReference]
            ):
                holzlisteReference = [
                    self.HolzlisteReference(i) for i in holzlisteReference
                ]

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [holzlisteReference]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Preismatrix(SubmodelElementList):
        class Preismatrixeintrag(SubmodelElementCollection):
            class Preis(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "Preis",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Preis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class MittendurchmesserVon(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "MittendurchmesserVon",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserVon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class MittendurchmesserBis(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "MittendurchmesserBis",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserBis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class GueteklasseVon(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "GueteklasseVon",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/GueteklasseVon",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class GueteklasseBis(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "GueteklasseBis",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/GueteklasseBis",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Sortimentstyp(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Sortimentstyp",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Sortimentstyp",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Sorte(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Sorte",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Sorte",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="One",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                preis: Union[float, Preis],
                sorte: Union[str, Sorte],
                mittendurchmesserVon: Optional[
                    Union[float, MittendurchmesserVon]
                ] = None,
                mittendurchmesserBis: Optional[
                    Union[float, MittendurchmesserBis]
                ] = None,
                gueteklasseVon: Optional[Union[str, GueteklasseVon]] = None,
                gueteklasseBis: Optional[Union[str, GueteklasseBis]] = None,
                sortimentstyp: Optional[Union[str, Sortimentstyp]] = None,
                id_short: str = "Preismatrixeintrag",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if preis and not isinstance(preis, SubmodelElement):
                    preis = self.Preis(preis)

                # Build a submodel element if a raw value was passed in the argument
                if mittendurchmesserVon and not isinstance(
                    mittendurchmesserVon, SubmodelElement
                ):
                    mittendurchmesserVon = self.MittendurchmesserVon(
                        mittendurchmesserVon
                    )

                # Build a submodel element if a raw value was passed in the argument
                if mittendurchmesserBis and not isinstance(
                    mittendurchmesserBis, SubmodelElement
                ):
                    mittendurchmesserBis = self.MittendurchmesserBis(
                        mittendurchmesserBis
                    )

                # Build a submodel element if a raw value was passed in the argument
                if gueteklasseVon and not isinstance(gueteklasseVon, SubmodelElement):
                    gueteklasseVon = self.GueteklasseVon(gueteklasseVon)

                # Build a submodel element if a raw value was passed in the argument
                if gueteklasseBis and not isinstance(gueteklasseBis, SubmodelElement):
                    gueteklasseBis = self.GueteklasseBis(gueteklasseBis)

                # Build a submodel element if a raw value was passed in the argument
                if sortimentstyp and not isinstance(sortimentstyp, SubmodelElement):
                    sortimentstyp = self.Sortimentstyp(sortimentstyp)

                # Build a submodel element if a raw value was passed in the argument
                if sorte and not isinstance(sorte, SubmodelElement):
                    sorte = self.Sorte(sorte)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [
                    preis,
                    mittendurchmesserVon,
                    mittendurchmesserBis,
                    gueteklasseVon,
                    gueteklasseBis,
                    sortimentstyp,
                    sorte,
                ]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            preismatrixeintrag: Optional[Iterable[Preismatrixeintrag]] = None,
            id_short: str = "Preismatrix",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [preismatrixeintrag]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        verkaufsstatus: Union[int, Verkaufsstatus],
        abrechnungsmass: Union[str, Abrechnungsmass],
        bereitstellungsart: Union[str, Bereitstellungsart],
        verfuegbarAb: Union[Date, VerfuegbarAb],
        verfuegbarBis: Optional[Union[Date, VerfuegbarBis]] = None,
        verkaeufer: Optional[Verkaeufer] = None,
        notiz: Optional[Union[str, Notiz]] = None,
        holzlisten: Optional[Holzlisten] = None,
        preismatrix: Optional[Preismatrix] = None,
        id_short: str = "Verkaufslos",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if verkaufsstatus and not isinstance(verkaufsstatus, SubmodelElement):
            verkaufsstatus = self.Verkaufsstatus(verkaufsstatus)

        # Build a submodel element if a raw value was passed in the argument
        if abrechnungsmass and not isinstance(abrechnungsmass, SubmodelElement):
            abrechnungsmass = self.Abrechnungsmass(abrechnungsmass)

        # Build a submodel element if a raw value was passed in the argument
        if bereitstellungsart and not isinstance(bereitstellungsart, SubmodelElement):
            bereitstellungsart = self.Bereitstellungsart(bereitstellungsart)

        # Build a submodel element if a raw value was passed in the argument
        if verfuegbarAb and not isinstance(verfuegbarAb, SubmodelElement):
            verfuegbarAb = self.VerfuegbarAb(verfuegbarAb)

        # Build a submodel element if a raw value was passed in the argument
        if verfuegbarBis and not isinstance(verfuegbarBis, SubmodelElement):
            verfuegbarBis = self.VerfuegbarBis(verfuegbarBis)

        # Build a submodel element if a raw value was passed in the argument
        if notiz and not isinstance(notiz, SubmodelElement):
            notiz = self.Notiz(notiz)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [
            verkaufsstatus,
            abrechnungsmass,
            bereitstellungsart,
            verfuegbarAb,
            verfuegbarBis,
            verkaeufer,
            notiz,
            holzlisten,
            preismatrix,
        ]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class Waldweg(Submodel):
    class Beschreibung(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Beschreibung",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Beschreibung",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Erstellungsdatum(Property):
        def __init__(
            self,
            value: Date,
            id_short: str = "Erstellungsdatum",
            value_type: DataTypeDefXsd = Date,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Erstellungsdatum",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Aktualisierungsdatum(Property):
        def __init__(
            self,
            value: Date,
            id_short: str = "Aktualisierungsdatum",
            value_type: DataTypeDefXsd = Date,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Aktualisierungsdatum",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Breite(Property):
        def __init__(
            self,
            value: float,
            id_short: str = "Breite",
            value_type: DataTypeDefXsd = float,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Breite",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Wegpunkte(SubmodelElementList):
        class Standort(SubmodelElementCollection):
            class Koordinate(SubmodelElementCollection):
                class X(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "x",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort/Koordinate/x",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Y(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "y",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort/Koordinate/y",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Alt(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "alt",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort/Koordinate/alt",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Crs(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "crs",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort/Koordinate/crs",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    x: Optional[Union[float, X]] = None,
                    y: Optional[Union[float, Y]] = None,
                    alt: Optional[Union[float, Alt]] = None,
                    crs: Optional[Union[str, Crs]] = None,
                    id_short: str = "Koordinate",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort/Koordinate",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="OneToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if x and not isinstance(x, SubmodelElement):
                        x = self.X(x)

                    # Build a submodel element if a raw value was passed in the argument
                    if y and not isinstance(y, SubmodelElement):
                        y = self.Y(y)

                    # Build a submodel element if a raw value was passed in the argument
                    if alt and not isinstance(alt, SubmodelElement):
                        alt = self.Alt(alt)

                    # Build a submodel element if a raw value was passed in the argument
                    if crs and not isinstance(crs, SubmodelElement):
                        crs = self.Crs(crs)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [x, y, alt, crs]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Notiz(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Notiz",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort/Notiz",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                koordinate: Iterable[Koordinate],
                notiz: Optional[Union[str, Notiz]] = None,
                id_short: str = "Standort",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Standort",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if notiz and not isinstance(notiz, SubmodelElement):
                    notiz = self.Notiz(notiz)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [koordinate, notiz]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            standort: Optional[Standort] = None,
            id_short: str = "Wegpunkte",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [standort]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Wendepunkte(SubmodelElementList):
        class Standort(SubmodelElementCollection):
            class Koordinate(SubmodelElementCollection):
                class X(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "x",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort/Koordinate/x",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Y(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "y",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort/Koordinate/y",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Alt(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "alt",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort/Koordinate/alt",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Crs(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "crs",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort/Koordinate/crs",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    x: Optional[Union[float, X]] = None,
                    y: Optional[Union[float, Y]] = None,
                    alt: Optional[Union[float, Alt]] = None,
                    crs: Optional[Union[str, Crs]] = None,
                    id_short: str = "Koordinate",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort/Koordinate",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="OneToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if x and not isinstance(x, SubmodelElement):
                        x = self.X(x)

                    # Build a submodel element if a raw value was passed in the argument
                    if y and not isinstance(y, SubmodelElement):
                        y = self.Y(y)

                    # Build a submodel element if a raw value was passed in the argument
                    if alt and not isinstance(alt, SubmodelElement):
                        alt = self.Alt(alt)

                    # Build a submodel element if a raw value was passed in the argument
                    if crs and not isinstance(crs, SubmodelElement):
                        crs = self.Crs(crs)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [x, y, alt, crs]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Notiz(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Notiz",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort/Notiz",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                koordinate: Iterable[Koordinate],
                notiz: Optional[Union[str, Notiz]] = None,
                id_short: str = "Standort",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Standort",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if notiz and not isinstance(notiz, SubmodelElement):
                    notiz = self.Notiz(notiz)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [koordinate, notiz]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            standort: Optional[Standort] = None,
            id_short: str = "Wendepunkte",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [standort]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        beschreibung: Union[str, Beschreibung],
        erstellungsdatum: Optional[Union[Date, Erstellungsdatum]] = None,
        aktualisierungsdatum: Optional[Union[Date, Aktualisierungsdatum]] = None,
        breite: Optional[Union[float, Breite]] = None,
        wegpunkte: Optional[Wegpunkte] = None,
        wendepunkte: Optional[Wendepunkte] = None,
        id_short: str = "Waldweg",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/Waldweg/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if beschreibung and not isinstance(beschreibung, SubmodelElement):
            beschreibung = self.Beschreibung(beschreibung)

        # Build a submodel element if a raw value was passed in the argument
        if erstellungsdatum and not isinstance(erstellungsdatum, SubmodelElement):
            erstellungsdatum = self.Erstellungsdatum(erstellungsdatum)

        # Build a submodel element if a raw value was passed in the argument
        if aktualisierungsdatum and not isinstance(
            aktualisierungsdatum, SubmodelElement
        ):
            aktualisierungsdatum = self.Aktualisierungsdatum(aktualisierungsdatum)

        # Build a submodel element if a raw value was passed in the argument
        if breite and not isinstance(breite, SubmodelElement):
            breite = self.Breite(breite)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [
            beschreibung,
            erstellungsdatum,
            aktualisierungsdatum,
            breite,
            wegpunkte,
            wendepunkte,
        ]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )


class ZuFaellendeBaeume(Submodel):
    class Beschreibung(Property):
        def __init__(
            self,
            value: str,
            id_short: str = "Beschreibung",
            value_type: DataTypeDefXsd = str,
            value_id: Optional[Reference] = None,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Beschreibung",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="One",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            super().__init__(
                value=value,
                id_short=id_short,
                value_type=value_type,
                value_id=value_id,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Umring(SubmodelElementCollection):
        class Name(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Name",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Name",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Beschreibung(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Beschreibung",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Beschreibung",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Koordinaten(SubmodelElementList):
            class Koordinate(SubmodelElementCollection):
                class X(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "x",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/x",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Y(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "y",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/y",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Alt(Property):
                    def __init__(
                        self,
                        value: float,
                        id_short: str = "alt",
                        value_type: DataTypeDefXsd = float,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/alt",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Crs(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "crs",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/crs",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    x: Optional[Union[float, X]] = None,
                    y: Optional[Union[float, Y]] = None,
                    alt: Optional[Union[float, Alt]] = None,
                    crs: Optional[Union[str, Crs]] = None,
                    id_short: str = "Koordinate",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="OneToMany",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if x and not isinstance(x, SubmodelElement):
                        x = self.X(x)

                    # Build a submodel element if a raw value was passed in the argument
                    if y and not isinstance(y, SubmodelElement):
                        y = self.Y(y)

                    # Build a submodel element if a raw value was passed in the argument
                    if alt and not isinstance(alt, SubmodelElement):
                        alt = self.Alt(alt)

                    # Build a submodel element if a raw value was passed in the argument
                    if crs and not isinstance(crs, SubmodelElement):
                        crs = self.Crs(crs)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [x, y, alt, crs]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                koordinate: Iterable[Koordinate],
                id_short: str = "Koordinaten",
                type_value_list_element: SubmodelElement = SubmodelElementCollection,
                semantic_id_list_element: Optional[Reference] = None,
                value_type_list_element: Optional[DataTypeDefXsd] = None,
                order_relevant: bool = True,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="One",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [koordinate]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    type_value_list_element=type_value_list_element,
                    semantic_id_list_element=semantic_id_list_element,
                    value_type_list_element=value_type_list_element,
                    order_relevant=order_relevant,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            name: Union[str, Name],
            beschreibung: Union[str, Beschreibung],
            koordinaten: Koordinaten,
            id_short: str = "Umring",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if name and not isinstance(name, SubmodelElement):
                name = self.Name(name)

            # Build a submodel element if a raw value was passed in the argument
            if beschreibung and not isinstance(beschreibung, SubmodelElement):
                beschreibung = self.Beschreibung(beschreibung)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [name, beschreibung, koordinaten]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Position(SubmodelElementCollection):
        class Koordinate(SubmodelElementCollection):
            class X(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "x",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/x",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Y(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "y",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/y",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Alt(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "alt",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/alt",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Crs(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "crs",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/crs",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                x: Optional[Union[float, X]] = None,
                y: Optional[Union[float, Y]] = None,
                alt: Optional[Union[float, Alt]] = None,
                crs: Optional[Union[str, Crs]] = None,
                id_short: str = "Koordinate",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="OneToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if x and not isinstance(x, SubmodelElement):
                    x = self.X(x)

                # Build a submodel element if a raw value was passed in the argument
                if y and not isinstance(y, SubmodelElement):
                    y = self.Y(y)

                # Build a submodel element if a raw value was passed in the argument
                if alt and not isinstance(alt, SubmodelElement):
                    alt = self.Alt(alt)

                # Build a submodel element if a raw value was passed in the argument
                if crs and not isinstance(crs, SubmodelElement):
                    crs = self.Crs(crs)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [x, y, alt, crs]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        class Notiz(Property):
            def __init__(
                self,
                value: str,
                id_short: str = "Notiz",
                value_type: DataTypeDefXsd = str,
                value_id: Optional[Reference] = None,
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Notiz",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToOne",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                super().__init__(
                    value=value,
                    id_short=id_short,
                    value_type=value_type,
                    value_id=value_id,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            koordinate: Iterable[Koordinate],
            notiz: Optional[Union[str, Notiz]] = None,
            id_short: str = "Position",
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Build a submodel element if a raw value was passed in the argument
            if notiz and not isinstance(notiz, SubmodelElement):
                notiz = self.Notiz(notiz)

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [koordinate, notiz]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    class Baumliste(SubmodelElementList):
        class StehenderBaum(SubmodelElementCollection):
            class Hoehe(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "Hoehe",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Hoehe",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Bhd(Property):
                def __init__(
                    self,
                    value: float,
                    id_short: str = "bhd",
                    value_type: DataTypeDefXsd = float,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/bhd",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Baumart(Property):
                def __init__(
                    self,
                    value: str,
                    id_short: str = "Baumart",
                    value_type: DataTypeDefXsd = str,
                    value_id: Optional[Reference] = None,
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Baumart",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    super().__init__(
                        value=value,
                        id_short=id_short,
                        value_type=value_type,
                        value_id=value_id,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            class Position(SubmodelElementCollection):
                class Koordinate(SubmodelElementCollection):
                    class X(Property):
                        def __init__(
                            self,
                            value: float,
                            id_short: str = "x",
                            value_type: DataTypeDefXsd = float,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position/Koordinate/x",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    class Y(Property):
                        def __init__(
                            self,
                            value: float,
                            id_short: str = "y",
                            value_type: DataTypeDefXsd = float,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position/Koordinate/y",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    class Alt(Property):
                        def __init__(
                            self,
                            value: float,
                            id_short: str = "alt",
                            value_type: DataTypeDefXsd = float,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position/Koordinate/alt",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    class Crs(Property):
                        def __init__(
                            self,
                            value: str,
                            id_short: str = "crs",
                            value_type: DataTypeDefXsd = str,
                            value_id: Optional[Reference] = None,
                            display_name: Optional[LangStringSet] = None,
                            category: Optional[str] = None,
                            description: Optional[LangStringSet] = None,
                            semantic_id: Optional[Reference] = GlobalReference(
                                key=(
                                    Key(
                                        type_=KeyTypes.GLOBAL_REFERENCE,
                                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position/Koordinate/crs",
                                    ),
                                ),
                                referred_semantic_id=None,
                            ),
                            qualifier: Iterable[Qualifier] = None,
                            kind: ModelingKind = ModelingKind.INSTANCE,
                            extension: Iterable[Extension] = (),
                            supplemental_semantic_id: Iterable[Reference] = (),
                            embedded_data_specifications: Iterable[
                                EmbeddedDataSpecification
                            ] = None,
                        ):
                            if qualifier is None:
                                qualifier = (
                                    Qualifier(
                                        type_="Cardinality",
                                        value_type=str,
                                        value="ZeroToOne",
                                        value_id=None,
                                        kind=QualifierKind.CONCEPT_QUALIFIER,
                                        semantic_id=None,
                                        supplemental_semantic_id=(),
                                    ),
                                )

                            if embedded_data_specifications is None:
                                embedded_data_specifications = []

                            super().__init__(
                                value=value,
                                id_short=id_short,
                                value_type=value_type,
                                value_id=value_id,
                                display_name=display_name,
                                category=category,
                                description=description,
                                semantic_id=semantic_id,
                                qualifier=qualifier,
                                kind=kind,
                                extension=extension,
                                supplemental_semantic_id=supplemental_semantic_id,
                                embedded_data_specifications=embedded_data_specifications,
                            )

                    def __init__(
                        self,
                        x: Optional[Union[float, X]] = None,
                        y: Optional[Union[float, Y]] = None,
                        alt: Optional[Union[float, Alt]] = None,
                        crs: Optional[Union[str, Crs]] = None,
                        id_short: str = "Koordinate",
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position/Koordinate",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="OneToMany",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        # Build a submodel element if a raw value was passed in the argument
                        if x and not isinstance(x, SubmodelElement):
                            x = self.X(x)

                        # Build a submodel element if a raw value was passed in the argument
                        if y and not isinstance(y, SubmodelElement):
                            y = self.Y(y)

                        # Build a submodel element if a raw value was passed in the argument
                        if alt and not isinstance(alt, SubmodelElement):
                            alt = self.Alt(alt)

                        # Build a submodel element if a raw value was passed in the argument
                        if crs and not isinstance(crs, SubmodelElement):
                            crs = self.Crs(crs)

                        # Add all passed/initialized submodel elements to a single list
                        embedded_submodel_elements = []
                        for se_arg in [x, y, alt, crs]:
                            if se_arg is None:
                                continue
                            elif isinstance(se_arg, SubmodelElement):
                                embedded_submodel_elements.append(se_arg)
                            elif isinstance(se_arg, Iterable):
                                for n, element in enumerate(se_arg):
                                    element.id_short = f"{element.id_short}{n}"
                                    embedded_submodel_elements.append(element)
                            else:
                                raise TypeError(
                                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                                )

                        super().__init__(
                            value=embedded_submodel_elements,
                            id_short=id_short,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                class Notiz(Property):
                    def __init__(
                        self,
                        value: str,
                        id_short: str = "Notiz",
                        value_type: DataTypeDefXsd = str,
                        value_id: Optional[Reference] = None,
                        display_name: Optional[LangStringSet] = None,
                        category: Optional[str] = None,
                        description: Optional[LangStringSet] = None,
                        semantic_id: Optional[Reference] = GlobalReference(
                            key=(
                                Key(
                                    type_=KeyTypes.GLOBAL_REFERENCE,
                                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position/Notiz",
                                ),
                            ),
                            referred_semantic_id=None,
                        ),
                        qualifier: Iterable[Qualifier] = None,
                        kind: ModelingKind = ModelingKind.INSTANCE,
                        extension: Iterable[Extension] = (),
                        supplemental_semantic_id: Iterable[Reference] = (),
                        embedded_data_specifications: Iterable[
                            EmbeddedDataSpecification
                        ] = None,
                    ):
                        if qualifier is None:
                            qualifier = (
                                Qualifier(
                                    type_="Cardinality",
                                    value_type=str,
                                    value="ZeroToOne",
                                    value_id=None,
                                    kind=QualifierKind.CONCEPT_QUALIFIER,
                                    semantic_id=None,
                                    supplemental_semantic_id=(),
                                ),
                            )

                        if embedded_data_specifications is None:
                            embedded_data_specifications = []

                        super().__init__(
                            value=value,
                            id_short=id_short,
                            value_type=value_type,
                            value_id=value_id,
                            display_name=display_name,
                            category=category,
                            description=description,
                            semantic_id=semantic_id,
                            qualifier=qualifier,
                            kind=kind,
                            extension=extension,
                            supplemental_semantic_id=supplemental_semantic_id,
                            embedded_data_specifications=embedded_data_specifications,
                        )

                def __init__(
                    self,
                    koordinate: Iterable[Koordinate],
                    notiz: Optional[Union[str, Notiz]] = None,
                    id_short: str = "Position",
                    display_name: Optional[LangStringSet] = None,
                    category: Optional[str] = None,
                    description: Optional[LangStringSet] = None,
                    semantic_id: Optional[Reference] = GlobalReference(
                        key=(
                            Key(
                                type_=KeyTypes.GLOBAL_REFERENCE,
                                value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum/Position",
                            ),
                        ),
                        referred_semantic_id=None,
                    ),
                    qualifier: Iterable[Qualifier] = None,
                    kind: ModelingKind = ModelingKind.INSTANCE,
                    extension: Iterable[Extension] = (),
                    supplemental_semantic_id: Iterable[Reference] = (),
                    embedded_data_specifications: Iterable[
                        EmbeddedDataSpecification
                    ] = None,
                ):
                    if qualifier is None:
                        qualifier = (
                            Qualifier(
                                type_="Cardinality",
                                value_type=str,
                                value="ZeroToOne",
                                value_id=None,
                                kind=QualifierKind.CONCEPT_QUALIFIER,
                                semantic_id=None,
                                supplemental_semantic_id=(),
                            ),
                        )

                    if embedded_data_specifications is None:
                        embedded_data_specifications = []

                    # Build a submodel element if a raw value was passed in the argument
                    if notiz and not isinstance(notiz, SubmodelElement):
                        notiz = self.Notiz(notiz)

                    # Add all passed/initialized submodel elements to a single list
                    embedded_submodel_elements = []
                    for se_arg in [koordinate, notiz]:
                        if se_arg is None:
                            continue
                        elif isinstance(se_arg, SubmodelElement):
                            embedded_submodel_elements.append(se_arg)
                        elif isinstance(se_arg, Iterable):
                            for n, element in enumerate(se_arg):
                                element.id_short = f"{element.id_short}{n}"
                                embedded_submodel_elements.append(element)
                        else:
                            raise TypeError(
                                f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                            )

                    super().__init__(
                        value=embedded_submodel_elements,
                        id_short=id_short,
                        display_name=display_name,
                        category=category,
                        description=description,
                        semantic_id=semantic_id,
                        qualifier=qualifier,
                        kind=kind,
                        extension=extension,
                        supplemental_semantic_id=supplemental_semantic_id,
                        embedded_data_specifications=embedded_data_specifications,
                    )

            def __init__(
                self,
                hoehe: Optional[Union[float, Hoehe]] = None,
                bhd: Optional[Union[float, Bhd]] = None,
                baumart: Optional[Union[str, Baumart]] = None,
                position: Optional[Position] = None,
                id_short: str = "StehenderBaum",
                display_name: Optional[LangStringSet] = None,
                category: Optional[str] = None,
                description: Optional[LangStringSet] = None,
                semantic_id: Optional[Reference] = GlobalReference(
                    key=(
                        Key(
                            type_=KeyTypes.GLOBAL_REFERENCE,
                            value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/StehenderBaum",
                        ),
                    ),
                    referred_semantic_id=None,
                ),
                qualifier: Iterable[Qualifier] = None,
                kind: ModelingKind = ModelingKind.INSTANCE,
                extension: Iterable[Extension] = (),
                supplemental_semantic_id: Iterable[Reference] = (),
                embedded_data_specifications: Iterable[
                    EmbeddedDataSpecification
                ] = None,
            ):
                if qualifier is None:
                    qualifier = (
                        Qualifier(
                            type_="Cardinality",
                            value_type=str,
                            value="ZeroToMany",
                            value_id=None,
                            kind=QualifierKind.CONCEPT_QUALIFIER,
                            semantic_id=None,
                            supplemental_semantic_id=(),
                        ),
                    )

                if embedded_data_specifications is None:
                    embedded_data_specifications = []

                # Build a submodel element if a raw value was passed in the argument
                if hoehe and not isinstance(hoehe, SubmodelElement):
                    hoehe = self.Hoehe(hoehe)

                # Build a submodel element if a raw value was passed in the argument
                if bhd and not isinstance(bhd, SubmodelElement):
                    bhd = self.Bhd(bhd)

                # Build a submodel element if a raw value was passed in the argument
                if baumart and not isinstance(baumart, SubmodelElement):
                    baumart = self.Baumart(baumart)

                # Add all passed/initialized submodel elements to a single list
                embedded_submodel_elements = []
                for se_arg in [hoehe, bhd, baumart, position]:
                    if se_arg is None:
                        continue
                    elif isinstance(se_arg, SubmodelElement):
                        embedded_submodel_elements.append(se_arg)
                    elif isinstance(se_arg, Iterable):
                        for n, element in enumerate(se_arg):
                            element.id_short = f"{element.id_short}{n}"
                            embedded_submodel_elements.append(element)
                    else:
                        raise TypeError(
                            f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                        )

                super().__init__(
                    value=embedded_submodel_elements,
                    id_short=id_short,
                    display_name=display_name,
                    category=category,
                    description=description,
                    semantic_id=semantic_id,
                    qualifier=qualifier,
                    kind=kind,
                    extension=extension,
                    supplemental_semantic_id=supplemental_semantic_id,
                    embedded_data_specifications=embedded_data_specifications,
                )

        def __init__(
            self,
            stehenderBaum: Optional[Iterable[StehenderBaum]] = None,
            id_short: str = "Baumliste",
            type_value_list_element: SubmodelElement = SubmodelElementCollection,
            semantic_id_list_element: Optional[Reference] = None,
            value_type_list_element: Optional[DataTypeDefXsd] = None,
            order_relevant: bool = True,
            display_name: Optional[LangStringSet] = None,
            category: Optional[str] = None,
            description: Optional[LangStringSet] = None,
            semantic_id: Optional[Reference] = GlobalReference(
                key=(
                    Key(
                        type_=KeyTypes.GLOBAL_REFERENCE,
                        value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste",
                    ),
                ),
                referred_semantic_id=None,
            ),
            qualifier: Iterable[Qualifier] = None,
            kind: ModelingKind = ModelingKind.INSTANCE,
            extension: Iterable[Extension] = (),
            supplemental_semantic_id: Iterable[Reference] = (),
            embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
        ):
            if qualifier is None:
                qualifier = (
                    Qualifier(
                        type_="Cardinality",
                        value_type=str,
                        value="ZeroToOne",
                        value_id=None,
                        kind=QualifierKind.CONCEPT_QUALIFIER,
                        semantic_id=None,
                        supplemental_semantic_id=(),
                    ),
                )

            if embedded_data_specifications is None:
                embedded_data_specifications = []

            # Add all passed/initialized submodel elements to a single list
            embedded_submodel_elements = []
            for se_arg in [stehenderBaum]:
                if se_arg is None:
                    continue
                elif isinstance(se_arg, SubmodelElement):
                    embedded_submodel_elements.append(se_arg)
                elif isinstance(se_arg, Iterable):
                    for n, element in enumerate(se_arg):
                        element.id_short = f"{element.id_short}{n}"
                        embedded_submodel_elements.append(element)
                else:
                    raise TypeError(
                        f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                    )

            super().__init__(
                value=embedded_submodel_elements,
                id_short=id_short,
                type_value_list_element=type_value_list_element,
                semantic_id_list_element=semantic_id_list_element,
                value_type_list_element=value_type_list_element,
                order_relevant=order_relevant,
                display_name=display_name,
                category=category,
                description=description,
                semantic_id=semantic_id,
                qualifier=qualifier,
                kind=kind,
                extension=extension,
                supplemental_semantic_id=supplemental_semantic_id,
                embedded_data_specifications=embedded_data_specifications,
            )

    def __init__(
        self,
        id_: str,
        beschreibung: Union[str, Beschreibung],
        umring: Optional[Umring] = None,
        position: Optional[Position] = None,
        baumliste: Optional[Baumliste] = None,
        id_short: str = "ZuFaellendeBaeume",
        display_name: Optional[LangStringSet] = None,
        category: Optional[str] = None,
        description: Optional[LangStringSet] = None,
        administration: Optional[AdministrativeInformation] = None,
        semantic_id: Optional[Reference] = GlobalReference(
            key=(
                Key(
                    type_=KeyTypes.GLOBAL_REFERENCE,
                    value="https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1",
                ),
            ),
            referred_semantic_id=None,
        ),
        qualifier: Iterable[Qualifier] = None,
        kind: ModelingKind = ModelingKind.INSTANCE,
        extension: Iterable[Extension] = (),
        supplemental_semantic_id: Iterable[Reference] = (),
        embedded_data_specifications: Iterable[EmbeddedDataSpecification] = None,
    ):
        if qualifier is None:
            qualifier = ()

        if embedded_data_specifications is None:
            embedded_data_specifications = []

        # Build a submodel element if a raw value was passed in the argument
        if beschreibung and not isinstance(beschreibung, SubmodelElement):
            beschreibung = self.Beschreibung(beschreibung)

        # Add all passed/initialized submodel elements to a single list
        embedded_submodel_elements = []
        for se_arg in [beschreibung, umring, position, baumliste]:
            if se_arg is None:
                continue
            elif isinstance(se_arg, SubmodelElement):
                embedded_submodel_elements.append(se_arg)
            elif isinstance(se_arg, Iterable):
                for n, element in enumerate(se_arg):
                    element.id_short = f"{element.id_short}{n}"
                    embedded_submodel_elements.append(element)
            else:
                raise TypeError(
                    f"Unknown type of value in submodel_element_args: {type(se_arg)}"
                )

        super().__init__(
            submodel_element=embedded_submodel_elements,
            id_=id_,
            id_short=id_short,
            display_name=display_name,
            category=category,
            description=description,
            administration=administration,
            semantic_id=semantic_id,
            qualifier=qualifier,
            kind=kind,
            extension=extension,
            supplemental_semantic_id=supplemental_semantic_id,
            embedded_data_specifications=embedded_data_specifications,
        )
