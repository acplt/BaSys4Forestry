FROM python:3.7

ENV CONTAINER_HOME=/var/www

ADD . $CONTAINER_HOME
WORKDIR $CONTAINER_HOME

RUN pip install -r $CONTAINER_HOME/requirements.txt 

CMD python $CONTAINER_HOME/demos/demo2/demo2_dzwald.py

