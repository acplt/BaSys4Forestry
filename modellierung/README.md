# Submodelle 

## Waldweg (Submodel Template)

Die Beschreibung eines Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg](https://admin-shell.io/kwh40/forestml40/Waldweg)

### Beschreibung (SubmodelElement)

Die textuelle Beschreibung eines Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Beschreibung](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Beschreibung)

### Erstellungsdatum (SubmodelElement)

Erstellungsdatum des Waldwegs

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Erstellungsdatum](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Erstellungsdatum)

### Aktualisierungsdatum (SubmodelElement)

Datum der letzten Aktualisierung des Waldwegs

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Aktualisierungsdatum](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Aktualisierungsdatum)

### Breite (SubmodelElement)

Breite des Waldwegs in m

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Breite](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Breite)

### Wegpunkte (SubmodelElementList)

Liste der Wegpunkte eines Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte)

### Wegpunkt (SubmodelElementCollection)

Einzelner Wegpunkt eines Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt)

### Koordinate (SubmodelElementCollection)

Lage des Wegpunkt des Waldwegs in Geo-Koordinaten.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage des Wegpunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/x](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage des Wegpunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/y](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Lage des Wegpunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Beschreibung der Lage des Wegpunkt des Waldwegs.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Koordinate/crs)

### Notiz (SubmodelElement)

Notiz zur Beschreibung der Lage des Wegpunkt des Waldwegs, Freitext.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Notiz](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wegpunkte/Wegpunkt/Notiz)

### Wendepunkte (SubmodelElementList)

Liste der Wendepunkte des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte)

### Wendepunkt (SubmodelElementCollection)

Einzelner Wendepunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt)

### Koordinate (SubmodelElementCollection)

Lage des Wendepunkt des Waldwegs in Geo-Koordinaten.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage des Wendepunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/x](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage des Wendepunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/y](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Lage des Wendepunkt des Waldwegs.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Beschreibung der Lage des Wendepunkt des Waldwegs.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Koordinate/crs)

### Notiz (SubmodelElement)

Notiz zur Beschreibung der Lage des Wendepunkt des Waldwegs, Freitext.

[https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Notiz](https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Wendepunkte/Wendepunkt/Notiz)

## Bestandesdaten (Submodel Template)

Bestandesdaten (Naturaldaten) eines Waldbestands.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten](https://admin-shell.io/kwh40/forestml40/Bestandesdaten)

### Umring (SubmodelElementCollection)

Geometrischer Umring des Waldbestands.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring)

### Name (SubmodelElement)

Bezeichnung des Waldbestands.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Name](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Name)

### Beschreibung (SubmodelElement)

Textuelle Beschreibung des Waldbestands.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Beschreibung](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Beschreibung)

### Koordinaten (SubmodelElementList)

Koordinatenliste des Umrings des Bestandes.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten)

### Koordinate (SubmodelElementCollection)

Einzelne Geo-Koordinate des Umrings des Bestandes.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Geo-Koordinate des Umrings des Bestandes.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/x](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Geo-Koordinate des Umrings des Bestandes.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/y](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. der Geo-Koordinate des Umrings des Bestandes.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Beschreibung der Geo-Koordinate des Umrings des Bestandes.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Umring/Koordinaten/Koordinate/crs)

### Waldbesitzer (SubmodelElementCollection)

Besitzer des Waldes.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer)

### Firmenname (SubmodelElement)

Firmenname des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Firmenname](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Firmenname)

### Anrede (SubmodelElement)

Anrede des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Anrede](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Anrede)

### Vorname (SubmodelElement)

Vorname des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Vorname](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Vorname)

### Nachname (SubmodelElement)

Nachname des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Nachname](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Nachname)

### Adresse (SubmodelElement)

Adresse des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Adresse](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Emailadressen](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Waldbesitzers, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/IBAN](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Waldbesitzers.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Steuernummer](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Waldbesitzer/Steuernummer)

### Baumarten (SubmodelElementList)

Liste der Baumarten im Waldbestand.

Erlaubte Listeneinträge: https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten/Baumart.

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten)

### Baumart (SubmodelElement)

Einzelne Baumart im Waldbestand.

Wertebereich siehe [hier](#holzart--enum-).

[https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten/Baumart](https://admin-shell.io/kwh40/forestml40/Bestandesdaten/0/1/Baumarten/Baumart)

## Beobachtung (Submodel Template)

Beschreibung einer Beobachtung im Waldbestand.

[https://admin-shell.io/kwh40/forestml40/Beobachtung](https://admin-shell.io/kwh40/forestml40/Beobachtung)

### Name (SubmodelElement)

Name der Beobachtung im Waldbestand.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Name](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Name)

### Beschreibung (SubmodelElement)

Beschreibung der Beobachtung im Waldbestand. 

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Beschreibung](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Beschreibung)

### Umring (SubmodelElementCollection)

Geometrischer Umring der Beobachtung im Waldbestand.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring)

### Name (SubmodelElement)

Name des Umrings der Beobachtung im Waldbestand.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Name](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Name)

### Beschreibung (SubmodelElement)

Beschreibung des Umrings der Beobachtung im Waldbestand.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Beschreibung](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Beschreibung)

### Koordinaten (SubmodelElementList)

Koordinatenliste des Umrings der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten)

### Koordinate (SubmodelElementCollection)

Einzelne Geo-Koordinate des Umrings der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Geo-Koordinate des Umrings der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/x](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Geo-Koordinate des Umrings der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/y](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Geo-Koordinate des Umrings der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Geo-Koordinate des Umrings der Beobachtung.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Umring/Koordinaten/Koordinate/crs)

### Position (SubmodelElementCollection)

Position der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position)

### Koordinate (SubmodelElementCollection)

Lage der Beobachtung in Geo-Koordinaten.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/x](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/y](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Lage der Beobachtung.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Beschreibung der Lage der Beobachtung.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Koordinate/crs)

### Notiz (SubmodelElement)

Notiz zur Beschreibung der Lage der Beobachtung, Freitext.

[https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Notiz](https://admin-shell.io/kwh40/forestml40/Beobachtung/0/1/Position/Notiz)

## ZuFaellendeBaeume (Submodel Template)

Zusammenstellung der Bäume die im Waldbestand zum Fällen ausgewählt wurden.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume)

### Beschreibung (SubmodelElement)

Beschreibung der Auswahl von Bäumen, die im Waldbestand zum Fällen ausgewählt wurden.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Beschreibung](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Beschreibung)

### Umring (SubmodelElementCollection)

Geometrischer Umring der Auswahl von Bäumen, die im Waldbestand zum Fällen ausgewählt wurden.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring)

### Name (SubmodelElement)

Name des geometrischen Umrings der Auswahl von Bäumen, die im Waldbestand zum Fällen ausgewählt wurden.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Name](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Name)

### Beschreibung (SubmodelElement)

Beschreibung des geometrischen Umrings der Auswahl von Bäumen, die im Waldbestand zum Fällen ausgewählt wurden.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Beschreibung](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Beschreibung)

### Koordinaten (SubmodelElementList)

Koordinatenliste des Umrings der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten)

### Koordinate (SubmodelElementCollection)

Einzelne Geo-Koordinate des Umrings der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Geo-Koordinate des Umrings der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/x](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Geo-Koordinate des Umrings der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/y](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Geo-Koordinate des Umrings der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Geo-Koordinate des Umrings der zu fällenden Bäume.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Umring/Koordinaten/Koordinate/crs)

### Position (SubmodelElementCollection)

Position der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position)

### Koordinate (SubmodelElementCollection)

Geo-Koordinaten der Position der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert der Position der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/x](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert der Position der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/y](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. der Position der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem der Position der zu fällenden Bäume.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Koordinate/crs)

### Notiz (SubmodelElement)

Notiz zur Beschreibung der Position der zu fällenden Bäume, Freitext.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Notiz](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Position/Notiz)

### Baumliste (SubmodelElementList)

Liste zu fällender Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste)

### Baum (SubmodelElementCollection)

Einzelner zu fällender Baum.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum)

### Hoehe (SubmodelElement)

Höhe in m eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Hoehe](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Hoehe)

### BHD (SubmodelElement)

Brusthöhendurchmesser in cm eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/BHD](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/BHD)

### Baumart (SubmodelElement)

Baumart eines Baums in der Liste der zu fällenden Bäume.

Wertebereich siehe [hier](#holzart--enum-).

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Baumart](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Baumart)

### Position (SubmodelElementCollection)

Position eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position)

### Koordinate (SubmodelElementCollection)

Geo-Koordinaten zur Position eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Position eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/x](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Position eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/y](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Position eines Baums in der Liste der zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Position eines Baums in der Liste der zu fällenden Bäume.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Koordinate/crs)

### Notiz (SubmodelElement)

Notiz zur Beschreibung der Position eines Baums in der Liste der zu fällenden Bäume, Freitext.

[https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Notiz](https://admin-shell.io/kwh40/forestml40/ZuFaellendeBaeume/0/1/Baumliste/Baum/Position/Notiz)

## Verkaufslos (Submodel Template)

Zusammenstellung von Holzlisten, die zum Verkauf angeboten werden.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos](https://admin-shell.io/kwh40/forestml40/Verkaufslos)

### Holzlisten (SubmodelElementList)

Holzlisten, die dem Verkaufslos zugeordnet wurden.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Holzlisten](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Holzlisten)

### Holzliste (SubmodelElement)

Einzelne Holzliste, die dem Verkaufslos zugeordnet wurde.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Holzlisten/Holzliste](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Holzlisten/Holzliste)

### Preismatrix (SubmodelElementList)

Matrix zur Herleitung von Holzpreisen auf Basis verschiedener Durchmesser- und Güteklassengruppen für das Verkaufslos. 

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix)

### Preismatrixeintrag (SubmodelElementCollection)

Einzelner Eintrag in der Matrix zur Herleitung von Holzpreisen im Verkauflos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag)

### Preis (SubmodelElement)

Resultierender Holzpreis in EUR pro fm ohne Rinde zur gewählten Kombination aus Durchmesser- und Güteklassengruppe sowie Sortimentstyp und Sorte in diesem Preismatrixeintrag im Verkauflos

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Preis](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Preis)

### MittendurchmesserVon (SubmodelElement)

Minimaler Mittendurchmesser in cm ohne Rinde zur gewählten Kombination von Durchmesser- und Güteklassengruppe in diesem Preismatrixeintrag im Verkauflos

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserVon](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserVon)

### MittendurchmesserBis (SubmodelElement)

Maximaler Mittendurchmesser in cm ohne Rinde zur gewählten Kombination von Durchmesser- und Güteklassengruppe in diesem Preismatrixeintrag im Verkauflos

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserBis](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/MittendurchmesserBis)

### GueteklasseVon (SubmodelElement)

Güteklasse (Minimum einschl.) zur gewählten Kombination von Eingangsgrößen in diesem Preismatrixeintrag im Verkauflos.

Wertebereich siehe [hier](#gueteklasse--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Gueteklasse](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Gueteklasse)

### GueteklasseBis (SubmodelElement)

Güteklasse (Maximum einschl.) zur gewählten Kombination von Eingangsgrößen in diesem Preismatrixeintrag im Verkauflos.

Wertebereich siehe [hier](#gueteklasse--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Gueteklasse](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Gueteklasse)

### Sortimentstyp (SubmodelElement)

Typ des Sortiments.

Wertebereich siehe [hier](#sortimentstyp--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Sortimentstyp](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Sortimentstyp)

### Sorte (SubmodelElement)

Sorte des Sortiments.

Wertebereich siehe [hier](#sorte--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Sorte](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Preismatrix/Preismatrixeintrag/Sorte)

### Verkaufsstatus (SubmodelElement)

Verkaufsstatus des Verkaufslos.

Wertebereich siehe [hier](#verkaufsstatus--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaufsstatus](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaufsstatus)

### Abrechnungsmass (SubmodelElement)

Abrechnungsmaß des Verkaufslos.

Wertebereich siehe [hier](#abrechnungsmass--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Abrechnungsmass](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Abrechnungsmass)

### Bereitstellungsart (SubmodelElement)

Bereitsstellungsart des Verkaufslos.

Wertebereich siehe [hier](#bereitstellungsart--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Bereitstellungsart](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Bereitstellungsart)

### VerfuegbarAb (SubmodelElement)

Früheste Verfügbarkeit des Verkaufslos (Datum) 

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/VerfuegbarAb](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/VerfuegbarAb)

### VerfuegbarBis (SubmodelElement)

Späteste Verfügbarkeit des Verkaufslos (Datum)

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/VerfuegbarBis](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/VerfuegbarBis)

### Verkaeufer (SubmodelElementCollection)

Verkäufer des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer)

### Firmenname (SubmodelElement)

Name der Firma des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Firmenname](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Firmenname)

### Anrede (SubmodelElement)

Anrede des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Anrede](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Anrede)

### Vorname (SubmodelElement)

Vorname des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Vorname](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Vorname)

### Nachname (SubmodelElement)

Nachname des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Nachname](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Nachname)

### Adresse (SubmodelElement)

Adresse des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Adresse](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnnummern des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Emailadressen](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Verkäufers des Verkaufslos, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/IBAN](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Verkäufers des Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Steuernummer](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Verkaeufer/Steuernummer)

### Notiz (SubmodelElement)

Notiz zum Verkaufslos.

[https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Notiz](https://admin-shell.io/kwh40/forestml40/Verkaufslos/0/1/Notiz)

## Arbeitsauftrag (Submodel Template)

Beschreibung eines Arbeitsauftrags für einen Einsatz im Waldbestand.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag)

### Hiebsnummer (SubmodelElement)

Nummer des Hiebs - Alphanummerischer Identifikator

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Hiebsnummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Hiebsnummer)

### Ansprechpartner (SubmodelElementCollection)

Ansprechpartner für diesen Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner)


### Firmenname (SubmodelElement)

Firmenname des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Firmenname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Firmenname)

### Anrede (SubmodelElement)

Anrede des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Anrede](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Anrede)

### Vorname (SubmodelElement)

Vorname des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Vorname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Vorname)

### Nachname (SubmodelElement)

Nachname des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Nachname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Nachname)

### Adresse (SubmodelElement)

Adresse des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Adresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Ansprechpartners zum Arbeitsauftrag..

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Emailadressen](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Ansprechpartners zum Arbeitsauftrag, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/IBAN](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Ansprechpartners zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Steuernummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Ansprechpartner/Steuernummer)

### UnternehmerHolzernte (SubmodelElementCollection)

Beschreibung des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte)

### Firmenname (SubmodelElement)

Firmenname des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Firmenname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Firmenname)

### Anrede (SubmodelElement)

Anrede des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Anrede](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Anrede)

### Vorname (SubmodelElement)

Vorname des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Vorname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Vorname)

### Nachname (SubmodelElement)

Nachname des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Nachname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Nachname)

### Adresse (SubmodelElement)

Adresse des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Adresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnnummern des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Emailadressen](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Unternehmers für die Holzernte zum Arbeitsauftrag, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/IBAN](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Unternehmers für die Holzernte zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Steuernummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzernte/Steuernummer)

### UnternehmerHolzrueckung (SubmodelElementCollection)

Beschreibung des Unternehmers für die Holzrückung zum Arbeitsauftrag

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung)

### Firmenname (SubmodelElement)

Firmenname des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Firmenname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Firmenname)

### Anrede (SubmodelElement)

Anrede des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Anrede](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Anrede)

### Vorname (SubmodelElement)

Vorname des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Vorname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Vorname)

### Nachname (SubmodelElement)

Nachname des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Nachname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Nachname)

### Adresse (SubmodelElement)

Adresse des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Adresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Emailadressen](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Unternehmers für die Rückung zum Arbeitsauftrag, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/IBAN](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Unternehmers für die Rückung zum Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Steuernummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/UnternehmerHolzrueckung/Steuernummer)

### Kurzbeschreibung (SubmodelElement)

Kurzbeschreibung des Arbeitsauftrags.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Kurzbeschreibung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Kurzbeschreibung)

### Sicherheitshinweise (SubmodelElement)

Sicherheitshinweise zum Arbeitsauftrag, Freitext.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sicherheitshinweise](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sicherheitshinweise)

### Naturschutzhinweise (SubmodelElement)

Naturschutzhinweise zum Arbeitsauftrag, Freitext.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Naturschutzhinweise](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Naturschutzhinweise)

### Sortimente (SubmodelElementList)

Lsite der Sortimente für diesen Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente)

### Sortiment (SubmodelElementCollection)

Einzelnes Sortiment für einen Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment)

### Nummer (SubmodelElementCollection)

Nummer des Sortiments im Arbeitsauftrag (eindeutig innerhalb des Arbeitsauftrags).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Nummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Nummer)

### Sortimentstyp (SubmodelElement)

Typ des Sortiments.

Wertebereich siehe [hier](#sortimentstyp--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Sortimentstyp](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Sortimentstyp)

### Sorte (SubmodelElement)

Sorte des Sortiments.

Wertebereich siehe [hier](#sorte--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Sorte](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Sorte)

### Holzart (SubmodelElement)

Holzart des Sortiments.

Wertebereich siehe [hier](#holzart--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Holzart](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Holzart)

### Gueteklasse (SubmodelElement)

Güteklasse des Sortiments.

Wertebereich siehe [hier](#gueteklasse--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Gueteklasse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Gueteklasse)

### LaengeVon (SubmodelElement)

Das Sortiment dieses Arbeitsauftrags enthält Stämme ab dieser Länge, in m

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/LaengeVon](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/LaengeVon)

### LaengeBis (SubmodelElement)

Das Sortiment dieses Arbeitsauftrags enthält Stämme bis zu dieser Länge, in m

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/LaengeBis](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/LaengeBis)

### Mindestzopf (SubmodelElement)

Mindestdurchmesser des Zopfs in diesem Sortiment dieses Arbeitsauftrags, in cm ohne Rinde

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Mindestzopf](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Mindestzopf)

### MaxDurchmesser (SubmodelElement)

Maximaler Durchmesser der Stämme in diesem Sortiment dieses Arbeitsauftrags, in cm ohne Rinde

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/MaxDurchmesser](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/MaxDurchmesser)

### Kaeufer (SubmodelElementCollection)

Beschreibung des Käufers dieses Sortiments dieses Arbeitsauftrags.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer)

### Firmenname (SubmodelElement)

Firmenname des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Firmenname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Firmenname)

### Anrede (SubmodelElement)

Anrede des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Anrede](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Anrede)

### Vorname (SubmodelElement)

Vorname des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Vorname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Vorname)

### Nachname (SubmodelElement)

Nachname des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Nachname](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Nachname)

### Adresse (SubmodelElement)

Adresse des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Adresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Emailadressen](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Käufers des Sortiments im Arbeitsauftrag, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/IBAN](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Käufers des Sortiments im Arbeitsauftrag.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Steuernummer](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Kaeufer/Steuernummer)

### Laengenzugabe (SubmodelElement)

Optionale Eintragung des Übermaßes von Stammlängen

Basiert auf externer Definition: -

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Laengenzugabe](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Laengenzugabe)

### Mengenschaetzung (SubmodelElement)

Einschätzung der Menge ohne vorhergehende detaillierte Vermessung

Basiert auf externer Definition: -

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Mengenschaetzung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Mengenschaetzung)

### Bemerkung (SubmodelElement)

Bemerkung zu Sortiment eines Arbeitsauftrags.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Bemerkung](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Bemerkung)

### Ergebnis (SubmodelElementList)

Beschreibung des Ergebnisses eines Arbeitsauftrags in Form einer Liste entstandener Holzlisten.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Ergebnis](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Ergebnis)

### Holzliste (SubmodelElement)

Einzelnes Ergebnis eines Arbeitsauftrags in Form einer entstandenen Holzliste.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Ergebnis/Holzliste](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Sortimente/Sortiment/Ergebnis/Holzliste)

### Karte (SubmodelElement)

Karte (URL oder BASE64-kodierte Bilddaten) zur Beschreibung des Arbeitsauftrags.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Karte](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Karte)

### Auftragsstatus (SubmodelElement)

Status des Arbeitsauftrags.

Wertebereich siehe [hier](#auftragsstatus--enum-).

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Auftragsstatus](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/Auftragsstatus)

### ZuFaellendeBaeume (SubmodelElementCollection)

Beschreibung der im Rahmen des Arbeitsauftrags zu fällenden Bäume.

[https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/ZuFaellendeBaeume](https://admin-shell.io/kwh40/forestml40/Arbeitsauftrag/0/1/ZuFaellendeBaeume)

## Holzliste (Submodel Template)

Beschreibung einer Holzliste, d.h. einer Auflistung geschlagenen Holzes.

[https://admin-shell.io/kwh40/forestml40/Holzliste](https://admin-shell.io/kwh40/forestml40/Holzliste)

### Kopfdaten (SubmodelElementCollection)

Kopfdaten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten)

### Projekt (SubmodelElement)

Projektbezeichnung für die Holzliste, Freitextfeld.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Projekt](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Projekt)

### Hieb (SubmodelElement)

Informationen zum Hieb.

Wertebereich siehe [hier](#hieb--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Hieb](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Hieb)

### Ernte (SubmodelElementCollection)

Informationen zur Ernte, Freitextfeld.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Ernte](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Ernte)

### Lieferant (SubmodelElementCollection)

Beschreibung des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant)

### Firmenname (SubmodelElement)

Firmenname des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Firmenname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Firmenname)

### Anrede (SubmodelElement)

Anrede des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Anrede](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Anrede)

### Vorname (SubmodelElement)

Vorname des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Vorname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Vorname)

### Nachname (SubmodelElement)

Nachname des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Nachname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Nachname)

### Adresse (SubmodelElement)

Adresse des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Adresse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Emailadressen](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Lieferanten der Holzliste, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/IBAN](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Lieferanten der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Steuernummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Lieferant/Steuernummer)

### Kaeufer (SubmodelElementCollection)

Beschreibung des Käufders der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer)

### Firmenname (SubmodelElement)

Firmenname des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Firmenname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Firmenname)

### Anrede (SubmodelElement)

Anrede des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Anrede](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Anrede)

### Vorname (SubmodelElement)

Vorname des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Vorname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Vorname)

### Nachname (SubmodelElement)

Nachname des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Nachname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Nachname)

### Adresse (SubmodelElement)

Adresse des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Adresse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Emailadressen](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Käufers der Holzliste, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/IBAN](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Käufers der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Steuernummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Kaeufer/Steuernummer)

### Fuhrmann (SubmodelElementCollection)

Beschreibung des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann)

### Firmenname (SubmodelElement)

Firmenname des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Firmenname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Firmenname)

### Anrede (SubmodelElement)

Anrede des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Anrede](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Anrede)

### Vorname (SubmodelElement)

Vorname des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Vorname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Vorname)

### Nachname (SubmodelElement)

Nachname des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Nachname](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Nachname)

### Adresse (SubmodelElement)

Adresse des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Adresse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Adresse)

### Telefonnummern (SubmodelElementList)

Liste von Telefonnummern des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Telefonnummern](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Telefonnummern)

### Telefonnummer (SubmodelElement)

Einzelne Telefonnummer des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Telefonnummern/Telefonnummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Telefonnummern/Telefonnummer)

### Emailadressen (SubmodelElementList)

Liste von E-Mail-Adressen des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Emailadressen](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Emailadressen)

### Emailadresse (SubmodelElement)

Einzelne E-Mail-Adresse des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Emailadressen/Emailadresse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Emailadressen/Emailadresse)

### Umsatzbesteuerung (SubmodelElement)

Hinterlegter Steuersatz des Fuhrmanns der Holzliste, der für die Steuerberechnung bei Verkäufen benötigt wird.

Wertebereich siehe [hier](#umsatzbesteuerung--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Umsatzbesteuerung](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Umsatzbesteuerung)

### IBAN (SubmodelElement)

IBAN des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/IBAN](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/IBAN)

### Steuernummer (SubmodelElement)

Umsatzsteuer-ID des Fuhrmanns der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Steuernummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Fuhrmann/Steuernummer)

### Notiz (SubmodelElement)

Notizen zur Holzliste, Freitextfeld.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Notiz](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Kopfdaten/Notiz)

### PolterListe (SubmodelElementList)

Liste von Poltern, welche die Holzliste umfasst.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe)

### Polter (SubmodelElementCollection)

Beschreibung eines Polters in der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter)

### Standort (SubmodelElementCollection)

Beschreibung des Standorts des Polters.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort)

### Koordinate (SubmodelElementCollection)

Lage des Polters in Geo-Koordinaten.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate)

### x (SubmodelElement)

Erster (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage des Polters. 

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/x](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/x)

### y (SubmodelElement)

Zweiter (gemäß Koordinatensystem-Definition) Koordinatenwert zur Lage des Polters.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/y](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/y)

### alt (SubmodelElement)

Höhenangabe über N.N. zur Lage des Polters.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/alt](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/alt)

### crs (SubmodelElement)

Verwendetes Koordinatensystem zur Beschreibung der Lage des Polters.

Wertebereich siehe [hier](#crs--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/crs](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Koordinate/crs)

### Notiz (SubmodelElement)

Notiz zur Beschreibung der Lage des Polters, Freitext.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Notiz](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Standort/Notiz)

### Polternummer (SubmodelElement)

Identifier für den Polter, einzigartig innerhalb der Holzliste.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Polternummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Polternummer)

### Vermessungsverfahren (SubmodelElement)

Definition des für den Polter verwendeten Vermessungsverfahrens.

Wertebereich siehe [hier](#vermessungsverfahren--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Vermessungsverfahren](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Vermessungsverfahren)

### Messergebnis_EinzelstammListe (SubmodelElementCollection)

Beschreibung eines Polters auf Basis einzelner vermessener Stämme.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe)

### Einzelstaemme (SubmodelElementList)

Liste an einzelnen Stämmen die im Polter gemessen wurden.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme)

### Einzelstamm (SubmodelElementCollection)

Einzelner Stamm, der im Polter gemessen wurde, und seine Eigenschaften.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm)

### Menge (SubmodelElement)

Volumen des Einzelstamms in X,XXX Fm o. R. (Festmeter ohne Rinde).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Menge](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Menge)

### Stammlaenge (SubmodelElement)

Länge des Stamms in X,XX m.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Stammlaenge](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Stammlaenge)

### Stammnummer (SubmodelElement)

Numerischer Identifier für einen Stamm in der Holzliste, eindeutig pro Polter.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Stammnummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Stammnummer)

### Gueteklasse (SubmodelElement)

Beschreibt die Güteklasse des einzelnen Stamms im Polter.

Wertebereich siehe [hier](#gueteklasse--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Gueteklasse](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Gueteklasse)

### Mittendurchmesser (SubmodelElement)

Mittendurchmesser des Stamms in X cm.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Mittendurchmesser](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Mittendurchmesser)

### Klammerstammabschnittsnummer (SubmodelElement)

Numerischer Identifier für den Abschnitt eines Einzelstamms, wird als Zahl X hinter der Stammnummer hinzugefügt (z.B. 1.X).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Klammerstammabschnittsnummer](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_EinzelstammListe/Einzelstaemme/Einzelstamm/Klammerstammabschnittsnummer)

### Messergebnis_RaummassIndustrieholz (SubmodelElementCollection)

Beschreibung eines Polters zur Industrieholz-Nutzung in Form von Menge und Stammlänge.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz)

### Menge (SubmodelElement)

Menge im Industrieholz-Polter in X,XX Fm o. R. (Festmeter ohne Rinde).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz/Menge](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz/Menge)

### Stammlaenge (SubmodelElement)

Stammlänge im Industrieholz-Polter in X,XX Metern.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz/Stammlaenge](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz/Stammlaenge)

### Stammanzahl (SubmodelElementCollection)

Menge im Industrieholz-Polter in X,XX Fm o. R. (Festmeter ohne Rinde).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz/Stammanzahl](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassIndustrieholz/Stammanzahl)

### Messergebnis_RaummassEnergieholz (SubmodelElementCollection)

Beschreibung eines Polters zur Energieholz-Nutzung in Form von Menge und Stammlänge.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassEnergieholz](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassEnergieholz)

### Menge (SubmodelElement)

Menge im Energieholz-Polter in X,XX Fm o. R. (Festmeter ohne Rinde).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassEnergieholz/Menge](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassEnergieholz/Menge)

### Stammlaenge (SubmodelElement)

Stammlänge im Energieholz-Polter in X,XX Metern.

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassEnergieholz/Stammlaenge](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Messergebnis_RaummassEnergieholz/Stammlaenge)

### Polterstatus (SubmodelElement)

Status des Polters, um Logistikprozess abzubilden (z. B. In Planung, Bereitgestellt, Verladen ...).

Wertebereich siehe [hier](#polterstatus--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Polterstatus](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Polterstatus)

### Fotos (SubmodelElementList)

Fotos des Polters (Liste von URLs).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Fotos](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Fotos)

### Foto (SubmodelElement)

Foto des Polters (URL)

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Fotos/Foto](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Fotos/Foto)

### Videos (SubmodelElementList)

Videos des Polters (Liste von URLs)

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Videos](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Videos)

### Video (SubmodelElement)

Video des Polters (URL).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Videos/Video](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/PolterListe/Polter/Videos/Video)

### Sortimentstyp (SubmodelElement)

Beschreibung des Sortiments der Holzliste (Stammholz, Industrieholz, Energieholz).

Wertebereich siehe [hier](#sortimentstyp--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Sortimentstyp](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Sortimentstyp)

### Sorte (SubmodelElement)

Beschreibung der Sorte der Holzliste (lang, kurz, Abschnitte ...).

Wertebereich siehe [hier](#sorte--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Sorte](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Sorte)

### Holzart (SubmodelElement)

Name der Holzart.

Wertebereich siehe [hier](#holzart--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Holzart](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Holzart)

### Holzlistenstatus (SubmodelElement)

Status der Holzliste.

Wertebereich siehe [hier](#holzlistenstatus--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Holzlistenstatus](https://admin-shell.io/kwh40/forestml40/Holzliste/0/1/Holzlistenstatus)

## Holzpreisbereiche (Submodel Template)

Bestimmung des Holzpreises je nach Stärkeklasse und Güteklasse.

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche)

### Holzpreisbereiche (SubmodelElementList)

Liste der Holzpreise je nach Stärkeklasse und Güteklasse.

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche)

### Holzpreisbereich (SubmodelElementCollection)

Bestimmung des Holzpreises je nach Stärkeklasse und Güteklasse.

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich)

### Baumart (SubmodelElement)

Bestimmung der Baumart, die für den Holzpreisbereich gilt

Wertebereich siehe [hier](#holzart--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Baumart](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Baumart)

### PreisVon (SubmodelElement)

Bestimmung Minimum-Preis der für den Holzpreisbereich gilt

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/PreisVon](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/PreisVon)

### PreisBis (SubmodelElement)

Bestimmung Maximum-Preis der für den Holzpreisbereich gilt

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/PreisBis](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/PreisBis)

### Sorte (SubmodelElement)

Bestimmung der Sorte, die für den Holzpreisbereich gilt

Wertebereich siehe [hier](#sorte--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Sorte](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Sorte)

### Gueteklasse (SubmodelElement)

Bestimmung der Güteklasse, die für den Holzpreisbereich gilt

Wertebereich siehe [hier](#gueteklasse--enum-).

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Gueteklasse](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Gueteklasse)

### MittendurchmesserVon (SubmodelElement)

Bestimmung des Minimum Durchmessers der für den Holzpreisbereich gilt

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/MittendurchmesserVon](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/MittendurchmesserVon)

### MittendurchmesserBis (SubmodelElement)

Bestimmung des Maximum Durchmessers der für den Holzpreisbereich gilt

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/MittendurchmesserBis](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/MittendurchmesserBis)

### Holzliste (SubmodelElement)

Angehängte Holzliste, die für den Holzpreisbereich gilt

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Holzliste](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Holzliste)

### Datum (SubmodelElement)

Zeitpunkt der Bestimmung des Holzpreisbereichs

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Datum](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Datum)

### Quelle (SubmodelElement)

Bestimmung der Herkunft/Quelle der Anfrage des Holzpreisbereichs

[https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Quelle](https://admin-shell.io/kwh40/forestml40/Holzpreisbereiche/0/1/Holzpreisbereiche/Holzpreisbereich/Quelle)

# Enumerationen

## Sortimentstyp (Enum)

Enumerationstyp zur Definition des Typs des Holzsortiments, basierend auf https://eldatstandard.de/dokumentation/1.0.2/grade (dortige Definitionen aufgetrennt nach Typ (z.B. ‚Stammholz‘) und Sorte (z.B. ‚lang‘) des Sortiments.

 * st - Stammholz
 * en - Energieholz
 * in - Industrieholz
 * so - Sondersortimente

## Sorte (Enum)

Enumerationstyp zur Definition der Sorte des Holzsortiments, basierend auf https://eldatstandard.de/dokumentation/1.0.2/grade (dortige Definitionen aufgetrennt nach Typ (z.B. ‚Stammholz‘) und Sorte (z.B. ‚lang‘) des Sortiments.

 * lg - lang
 * kz - kurz
 * ab - Abschnitte
 * hs - Hackschnitzel

## Holzart (Enum)

Enumerationstyp zur Definition der verschiedenen Holzarten bzw. Baumarten übernommen von https://eldatstandard.de/dokumentation/1.0.2/species.

 * xy - Keine
 * ndh - Nadelholz
 * fi - Fichte
 * gfi - Gemeine Fichte
 * ofi - Omorikafichte
 * sfi - Sitkafichte
 * swfi - Schwarzfichte
 * bfi - Blaufichte Stechfichte
 * efi - Engelmannsfichte
 * wfi - Weißfichte
 * sofi - Sonstige Fichten
 * kie - Kiefer
 * ki - Gemeine Kiefer
 * bki - Bergkiefer
 * ski - Schwarzkiefer
 * rki - Rumelische Kiefer
 * zki - Zirbelkiefer
 * wki - Weymouthskiefer
 * mki - Murraykiefer
 * gki - Gelbkiefer
 * soki - Sonstige Kiefer
 * ta - Tanne
 * wta - Weißtanne
 * ata - Amerikanische Edeltanne
 * cta - Coloradotanne
 * kta - Küstentanne
 * nita - Nikkotanne
 * nota - Nordmannstanne
 * vta - Veitchtanne
 * sota - Sonstige Tannen
 * dgl - Douglasie
 * la - Lärche
 * ela - Europäische Lärche
 * jla - Japanische Lärche (+Hybrid)
 * sla - Sonstige Lärchen
 * sonb - Sonstige Nadelbäume
 * lb - Lebensbaum
 * ht - Hemlockstanne
 * mam - Mammutbaum
 * eib - Eibe
 * sz - Lawsonszypresse
 * bu - Buche
 * sei - Stieleiche
 * tei - Traubeneiche
 * rei - Roteiche
 * zei - Zerreiche
 * suei - Sumpfeiche,
 * ei - Eiche
 * que - sonstige Eichen
 * es - Esche
 * ges - Gemeine Esche
 * wes - Weißesche
 * fra - Sonstige Eschen
 * hbu - Hainbuche (Weißbuche)
 * ah - Ahorn
 * bah - Bergahorn
 * sah - Spitzahorn
 * fah - Feldahorn
 * eah - Eschenblättriger Ahorn
 * siah - Silberahorn
 * ace - Sonstige Ahorne
 * li - Linde
 * wli - Winterlinde
 * sli - Sommerlinde
 * til - Sonstige Linden
 * rob - Robinie
 * akz - Akazie
 * ul - Ulme
 * bul - Bergulme
 * ful - Feldulme
 * flu - Flatterulme
 * ulm - Sonstige Ulmen
 * rka - Rosskastanie
 * eka - Edelkastanie
 * ka - Kastanie
 * mau - Weißer Maulbeerbaum
 * nus - Nussbaum
 * wnu - Walnuss
 * snu - Schwarznuss (+Hybriden)
 * jug - Sonstige Nussbäume
 * ste - Stechpalme
 * pla - Platane
 * apl - Ahornblättrige Platane
 * solh - Sonstige Laubbäume mit hoher Lebensdauer
 * gbi - Gemeine Birke
 * mbi - Moorbirke (+Karpatenbirke)
 * bi - Birke
 * erl - Erle
 * ser - Schwarzerle
 * wer - Weißerle Grauerle
 * ger - Grünerle
 * aln - Sonstige Erlen
 * pap - Pappel
 * zpa - Aspe Zitterpappel
 * spa - Europäische Schwarzpappel
 * spah - Schwarzpappel Hypriden
 * gpa - Graupappel (+Hybriden)
 * wpa - Silberpappel Weißpappel
 * bpa - Balsampappel
 * bpah - Balsampappel Hybriden
 * pop - Sonstige Pappeln
 * sor - Sorbusarten
 * sso - Sonstige Sorbusarten
 * vb - Vogelbeere
 * els - Elsbeere
 * spe - Speierling
 * meb - Echte Mehlbeere
 * wei - Weide
 * swei - Salweide
 * kir - Kirsche
 * gtk - Gewöhnliche Traubenkirsche
 * vk - Vogelkirsche
 * stk - Spätblühende Traubenkirsche
 * pru - Sonstige Kirschen
 * zwe - Zwetschge
 * hic - Hickory
 * soln - Sonstige Laubbäume mit niedriger Lebensdauer
 * fau - Gemeiner Faulbaum Pulverholz
 * wob - Wildobst (unbestimmt)
 * wap - Holzapfel Wildapfel
 * wbi - Holzbirne Wildbirne
 * has - Baumhasel
 * got - Gemeiner Götterbaum
 * slbh - Sonstiges Hartlaubholz
 * lbh - Laubholz
 * slbw - Sonstiges Weichlaubholz
 * str - Strauch (unbestimmt)
 * fita - Mischsortiment Fichte/Tanne    

## Vermessungsverfahren (Enum)

Enumerationstyp zur Definition der Vermessungsart, d.h. dem Verfahren das zur Vermessung verwendet wurde (gegebenenfalls nicht abrechnungsrelevant), übernommen von https://eldatstandard.de/dokumentation/1.0.2/measuring_medium.

 * mit - Mittenstärkenvermessung
 * mis - Mittenstärkenstichprobenverfahren
 * arm - Raummaßverfahren allgemein
 * srm - Sektionsraummaßverfahren
 * krm - Konventionelles Raummaßverfahren
 * stf - Stirnflächenverfahren
 * zae - Zählung
 * stz - Schätzung
 * wev - Werkvermessung
 * gwm - Gewichtsmaßermittlung
 * gwa - Gewichtsmaßermittlung atro
 * gwl - Gewichtsmaßermittlung lutro
 * stg - Stangenvermessung
 * zsv - Zopfstärkenvermessung
 * fra - Fremdvermessung
 * lfm - Laufmeter
 * son - Sonstiges
 * hvm - Harvestermaß
 * opv - optische Poltervermessung
 * wam - Waldmaß
 * xy - Keins

##  Gueteklasse (Enum)

Enumerationstyp zur Definition der Güteklasse (Qualität) des Holzes, übernommen von https://eldatstandard.de/dokumentation/1.0.2/qual_type.

 * o - Ohne Qualität
 * in - Normale Qualität
 * if - Fehlerhafte Qualität
 * ik - Krank
 * fk - Qualität fehlerhaft/krank
 * nf - Qualität normal/fehlerhaft
 * nfk - Qualität normal/fehlerhaft/krank
 * b - Qualität B
 * bk - Qualität B Rotkern
 * c - Qualität C
 * d - Qualität D
 * oa - Ohne Qualitätsausscheidung
 * bc - B/C Mischqualität
 * cd - C/D Mischqualität
 * bcd - B/C/D Mischqualität
 * a - Qualität A
 * ak - Qualität A Rotkern

In Deutschland keine Anwendung finden dabei:

 * a_fu - Furnierholz (ÖNORM L 2021)
 * a_is - Schleifholz (ÖNORM L 2021)
 * a_if - Faserholz (ÖNORM L 2021)
 * a_i2 - Sekunda (ÖNORM L 2021)
 * a_id - Industriedünnholz (ÖNORM L 2021)
 * a_im - Manipulationsholz (ÖNORM L 2021)
 * a_sp - Splitterholz (ÖNORM L 2021)
 * a_y - Braunbloche (ÖNORM L 2021)
 * a_x - C-Kreuz (noch sägefähiger Ausschuss) (ÖNORM L 2021)
 * a_z - Ausschuss (ÖNORM L 2021)
 * a_bh - Brennholz (ÖNORM L 2021)
 * ch_k - Qualität Käferholz
 * ch_ab - Qualität AB
 * ch_bc - Qualität BC
 * ch_cd - Qualität CD
 * ch_abc - Qualität ABC
 * ch_r - Qualität Rotholz
 * ch_1 - Qualität 1. Klasse
 * ch_2 - Qualität 2. Klasse
 * ch_bk - Braunkern
 * ch_sk - Spritzkern

## CRS (Enum)

 * EPSG:25832 - ETRS89 / UTM zone 32N
 * ...

Enumerationstyp zur Definition von Codes für Geokoordinatensysteme (Coordinate Reference System, CRS) basierend auf https://spatialreference.org/.

## Verkaufsstatus (Enum)

Enumerationstyp zur Definition des Status im Verkaufsprozess.

 * Entwurf
 * Veroeffentlicht
 * HatPreisvorschlaege
 * Verkauft
 * Archiviert

## Auftragsstatus (Enum)

Enumerationstyp zur Definition des Auftragsstatus, übernommen von https://eldatstandard.de/dokumentation/1.0.2/statusid.

 * 10 - Erstellt
 * 20 - Geändert
 * 30 - Storniert
 * 40 - Gesendet
 * 50 - Angenommen
 * 60 - Abgelehnt
 * 70 - Disponiert
 * 80 - Auftragsbeginn
 * 90 - Unterbrochen
 * 100 - Abgebrochen
 * 110 - Fahre ins Revier
 * 120 - Lieferschein erstellt
 * 130 - Verlasse Revier
 * 140 - Am Lieferort angekommen
 * 150 - Auftragsende

## Hieb (Enum)

 * sh - Sammelhieb
 * df - Durchforstung
 * kl - Kalamität

Eigendefinition.

## Bereitstellungsart (Enum)

Enumerationstyp zur Definition der Bereitstellungsart des Holzes, übernommen von https://eldatstandard.de/dokumentation/1.0.2/delivery_term.

 * uws - Unfrei Waldstraße
 * uwe - Unfrei Werk
 * uzw - Unfrei Zwischenlager
 * uwa - Unfrei Waggon
 * usc - Unfrei Schiff
 * exw - Frei Stock / Ab Werk incoterm
 * fca - Frei Waldstraße / Frei Frachtführer incoterm
 * fob - Frei Schiff/Waggon / Frei an Bord incoterm
 * dat - Frei Zwischenlager / Geliefert frei Terminal incoterm
 * ddp - Frei Werk / Geliefert verzollt incoterm
 * cpt - Frachtfrei bis incoterm
 * cip - Frachtfrei versichert bis incoterm
 * dap - Geliefert benannter Ort incoterm
 * fas - Frei längsseits Schiff incoterm
 * cfr - Kosten und Fracht incoterm
 * cif - Frachtfrei incoterm

## Abrechnungsmass (Enum)

Enumerationstyp zur Definition des Abrechnungsmaßes im Sinne des abrechnungsrelevanten Vermessungsverfahrens („Angabe des Vermessungsverfahrens nach dem abgerechnet wird“), übernommen von https://eldatstandard.de/dokumentation/1.0.2/measurement_method.

 * mit - Mittenstärkenvermessung
 * mis - Mittenstärkenstichprobenverfahren
 * arm - Raummaßverfahren allgemein
 * srm - Sektionsraummaßverfahren
 * krm - Konventionelles Raummaßverfahren
 * stf - Stirnflächenverfahren
 * zae - Zählung
 * stz - Schätzung
 * wev - Werkvermessung
 * gwm - Gewichtsmaßermittlung
 * gwa - Gewichtsmaßermittlung atro
 * gwl - Gewichtsmaßermittlung lutro
 * stg - Stangenvermessung
 * zsv - Zopfstärkenvermessung
 * fra - Fremdvermessung
 * lfm - Laufmeter
 * son - Sonstiges
 * hvm - Harvestermaß
 * opv - optische Poltervermessung
 * wam - Waldmaß
 * xy - Keins

## Umsatzbesteuerung (Enum)

 * regelbesteuert
 * pauschalbesteuert
 * sortimentsbezogen

Eigendefinition.

## Polterstatus (Enum)

Beschreibung logistische Aspekte. Für zukünftige Nutzung reserviert.

## Holzlistenstatus (Enum)

Beschreibung kaufmännische Aspekte. Für zukünftige Nutzung reserviert.
