import copy
import json

"""
Input: Intermediate JSON file created from PlantUML file using https://github.com/Enteee/plantuml-parser
Output: Concept description stubs for a readme.md corresponding to https://github.com/admin-shell-io/id/ 
"""

HOLZLISTE_PUML_JSON = 'holzliste_puml.json'
README_MD = "README.md"
SEMANTIC_ID_PREFIX = "https://admin-shell.io/kwh40/forestml40"
VERSION = 0
REVISION = 1


def create_semantic_id(recursion_path):
    if len(recursion_path) == 1:
        return f"{SEMANTIC_ID_PREFIX}/{recursion_path[0]}"
    elif len(recursion_path) > 1:
        rest_path = "/".join(recursion_path[1:len(recursion_path)])
        return f"{SEMANTIC_ID_PREFIX}/{recursion_path[0]}/{VERSION}/{REVISION}/{rest_path}"
    else:
        raise Exception("empty recursion_path")


def dissolve_type_recursively(parent_elem, recursion_path):
    # recurse into base classes, currently, no existing case
    # parent_classname = elem['name']
    # if parent_classname in map_class_to_baseclass:
    #     baseclass_name = map_class_to_baseclass[parent_classname]
    #     if baseclass_name not in map_SMEC:
    #         raise Exception(f"baseclass_name {baseclass_name} not in map_SMEC {map_SMEC.keys()}")
    #     baseclass_elem = map_SMEC.get(baseclass_name)
    #     dissolve_type_recursively(baseclass_elem,recursion_path)

    for member in parent_elem.get('members', []):
        member_name = member['name']
        member_type = member['type']
        is_se_list = member_type.split(".")[0] == "SEList"
        is_enum = member_type.split(".")[0] == "ENUM"
        is_reference = "Reference" in member_type.split(".")  # or "SM" in member_type.split(".")
        member_type = member_type.split(".")[-1]
        is_simple_type = member_type in simple_data_types
        is_known_smec = member_type in map_SMEC
        member_recursion_path = copy.deepcopy(recursion_path)
        member_recursion_path.append(member_name)

        # produce documentation for current member
        property_type = "SubmodelElement" if member_type in simple_data_types or is_enum or is_reference else "SubmodelElementCollection" if not is_se_list else "SubmodelElementList"
        print(member_recursion_path)  # debug
        output_data.append(f"### {member_name} ({property_type})")
        output_data.append("TODO add description here")
        output_data.append("Basiert auf externer Definition: -")
        semantic_id = create_semantic_id(member_recursion_path)
        output_data.append(f"[{semantic_id}]({semantic_id})")

        if is_se_list:
            member_recursion_path.append(f"{member_name}{{00}}")

            # produce documentation for current member - list element
            property_type = "SubmodelElement" if member_type in simple_data_types or is_enum or is_reference else "SubmodelElementCollection"
            print(member_recursion_path)  # debug
            output_data.append(f"### {member_name} ({property_type})")
            output_data.append("TODO add description here")
            output_data.append("Basiert auf externer Definition: -")
            semantic_id = create_semantic_id(member_recursion_path)
            output_data.append(f"[{semantic_id}]({semantic_id})")

        if is_known_smec:
            if member_type not in map_SMEC:
                raise Exception(f"member_type {member_type} not in map_SMEC {map_SMEC.keys()}")
            member_type_definition = map_SMEC.get(member_type)
            dissolve_type_recursively(member_type_definition, member_recursion_path)


simple_data_types = ["String", "Date", "Float", "Double", "URL"]

json_data = None
with open(HOLZLISTE_PUML_JSON, 'r') as json_file:
    json_data = json.load(json_file)

diagram_elements = json_data[0]['diagrams'][0]['elements']

output_data = []

# 1st pass: collect classes and associations
map_class_to_stereotype = dict()
map_class_to_baseclass = dict()
map_SMEC = dict()
for elem in diagram_elements:
    # if elem.get('leftArrowHead', "") == "^":  # inheritance
    #     base_class = elem['left']
    #     derived_class = elem['right']
    #     map_class_to_baseclass[derived_class] = base_class
    if "isAbstract" in elem:  # class
        if len(elem['stereotypes']) != 1:
            raise "len(elem['stereotypes']) == 1"
        stereotype = elem['stereotypes'][0]
        classname = elem['name']
        map_class_to_stereotype[classname] = stereotype
        if stereotype == "SubmodelElementCollection":
            map_SMEC[classname] = elem

# 2nd pass
# collect content (= attributes) of all SMEC classes
# to dissolve UML attribute types to content of AAS property
map_class_to_content = dict()
for elem in diagram_elements:
    if "isAbstract" in elem:  # class
        stereotypes = elem['stereotypes']
        if len(stereotypes) != 1:
            raise "len(stereotypes) == 1"
        elif "Submodel" in stereotypes:
            submodel_name = elem.get('name', "")

            # produce documentation for submodel
            print([submodel_name])  # debug
            output_data.append(f"## {submodel_name} (Submodel Template)")
            output_data.append("TODO add description here")
            semantic_id = create_semantic_id([submodel_name])
            output_data.append(f"[{semantic_id}]({semantic_id})")

            dissolve_type_recursively(elem, [submodel_name])

spaced_output_data = ["\n\n"] * (2 * len(output_data))
spaced_output_data[::2] = output_data

with open(README_MD, 'w') as readme_md_file:
    readme_md_file.writelines(spaced_output_data)
