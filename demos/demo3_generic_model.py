import os, sys, inspect
from typing import List

import basyx
from basyx.aas import model
from basyx.aas.adapter.json import json_serialization
import datetime

from model import enums, generic_model


def create_arbeitsauftrag():
    arbeitsauftrag = generic_model.Arbeitsauftrag(
        id_="https://www.company.com/submodels/arbeitsauftrag",
        hiebsnummer="abc123",
        kurzbeschreibung="beschreibung",
        ansprechpartner=generic_model.Arbeitsauftrag.Ansprechpartner(),
        unternehmerHolzrueckung=generic_model.Arbeitsauftrag.UnternehmerHolzrueckung(),
        unternehmerHolzernte=generic_model.Arbeitsauftrag.UnternehmerHolzernte(),
        sortimente=[create_sortiment()],
        zuFaellendeBaeume=model.ModelReference.from_referable(create_zu_faellende_baeume())
    )
    return arbeitsauftrag


def create_sortiment():
    sortiment = generic_model.Arbeitsauftrag.Sortimente.Sortiment(
        nummer=1,
        sortimentstyp=enums.Sortimentstyp.Stammholz.value,
        sorte=enums.Sorte.lang.value,
        holzart=enums.Holzart.Fichte.value,
        gueteklasseVon=enums.Gueteklasse.Qualitaet_A.value,
        gueteklasseBis=enums.Gueteklasse.Qualitaet_B.value,
        laengeVon=3.0,
        laengeBis=6.0,
        mindestzopf=1.0,
        maxDurchmesser=10.0,
        laengenzugabe=10.0,
        mengenschaetzung=10.0,
        bemerkung="Bemerkung",
        ergebnis=generic_model.Arbeitsauftrag.Sortimente.Sortiment.Ergebnis([model.ModelReference.from_referable(create_holzliste())]),
        kaeufer=generic_model.Arbeitsauftrag.Sortimente.Sortiment.Kaeufer(kontaktrolle=enums.Kontaktrolle.Kaeufer.value)
    )
    return sortiment


def create_zu_faellende_baeume():
    umring = generic_model.ZuFaellendeBaeume.Umring(
        name="name", beschreibung="beschreibung",
        koordinaten=generic_model.ZuFaellendeBaeume.Umring.Koordinaten([
            generic_model.ZuFaellendeBaeume.Umring.Koordinaten.Koordinate(x=30.00004, y=120.80),
            generic_model.ZuFaellendeBaeume.Umring.Koordinaten.Koordinate(x=31.00004, y=121.80),
            generic_model.ZuFaellendeBaeume.Umring.Koordinaten.Koordinate(x=32.00004, y=122.80),
        ]))

    zufaellendebaeume = generic_model.ZuFaellendeBaeume(
        id_="https://www.company.com/submodels/zu_faellende_baeume",
        beschreibung="Baueme sollen demnaechst gefaellt werden",
        umring=umring,
        position=generic_model.ZuFaellendeBaeume.Position(
            koordinate=generic_model.ZuFaellendeBaeume.Position.Koordinate(x=31.00004, y=121.80),
            notiz="Die Position"
        ),
        baumliste=(
            generic_model.ZuFaellendeBaeume.Baumliste.StehenderBaum(
                hoehe=15.3, bhd=20.2, baumart=enums.Holzart.Buche.value,
                position=generic_model.ZuFaellendeBaeume.Baumliste.StehenderBaum.Position(
                    koordinate=generic_model.ZuFaellendeBaeume.Position.Koordinate(x=31.00004, y=121.80),
                    notiz="Position"
                )),
        ),
    )
    return zufaellendebaeume


def create_holzliste():
    kopfdaten = generic_model.Holzliste.Kopfdaten(
        projekt="Eslohe2023",
        hieb=enums.Hieb.Durchforstung.value,
        ernte=generic_model.Holzliste.Kopfdaten.Ernte(min=datetime.date(2023, 12, 1), max=datetime.date(2023, 12, 31)),
        lieferant=generic_model.Holzliste.Kopfdaten.Lieferant(),
        kaeufer=generic_model.Holzliste.Kopfdaten.Kaeufer(),
        fuhrmann=generic_model.Holzliste.Kopfdaten.Fuhrmann(),
        notiz="Dieses Holz ist sehr gut"
    )

    messergebnis_einzelstammliste = generic_model.Holzliste.PolterListe.Polter.Messergebnis_EinzelstammListe(
        generic_model.Holzliste.PolterListe.Polter.Messergebnis_EinzelstammListe.Einzelstaemme([
            generic_model.Holzliste.PolterListe.Polter.Messergebnis_EinzelstammListe.Einzelstaemme.Einzelstamm(
                menge=3.4,
                stammlaenge=4.2,
                stammnummer="1",
                mittendurchmesser=47.0,
                gueteklasse=enums.Gueteklasse.Normale_Qualitaet.value
            )
        ])
    )

    polter = generic_model.Holzliste.PolterListe.Polter(
        polternummer="100",
        vermessungsverfahren=enums.Vermessungsverfahren.Mittenstaerkenvermessung.value,
        polterstatus=enums.Polterstatus.Vermessen.value,
        messergebnis_EinzelstammListe=messergebnis_einzelstammliste,
        standort=generic_model.Holzliste.PolterListe.Polter.Standort(
            koordinate=generic_model.Holzliste.PolterListe.Polter.Standort.Koordinate(x=31.00004, y=121.80),
            notiz="Die Position"
        ),
        fotos=[
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Altenbrak_Holz.JPG/1920px-Altenbrak_Holz.JPG",
            "https://de.wikipedia.org/wiki/Polter#/media/Datei:Baumst%C3%A4mme_bei_St%C3%BCbeckshorn.jpg"],
        videos=["https://www.youtube.com/watch?v=b65SSRfDwUo"],
    )

    holzliste = generic_model.Holzliste(
        id_="https://www.company.com/holzliste/1",
        kopfdaten=kopfdaten,
        polterListe=generic_model.Holzliste.PolterListe([polter]),
        sortimentstyp=enums.Sortimentstyp.Stammholz.value,
        sorte=enums.Sorte.lang.value,
        holzart=enums.Holzart.Fichte.value)

    return holzliste


def create_aas():
    arbeitsauftrag = create_arbeitsauftrag()
    holzliste = create_holzliste()
    zu_faellende_baeume = create_zu_faellende_baeume()

    submodels = [
        arbeitsauftrag,
        holzliste,
        zu_faellende_baeume,
    ]
    aas_dz_wald = model.AssetAdministrationShell(
        id_="https://www.company.com/dz_wald/1",
        id_short="DZWald",
        asset_information=model.AssetInformation(),
        submodel=submodels
    )
    return aas_dz_wald, submodels


def write_aas():
    aas, submodels = create_aas()

    objstore = basyx.aas.model.DictObjectStore(submodels)
    objstore.add(aas)

    with open("generic_dz_wald_example.json", "w", encoding='utf-8') as file:
        json_serialization.write_aas_json_file(file, objstore)


if __name__ == "__main__":
    write_aas()
