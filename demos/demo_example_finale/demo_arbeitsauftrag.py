from demos.demo_example_finale.utils import save_and_print_aas_obj
from model.enums import Auftragsstatus
from model.generic_model import Arbeitsauftrag, ZuFaellendeBaeume
from model.models import Koordinate

if __name__ == '__main__':
    coord1 = Koordinate(x=12.132925613639593, y=49.04954714847827, crs="EPSG:4326")  # todo implement CRS
    coord2 = Koordinate(x=12.114363040065479, y=49.04887650011801, crs="EPSG:4326")
    coord3 = Koordinate(x=12.112590150688762, y=49.03921011545453, crs="EPSG:4326")

    stehender_baum_1 = ZuFaellendeBaeume.Baumliste.StehenderBaum(
        hoehe=float(19),  # in metres
        bhd=float(5),  # in metres
        baumart="fi",  # e.g. fichte
        position=ZuFaellendeBaeume.Baumliste.StehenderBaum.Position(coord1),
        category="Fällen"
    )

    stehender_baum_2 = ZuFaellendeBaeume.Baumliste.StehenderBaum(
        hoehe=float(20),  # in metres
        bhd=float(4),  # in metres
        baumart="fi",  # e.g. fichte
        position=ZuFaellendeBaeume.Baumliste.StehenderBaum.Position(coord2),
        category="Fällen"
    )

    stehender_baum_3 = ZuFaellendeBaeume.Baumliste.StehenderBaum(
        hoehe=float(25),  # in metres
        bhd=float(4),  # in metres
        baumart="bi",  # e.g. birke
        position=ZuFaellendeBaeume.Baumliste.StehenderBaum.Position(coord3),
        category="Einsammeln"
    )
    baumliste = [stehender_baum_1, stehender_baum_2, stehender_baum_3]  # nicht sortenrein

    zu_faellende_baeume = ZuFaellendeBaeume(
        id_="1",  # todo what to fill in in id_? -> random uuid?
        position=None,  # im ForestManager sind die positionen in der Baumliste
        baumliste=baumliste,
        beschreibung="",
        umring=ZuFaellendeBaeume.Umring(  # todo Umring sollte optional sein
            beschreibung="",
            name="",
            koordinaten=ZuFaellendeBaeume.Umring.Koordinaten(koordinate=[])  # empty
        )
    )

    arbeitsauftrag = Arbeitsauftrag(id_="1",  # todo what to fill in in id_? -> random uuid?
                                    hiebsnummer=Arbeitsauftrag.Hiebsnummer(value="Hieb1"),  # string input
                                    kurzbeschreibung="Bitte Bäume fällen.",  # string input
                                    sicherheitshinweise="Sicherheitshinweise",  # string input
                                    naturschutzhinweise=None,  # string input
                                    sortimente=None,  # Sortimente werden vom Forestmanager nicht unterstützt
                                    zuFaellendeBaeume=zu_faellende_baeume,
                                    auftragsstatus=str(Auftragsstatus.Gesendet))

    save_and_print_aas_obj("Arbeitsauftrag", arbeitsauftrag)
