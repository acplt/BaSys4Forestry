from datetime import date

from demos.demo_example_finale.utils import save_and_print_aas_obj
from model.generic_model import Waldweg
from model.models import Koordinate, Standort

if __name__ == '__main__':
    coord1 = Koordinate(x=12.132925613639593, y=49.04954714847827, crs="EPSG:4326")  # todo implement CRS
    coord2 = Koordinate(x=12.114363040065479, y=49.04887650011801, crs="EPSG:4326")
    coord3 = Koordinate(x=12.112590150688762, y=49.03921011545453, crs="EPSG:4326")
    coord4 = Koordinate(x=12.08947871627966, y=49.03784924275019, crs="EPSG:4326")
    coord_wendepunkt = Koordinate(x=12.111363040065479, y=49.04837650011801, crs="EPSG:4326")

    standort_coord1 = Standort(coord1)
    standort_coord2 = Standort(coord2)
    standort_coord3 = Standort(coord3)
    standort_coord4 = Standort(coord4)
    standort_wendepunkt = Standort(coord_wendepunkt)

    wegpunkte = Waldweg.Wegpunkte(standort=[standort_coord1, standort_coord2, standort_coord3, standort_coord4])
    wendepunkte = Waldweg.Wendepunkte(standort=standort_wendepunkt)
    beschreibung = Waldweg.Beschreibung(value="Das ist ein Waldweg")
    erstellungsdatum = date.fromtimestamp(1692620103)
    aktualisierungsdatum = date.fromtimestamp(1692620122)
    breite = float(3)  # in metres

    waldweg = Waldweg(id_="1",  # todo what to fill in in id_? -> random uuid?
                      wegpunkte=wegpunkte,
                      wendepunkte=wendepunkte,
                      beschreibung=beschreibung,
                      erstellungsdatum=erstellungsdatum,
                      aktualisierungsdatum=aktualisierungsdatum,
                      breite=breite,
                      )
    save_and_print_aas_obj("Waldweg", waldweg)
