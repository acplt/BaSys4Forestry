import basyx
from basyx.aas.adapter.json import AASToJsonEncoder


def save_and_print_aas_obj(desc: str, obj):
    encoder = AASToJsonEncoder()
    json_string = encoder.encode(obj)
    # print
    print("{0}: {1}".format(desc, json_string))
    # to file
    with open("output/{0}.json".format(desc), 'w') as outfile:
        outfile.write(json_string)
