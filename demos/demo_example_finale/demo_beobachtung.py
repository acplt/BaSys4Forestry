from demos.demo_example_finale.utils import save_and_print_aas_obj
from model.generic_model import Beobachtung
from model.models import Umring, Koordinate

if __name__ == '__main__':
    coord1 = Koordinate(x=12.132925613639593, y=49.04954714847827, crs="EPSG:4326")  # todo implement CRS
    coord2 = Koordinate(x=12.114363040065479, y=49.04887650011801, crs="EPSG:4326")
    coord3 = Koordinate(x=12.112590150688762, y=49.03921011545453, crs="EPSG:4326")
    coord4 = Koordinate(x=12.08947871627966, y=49.03784924275019, crs="EPSG:4326")
    coord5 = Koordinate(x=12.132925613639593, y=49.04954714847827, crs="EPSG:4326")
    coords = [coord1, coord2, coord3, coord4, coord5]

    umring = Umring(name="Umring Name", koordinaten=coords, beschreibung="Umring Beschreibung")

    beobachtung = Beobachtung(id_="1",  # todo what to fill in in id_? -> random uuid?
                              name="Beobachtung Name",
                              beschreibung="Beschreibung Name",
                              umring=umring,
                              )

    save_and_print_aas_obj("Beobachtung", beobachtung)
