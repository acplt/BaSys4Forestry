from s3i import BrokerREST, IdentityProvider, TokenType
from basyx.aas.adapter import aasx
from basyx.aas import model
import base64
import io

"""
This serves as a dummy HMI for Forestify that 
(1) receives an aasx file from ForestManager using S3I and 
(2) uses the pyi40aas library to process it. 
"""

# The id of ForestManager's HMI is known beforehand
forestmanager_hmi_id = "s3i:e8ef672c-109b-4c36-8999-f4ababa0bffc"

# These are our HMI's credentials
forstify_hmi_id = "s3i:8a8ee1ab-63d2-42ea-92d1-1ae682a55e7a"
forstify_hmi_secret = "U9AkkeEXbOZNNFUYQvH5hb3WhbBGaw9a"


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=forstify_hmi_id,
        client_secret=forstify_hmi_secret,
    )
    return idp


def receive_msg():
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)
    broker = BrokerREST(access_token)
    forestify_endpoint = "s3ibs://" + forstify_hmi_id
    # Get message from S3I-Broker
    msg = broker.receive_once(queue=forestify_endpoint)
    if msg:
        print(f"[S3I] Message successfully received from {msg['sender']}.")
    else:
        print("[S3I] No new messages.")
    return msg


def main():
    # Step 1: Retrieve message from broker and extract the file's data
    msg = receive_msg()
    data = base64.b64decode(msg["attachments"][0]["data"])

    # Step 2: Load the data into an object-store, which serves as a registry of identifiables (incl. shells and submodels)
    object_store = model.DictObjectStore()
    file_store = aasx.DictSupplementaryFileContainer()
    with aasx.AASXReader(io.BytesIO(data)) as reader:
        reader.read_into(object_store=object_store, file_store=file_store)
        # Fetch the AAS from the object-store
        aas: model.AssetAdministrationShell = object_store.get_identifiable(
            model.Identifier("https://www.company.com/shells/rohholz")
        )
        print(
            f"The received aasx contains an aas with (id_short)/(id): ({aas.id_short})/({aas.id})."
        )
        # Fetch all the submodels referenced by the AAS from the object-store
        submodels_list = [ref.resolve(object_store) for ref in aas.submodel]
        submodels_dict = {x.id_short: x for x in submodels_list}
        print(f"{aas.id_short} contains the following submodels:")
        print(f"    {[*submodels_dict]}")
        # Get the submodel elements from the 'holzliste' submodel
        holzliste = submodels_dict.get("holzliste")
        print(f"{holzliste.id_short} contains the following submodel-elements:")
        print(f"    {[*holzliste.submodel_element._backend]}")
        # Get the woodpile's type from the 'holzart' submodel-element
        print(f"{holzliste.id_short} is of type (from the 'holzart' submodel-element):")
        # 'holzart' is a property in the 'holzliste' submodel
        holzart: model.Property = holzliste.submodel_element.get_referable("holzart")
        print(f"    {holzart.id_short}: {holzart.value} ({holzart.description})")
        # Get the woodpile's location from the 'standort' submodel-element
        print(
            f"{holzliste.id_short} is located at (from the 'standort' submodel-element):"
        )
        # 'standort' is a submodelelementcollection in the 'holzliste' submodel
        standort: model.SubmodelElementCollection = (
            holzliste.submodel_element.get_referable("standort")
        )
        for subelemet in standort.value:
            # For each property in standort, print its id-short, value and description
            if isinstance(subelemet, model.Property):
                property: model.Property = subelemet
                print(
                    f"    {property.id_short}: {property.value} ({property.description})"
                )


if __name__ == "__main__":
    main()
