import json
import base64
import uuid
from s3i import BrokerREST, IdentityProvider, UserMessage, TokenType
from basyx.aas import model
from basyx.aas.adapter import aasx
import io

"""
This serves as a dummy HMI for ForestManager that 
(1) uses S3I to send an aasx file with wood pile data. 
Additionaly the @create_aasx function shows how the aasx can be created using the pyi40aas library. 
"""

# These are our HMI's credentials
forestmanager_hmi_id = "s3i:e8ef672c-109b-4c36-8999-f4ababa0bffc"
forestmanager_hmi_secret = "cfs2YMj2bzPIS3kDWpHYFyUKzCorAQuV"

# The id of forstify's HMI is known beforehand
forstify_hmi_id = "s3i:8a8ee1ab-63d2-42ea-92d1-1ae682a55e7a"


def create_aasx():
    """
    This function creates an aasx and returns it as an io.Bytes object
    """
    asset_info = model.AssetInformation(
        asset_kind=model.AssetKind.INSTANCE,
        global_asset_id=model.GlobalReference(
            (
                model.Key(
                    type_=model.KeyTypes.GLOBAL_REFERENCE,
                    value="https://www.company.com/assets/rohholz",
                ),
            )
        ))
    vv_sid = model.Reference(
        (
            model.Key(
                type_=model.KeyTypes.GLOBAL_REFERENCE,
                value="https://www.eldatstandard.de/dokumentation/1.0.2/measurement_method",
            ),
        )
    )
    vermessungsverfahren = model.Property(
        id_short="vermessungsverfahren",
        value_type=model.datatypes.String,
        value="mit",
        description={
            "de": "Angabe des Vermessungsverfahrens nach dem abgerechnet wird"
        },
        semantic_id=vv_sid,
    )
    sorte_sid = model.Reference(
        (
            model.Key(
                type_=model.KeyTypes.GLOBAL_REFERENCE,
                value="https://eldatstandard.de/dokumentation/1.0.2/grade",
            ),
        )
    )
    sorte = model.Property(
        id_short="sorte",
        value_type=model.datatypes.String,
        value="st",
        description={"de": "Angabe zur Holzsortierung"},
        semantic_id=sorte_sid,
    )
    holzart_sid = model.Reference(
        (
            model.Key(
                type_=model.KeyTypes.GLOBAL_REFERENCE,
                value="https://eldatstandard.de/dokumentation/1.0.2/species",
            ),
        )
    )
    holzart = model.Property(
        id_short="holzart",
        value_type=model.datatypes.String,
        value="fl",
        description={"de": "Angabe zur Holzart"},
        semantic_id=holzart_sid,
    )
    notizen = model.Property(
        id_short="notizen", value_type=model.datatypes.String)
    kopf_daten = model.SubmodelElementCollection(
        id_short="kopf_daten", value=[notizen]
    )
    polter1_sid = model.Reference(
        (
            model.Key(
                type_=model.KeyTypes.GLOBAL_REFERENCE,
                value="https://eldatstandard.de/dokumentation/1.0.2/allocated_wood",
            ),
        )
    )
    polter1 = model.SubmodelElementCollection(
        id_short="kopf_daten",
        description={"de": "Polterdaten"},
        semantic_id=polter1_sid
    )
    polter_liste = model.SubmodelElementList(
        id_short="polter_liste",
        type_value_list_element=model.SubmodelElementCollection,
        semantic_id_list_element=polter1_sid,
        value=[polter1]
    )
    lon = model.Property(
        id_short="lon",
        value_type=model.datatypes.Float,
        value=8.916091918945312,
        description={
            "de": "Ost-Koordinate in Dezimalgrad (z.B.: 8,916091918945312)"},
        semantic_id=model.Reference(
            (
                model.Key(
                    type_=model.KeyTypes.GLOBAL_REFERENCE,
                    value="https://eldatstandard.de/dokumentation/1.0.2/longitude",
                ),
            )
        )
    )
    lat = model.Property(
        id_short="lat",
        value_type=model.datatypes.Float,
        value=49.86861816524657,
        description={
            "de": "Nord-Koordinate in Dezimalgrad (z.B.: 49,86861816524657)"},
        semantic_id=model.Reference(
            (
                model.Key(
                    type_=model.KeyTypes.GLOBAL_REFERENCE,
                    value="https://eldatstandard.de/dokumentation/1.0.2/latitude",
                ),
            )
        )
    )
    crs = model.Property(
        id_short="crs",
        value_type=model.datatypes.String,
        value="EPSG:4326",
        description={
            "de": "Angabe des Koordinatenreferenzsystems"},
        semantic_id=model.Reference(
            (
                model.Key(
                    type_=model.KeyTypes.GLOBAL_REFERENCE,
                    value="https://eldatstandard.de/dokumentation/1.0.2/crs",
                ),
            )
        )
    )
    standort = model.SubmodelElementCollection(
        id_short="standort",
        value=[lon, lat, crs],
        semantic_id=model.Reference(
            (
                model.Key(
                    type_=model.KeyTypes.GLOBAL_REFERENCE,
                    value="https://eldatstandard.de/dokumentation/1.0.2/coordinate",
                ),
            )
        )
    )
    submodel = model.Submodel(
        id_=model.Identifier("https://www.company.com/submodels/holzliste"),
        id_short="holzliste",
        submodel_element=[vermessungsverfahren,
                          sorte, holzart, kopf_daten, polter_liste, standort],
    )
    aas = model.AssetAdministrationShell(
        id_=model.Identifier("https://www.company.com/shells/rohholz"),
        id_short="shell_rohholz",
        asset_information=asset_info,
        submodel={model.ModelReference.from_referable(submodel)},
    )
    object_store: model.DictObjectStore[model.Identifiable] = model.DictObjectStore(
    )
    object_store.add(aas)
    object_store.add(submodel)
    file_store = aasx.DictSupplementaryFileContainer()
    aas_file = io.BytesIO()
    with aasx.AASXWriter(aas_file) as writer:
        writer.write_aas(
            aas_ids=aas.id,
            object_store=object_store,
            file_store=file_store,
        )
    aas_file.seek(0)
    return aas_file


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=forestmanager_hmi_id,
        client_secret=forestmanager_hmi_secret,
    )
    return idp


# Prepare a user message and include the aasx file in its attachments
def prepare_msg() -> UserMessage:
    uMsg = UserMessage()
    subject = "AAS:Rohholz"
    text = "AAS:Rohholz"
    message_id = "s3i:" + str(uuid.uuid4())
    sender_endpoint = "s3ibs://" + forestmanager_hmi_id
    attachments = []
    attachment_dict = {}
    attachment_dict["filename"] = "holz.aasx"
    # cwd = os.path.dirname(__file__)
    # path = os.path.join(cwd, "demo_send_data", "holz.aasx")
    aas_file = create_aasx()
    base64_data = base64.b64encode(aas_file.read())
    data = base64_data.decode()
    attachment_dict["data"] = data
    attachments.append(attachment_dict)
    uMsg.fillUserMessage(
        sender=forestmanager_hmi_id,
        receivers=[forstify_hmi_id],
        subject=subject,
        text=text,
        message_id=message_id,
        reply_to_endpoint=sender_endpoint,
        attachments=attachments,
    )

    return uMsg


def main():
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)
    broker = BrokerREST(access_token)
    user_msg = prepare_msg()
    forstify_endpoint = "s3ibs://" + forstify_hmi_id
    broker.send(
        receiver_endpoints=[forstify_endpoint], msg=json.dumps(
            user_msg.base_msg)
    )
    print(f"[S3I] Message successfully sent to {forstify_hmi_id}.")


if __name__ == "__main__":
    main()
