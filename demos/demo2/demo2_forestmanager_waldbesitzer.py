from basyx.aas import model
from s3i import IdentityProvider, TokenType, Directory
import asyncio
import logging
import os, sys, inspect
import base64

import broker_api
import helpers

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
sys.path.insert(0, os.path.dirname(parentdir))

from model import enums, security, utils


# print info logs to console
logger = logging.getLogger("broker_api")
logger.setLevel(logging.INFO)

# These are our HMI's credentials
forestmanager_waldbesitzer_id = "s3i:3f23c856-d252-42c9-a590-0b87d87478d4"
forestmanager_waldbesitzer_secret = "14jXsgYN3JlE31QRniN4ruMFBoVN0Uin"

# The id of forstify's HMI and dz_wald is known beforehand
forestmanager_hmi_id = "s3i:93f22e55-ddc2-4acb-88b6-1d8f8af01ce6"
dzwald_id = "s3i:bc30c279-02c5-4918-a2ad-761e927214dd"


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=forestmanager_waldbesitzer_id,
        client_secret=forestmanager_waldbesitzer_secret,
    )
    return idp


async def main():
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)

    # obtain dzwald's endpoint by checking its entry within the S3I-Directory
    s3i_dir = Directory("https://dir.s3i.vswf.dev/api/2/", access_token)
    dzwald_dir_entry = s3i_dir.queryThingIDBased(dzwald_id)
    s3ib_endpoints = [i for i in dzwald_dir_entry["attributes"]["allEndpoints"] if
                  i.startswith('s3ib')]
    dzwald_endpoint = s3ib_endpoints[0]

    # use async client to access AAS and its submodels through their REST API asynchronously
    client = broker_api.S3IBAsyncClient(access_token, forestmanager_waldbesitzer_id, loop)    

    resource_path = "aas"
    permission_receiver = forestmanager_hmi_id

    security_sm: model.Submodel = await client.getValue(dzwald_id, dzwald_endpoint,
                                                     f"/aas/submodels/{helpers.encode_id('https://www.company.com/security')}/submodel")
    access_control: model.SubmodelElementList = security_sm.get_referable("AccessControl")

    new_rules = [security.AccessPermissionRule(permission_receiver, 
                                           security.PermissionKind.ALLOW, 
                                           security.Permission.READ),
            security.AccessPermissionRule(permission_receiver, 
                                          security.PermissionKind.ALLOW, 
                                          security.Permission.WRITE)]

    for apc in access_control:
        target: model.ReferenceElement = apc.get_referable("Target")
        target_path = helpers.id_short_path_from_ref(target.value)
        if target_path == resource_path:
            rules: model.SubmodelElementList = apc.get_referable("Rules")
            for rule in rules:
                user: model.Property = rule.get_referable("User")
                if user.value == permission_receiver:
                    rules.remove_referable(rule.id_short)
            utils.add_items_to_se_list(rules, new_rules)

    await client.sendUserMessage(forestmanager_hmi_id, "s3ibs://" + forestmanager_hmi_id, "Authorized", "Authorized")
    
    


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        loop.stop()
