import os, sys, inspect
import re
from typing import List

import basyx
from basyx.aas import model
from basyx.aas.adapter.json import json_serialization
from s3i import IdentityProvider, TokenType, Directory
import asyncio
import logging
import datetime
import base64
from urllib.parse import quote
import time

import api
import broker_api
import helpers

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
sys.path.insert(0, os.path.dirname(parentdir))

from model import enums, models, configs, security, utils

# print info logs to console
logger = logging.getLogger("broker_api")
logger.setLevel(logging.INFO)

# Thing ID and secret in the S3I-Identityprovider
dzwald_id = "s3i:bc30c279-02c5-4918-a2ad-761e927214dd"
dzwald_secret = "syhdDLoC9HSov8nuw2IK6YixTK9072wy"

forestmanager_hmi_id = "s3i:e8ef672c-109b-4c36-8999-f4ababa0bffc"
forstify_hmi_id = "s3i:8a8ee1ab-63d2-42ea-92d1-1ae682a55e7a"


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=dzwald_id,
        client_secret=dzwald_secret,
    )
    return idp


def create_sortiment():
    sortiment = models.Sortiment(
        nummer=1,
        sortimentstyp=enums.Sortimentstyp.Stammholz,
        sorte=enums.Sorte.lang,
        holzart=enums.Holzart.Fichte,
        gueteklasseVon=enums.Gueteklasse.Qualitaet_A,
        gueteklasseBis=enums.Gueteklasse.Qualitaet_B,
        laenge_von=3.0,
        laenge_bis=6.0,
        mindestzopf=1.0,
        max_durchmesser=10.0,
        laengenzugabe=10.0,
        mengenschaetzung=10.0,
        kaeufer=create_example_contact(kontaktrolle=enums.Kontaktrolle.Kaeufer),
        ergebnis=[model.ModelReference.from_referable(create_holzliste())]
    )
    return sortiment


def create_arbeitsauftrag():
    arbeitsauftrag = models.Arbeitsauftrag(
        auftrag_id=model.Identifier("https://www.company.com/submodels/arbeitsauftrag"),
        hiebsnummer="abc123",
        kurzbeschreibung="beschreibung",
        ansprechpartner=create_example_contact(
            kontaktrolle=enums.Kontaktrolle.Waldbesitzer),
        unternehmer_holzrueckung=create_example_contact(
            kontaktrolle=enums.Kontaktrolle.UnternehmerHolzrueckung),
        unternehmer_holzernte=create_example_contact(
            kontaktrolle=enums.Kontaktrolle.UnternehmerHolzernte),
        zu_faellende_baeume=models.ModelReference.from_referable(
            create_zu_faellende_baeume()),
        sortimente=[create_sortiment()]
    )
    return arbeitsauftrag


def create_umring():
    koordinate1 = models.Koordinate(
        x=8.916091918945312,
        y=49.86861816524657
    )
    koordinate2 = models.Koordinate(
        x=30.00004,
        y=120.80
    )
    koordinate3 = models.Koordinate(
        x=30.00004,
        y=120.80
    )
    umring = models.Umring(
        name="name",
        beschreibung="beschreibung",
        koordinaten=[koordinate1, koordinate2, koordinate3],
    )
    return umring


def create_standort():
    koordinate = models.Koordinate(
        x=30.00004,
        y=120.80
    )
    return models.Standort(koordinate=koordinate, notiz="Die Position")


def create_example_contact(kontaktrolle: enums.Kontaktrolle):
    example_contact = models.Kontakt(
        kontaktrolle=kontaktrolle,
        firmenname=f"{kontaktrolle.value} GmbH",
        anrede="Herr",
        vorname="Max",
        nachname="Mustermann",
        adresse="Turmstr. 15, Aachen 52064",
        telefonnummern=["0241-123456"],
        emailadressen=[f"{kontaktrolle.value}@example.com"],
        umsatzbesteuerung=enums.Umsatzbesteuerung.regelbesteuert,
        iban="DE1234567890",
        steuernummer="99999999999"
    )
    return example_contact


def create_lieferant_contact():
    lieferant = models.Kontakt(
        kontaktrolle=enums.Kontaktrolle.Lieferant,
        firmenname="Holzlieferant GmbH",
        anrede="Herr",
        vorname="Waldemar",
        nachname="Schwarz",
        adresse="Turmstr. 15, Aachen 52064",
        telefonnummern=["0241-123456", "0176-123456"],
        emailadressen=["schwarz@waldemar.de"],
        umsatzbesteuerung=enums.Umsatzbesteuerung.regelbesteuert,
        iban="DE1234567890",
        steuernummer="99999999999"
    )
    return lieferant


def create_preismatrix():
    preismatrix = [
        models.Preismatrixeintrag(preis=50.5,
                                  mittendurchmesser_von=45.0,
                                  mittendurchmesser_bis=55.0,
                                  gueteklasse_von=enums.Gueteklasse.Normale_Qualitaet,
                                  gueteklasse_bis=enums.Gueteklasse.Normale_Qualitaet
                                  ),
        models.Preismatrixeintrag(preis=50.5,
                                  mittendurchmesser_von=55.0,
                                  mittendurchmesser_bis=65.0,
                                  gueteklasse_von=enums.Gueteklasse.Qualitaet_A,
                                  gueteklasse_bis=enums.Gueteklasse.Qualitaet_A
                                  ),
    ]
    return preismatrix


def create_beobachtung():
    beobachtung = models.Beobachtung(
        id=model.Identifier("https://www.company.com/submodels/beobachtung"),
        name="name",
        beschreibung="beschreibung",
        umring=create_umring(),
        position=create_standort()
    )
    return beobachtung


def create_waldweg():
    waldweg = models.Waldweg(
        id=model.Identifier("https://www.company.com/submodels/waldweg"),
        beschreibung="beschreibung",
        breite=150.5,
        wegpunkte=[create_standort()],
        wendepunkte=[create_standort()],
    )
    return waldweg


def create_holzliste():
    kopfdaten = models.Kopfdaten(
        projekt="Eslohe2023",
        hieb=enums.Hieb.Durchforstung,
        ernte=(datetime.date(2023, 12, 1), datetime.date(2023, 12, 31)),
        lieferant=create_lieferant_contact(),
        kaeufer=create_example_contact(kontaktrolle=enums.Kontaktrolle.Kaeufer),
        fuhrmann=create_example_contact(kontaktrolle=enums.Kontaktrolle.Fuhrmann),
        notiz="Dieses Holz ist sehr gut"
    )

    messergebnis_einzelstammliste = models.EinzelstammListe(einzelstaeme=[
        models.Einzelstamm(
            menge=3.4,
            stammlaenge=4.2,
            stammnummer="1",
            mittendurchmesser=47.0,
            gueteklasse=enums.Gueteklasse.Normale_Qualitaet
        ),
        models.Einzelstamm(
            menge=4.7,
            stammlaenge=4.3,
            stammnummer="2",
            mittendurchmesser=63.0,
            gueteklasse=enums.Gueteklasse.Qualitaet_A
        ),
        models.Einzelstamm(
            menge=2.7,
            stammlaenge=2.3,
            stammnummer="3",
            mittendurchmesser=63.0,
            gueteklasse=enums.Gueteklasse.Qualitaet_A,
            klammerstammabschnittsnummer="1"
        ),
        models.Einzelstamm(
            menge=1.7,
            stammlaenge=1.3,
            stammnummer="3",
            mittendurchmesser=43.0,
            gueteklasse=enums.Gueteklasse.Qualitaet_B,
            klammerstammabschnittsnummer="2"
        ),
    ])

    polter = models.Polter(
        polternummer="100",
        vermessungsverfahren=enums.Vermessungsverfahren.Mittenstaerkenvermessung,
        polterstatus=enums.Polterstatus.Vermessen,
        messergebnis_einzelstammliste=messergebnis_einzelstammliste,
        standort=create_standort(),
        fotos=[
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Altenbrak_Holz.JPG/1920px-Altenbrak_Holz.JPG",
            "https://de.wikipedia.org/wiki/Polter#/media/Datei:Baumst%C3%A4mme_bei_St%C3%BCbeckshorn.jpg"],
        videos=["https://www.youtube.com/watch?v=b65SSRfDwUo"],
    )

    holzliste = models.Holzliste(
        id="https://www.company.com/holzliste/1",
        kopfdaten=kopfdaten,
        polterliste=[polter],
        sortimentstyp=enums.Sortimentstyp.Stammholz,
        sorte=enums.Sorte.lang,
        holzart=enums.Holzart.Fichte,
        preismatrix=create_preismatrix())

    return holzliste


def create_zu_faellende_baeume():
    umring = create_umring()

    koordinate_baum1 = models.Koordinate(
        x=8.916091918945312,
        y=49.86861816524657
    )
    baum1 = models.StehenderBaum(
        hoehe=20.3,
        bhd=30.2,
        baumart=enums.Holzart.Fichte,
        position=models.Standort(koordinate=koordinate_baum1,
                                 notiz="Die Position Fichte")
    )

    koordinate_baum2 = models.Koordinate(
        x=30.00004,
        y=120.80
    )
    baum2 = models.StehenderBaum(
        hoehe=17.0,
        bhd=31.2,
        baumart=enums.Holzart.Eiche,
        position=models.Standort(koordinate=koordinate_baum2, notiz="Die Position Eiche")
    )

    koordinate_baum3 = models.Koordinate(
        x=30.00004,
        y=120.80
    )
    baum3 = models.StehenderBaum(
        hoehe=15.3,
        bhd=20.2,
        baumart=enums.Holzart.Buche,
        position=models.Standort(koordinate=koordinate_baum3, notiz="Die Position Buche")
    )


    zufaellendebaeume = models.ZuFaellendeBaeume(
        id="https://www.company.com/submodels/zu_faellende_baeume",
        beschreibung="Baueme sollen demnaechst gefaellt werden",
        umring=umring,
        position=create_standort(),
        baumliste=(baum1, baum2, baum3),
    )

    return zufaellendebaeume


def create_holzpreisbereiche():
    # TODO: Warum braucht man Holzpreisbereich, wenn es schon Preismatrizen gibt?
    holzpreisbereich = models.Holzpreisbereich(
        baumart=enums.Holzart.Fichte,
        preis_von=50.0,
        preis_bis=80.0,
        sorte=enums.Sorte.lang,
        gueteklasse=enums.Gueteklasse.Qualitaet_A,
        mittendurchmesser_von=40.3,
        mittendurchmesser_bis=60.4,
        holzliste=model.ModelReference.from_referable(create_holzliste()),
        datum=datetime.date.today(),
        quelle="Dies ist eine Quelle"
    )
    holzpreisbereiche = models.Holzpreisbereiche(
        id="https://www.company.com/submodels/holzpreisbereiche",
        holzpreisbereiche=[holzpreisbereich]
    )
    return holzpreisbereiche


def create_verkaufslos(holzlisten: List[model.Submodel]):
    holzlisten_refs = [model.ModelReference.from_referable(i) for i in holzlisten]

    # TODO: Warum braucht man Preismatrix sowohl in Verkaufslos als auch in Holzliste?

    verkaufslos = models.Verkaufslos(
        id=model.Identifier("https://www.company.com/submodels/verkaufslos"),
        verkaufsstatus=enums.Verkaufsstatus.Offen,
        abrechnungsmass=enums.Abrechnungsmass.Mittenstaerkenvermessung,
        bereitstellungsart=enums.Bereitstellungsart.Frachtfrei_bis_incoterm,
        verfuegbar_ab=datetime.date(2023, 1, 1),
        verfuegbar_bis=datetime.date(2023, 12, 31),
        verkauefer=create_lieferant_contact(),
        notiz="Dies ist ein Verkaufslos",
        holzlisten=holzlisten_refs,
        preismatrix=create_preismatrix(),
    )
    return verkaufslos


def create_bestandesdaten():
    # TODO: Haben wir nur eine Instanz von Bestandesdaten?
    #  Was passiert, wenn Wald aus mehreren nich zusammenhängenden Gebieten besteht?

    bestandesdaten = models.Bestandesdaten(
        id=model.Identifier("https://www.company.com/submodels/bestandesdaten"),
        umring=create_umring(),
        waldbesitzer=create_example_contact(kontaktrolle=enums.Kontaktrolle.Waldbesitzer),
        baumarten=[enums.Holzart.Fichte]
    )

    return bestandesdaten


def create_aas():
    arbeitsauftrag = create_arbeitsauftrag()
    beobachtung = create_beobachtung()
    bestandesdaten = create_bestandesdaten()
    holzliste = create_holzliste()
    holzpreisbereiche = create_holzpreisbereiche()
    verkaufslos = create_verkaufslos([holzliste])
    waldweg = create_waldweg()
    zu_faellende_baeume = create_zu_faellende_baeume()

    events = model.Submodel(
        id_="https://www.company.com/submodels/events",
        id_short="Events"
    )

    event = model.BasicEventElement(
        id_short="Auftragsstatus_Updated",
        observed=model.ModelReference.from_referable(arbeitsauftrag.get_referable(configs.AUFTRAGSSTATUS)),
        direction=model.Direction.OUTPUT,
        state=model.StateOfEvent.ON,
        message_topic="topic123"
    )

    events.add_referable(event)

    security_sm = security.Security("https://www.company.com/security")

    submodels = [
        arbeitsauftrag,
        beobachtung,
        bestandesdaten,
        holzliste,
        holzpreisbereiche,
        verkaufslos,
        waldweg,
        zu_faellende_baeume,
        events,
        security_sm
    ]

    """

    def remove_iteration_ending(val: str):
        # remove iteration ending like {00}
        val = re.sub(r'\{\d+\}$', '', val)
        # remove one or more digits at the end
        val = re.sub(r'_?\d+$', '', val)
        return val

    def set_semantic_id(referable: model.SubmodelElement):
        # example = "https://admin-shell.io/kwh40/forestml40/Waldweg/0/1/Beschreibung"

        if isinstance(referable, model.Submodel):
            base = "https://admin-shell.io/kwh40/forestml40"
            postfix = f"{referable.id_short}/0/1"
        else:
            base = referable.parent.semantic_id.key[0].value
            postfix = remove_iteration_ending(referable.id_short)

        referable.semantic_id = model.GlobalReference(
            (basyx.aas.model.Key(model.KeyTypes.GLOBAL_REFERENCE,
                                 f"{base}/{postfix}"),)
        )

    def set_semantic_id_for_each_in(obj):
        set_semantic_id(obj)
        try:
            iter(obj)
        except TypeError as e:
            print(e)
            return
        se_objects = list(obj)
        for se in se_objects:
            set_semantic_id_for_each_in(se)


    for submodel in submodels:
        set_semantic_id_for_each_in(submodel)

    """

    aas_dz_wald = models.DZWald(
        dzwald_id="https://www.company.com/dz_wald/1",
        asset=basyx.aas.model.AssetInformation(),
        holzlisten=[basyx.aas.model.ModelReference.from_referable(holzliste)],
        arbeitsauftraege=[basyx.aas.model.ModelReference.from_referable(arbeitsauftrag)],
        bestandesdaten=basyx.aas.model.ModelReference.from_referable(bestandesdaten),
        verkaufslose=[basyx.aas.model.ModelReference.from_referable(verkaufslos)],
        beobachtungen=[basyx.aas.model.ModelReference.from_referable(beobachtung)],
        waldwege=[basyx.aas.model.ModelReference.from_referable(waldweg)],
        zu_faellende_baeume=basyx.aas.model.ModelReference.from_referable(
            zu_faellende_baeume),
        holzpreisbereiche=basyx.aas.model.ModelReference.from_referable(
            holzpreisbereiche),
    )

    rules = [security.AccessPermissionRule(forestmanager_hmi_id,
                                           security.PermissionKind.ALLOW,
                                           security.Permission.READ),
            security.AccessPermissionRule(forestmanager_hmi_id,
                                          security.PermissionKind.ALLOW,
                                          security.Permission.WRITE)]

    rules_smc = security.AccessPermissionCollection(
        target=model.ModelReference.from_referable(aas_dz_wald),
        rules=rules
    )

    # print(rules_smc.get_referable("Target").value)

    access_control = security.AccessControl(permissions=[rules_smc])
    security_sm.add_referable(access_control)
    # print(security.get_dic_from_security_submodel(security_sm))

    getHolzpreisbereich_op = model.Operation(
        id_short="getHolzpreisbereich",
        input_variable=[model.OperationVariable(model.Property(
            id_short="HolzlisteId",
            value_type=model.datatypes.String
        ))]
    )
    holzliste.add_referable(getHolzpreisbereich_op)

    return aas_dz_wald, submodels


def write_aas():
    aas, submodels = create_aas()

    objstore = basyx.aas.model.DictObjectStore(submodels)
    objstore.add(aas)

    with open("dz_wald_example.json", "w", encoding='utf-8') as file:
        json_serialization.write_aas_json_file(file, objstore)

# Function corrosponding to the "getHolzpreisbereich" operation.
# Note that the function's parameters are named after the operation's
# input variables.
def getHolzpreisbereich(provider: api.ModelProvider, HolzlisteId: str):
    holzliste_sm = provider.getValue(f"/aas/submodels/{helpers.encode_id(HolzlisteId)}/submodel")
    # TODO: REST
    # CALL to Forstify
    # create a placeholder reply for now
    reply = {
        "Baumart": "kie",
        "PreisVon": 14500.0,
        "PreisBis": 14500.0,
        "Sorte": "ab",
        "MittendurchmesserVon": 100.0,
        "MittendurchmesserBis": 200.0,
        "Datum": "8/4/2023, 10:56:39 AM",
        "Quelle": "Forstify Marktplatz Angebote"
    }
    date_format = '%m/%d/%Y, %H:%M:%S %p'
    holzpreisbereich = models.Holzpreisbereich(
        baumart=enums.Holzart(reply["Baumart"]),
        preis_von=reply["PreisVon"],
        preis_bis=reply["PreisBis"],
        sorte=enums.Sorte(reply["Sorte"]),
        mittendurchmesser_bis=reply["MittendurchmesserBis"],
        mittendurchmesser_von=reply["MittendurchmesserVon"],
        datum=datetime.datetime.strptime(reply["Datum"], date_format),
        quelle=reply["Quelle"],
        holzliste=model.ModelReference.from_referable(holzliste_sm)
    )
    holzpreisbereiche_id = "https://www.company.com/submodels/holzpreisbereiche"
    holzpreisbereiche: model.SubmodelElementList =  provider.getValue(
        f"/aas/submodels/{helpers.encode_id(holzpreisbereiche_id)}/submodel"
        )
    utils.add_items_to_se_list(holzpreisbereiche, [holzpreisbereich])
    return reply



def main():
    # obtain access token
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)

    # update policy for other things to read the S3I-Directory entry of your thing
    # view helpers.py for a better insight
    # function call is commented because it needs to be done a single time
    #s3i_dir = Directory("https://dir.s3i.vswf.dev/api/2/", access_token)
    #helpers.grant_entry_read_permissions(s3i_dir, dzwald_id, [forestmanager_hmi_id, forstify_hmi_id])

    # create AAS
    aas, submodels = create_aas()

    # update S3I-Directory entry to include a short description of your AAS
    # helpers.add_aas_to_dir_entry(aas, s3i_dir, dzwald_id)

    # wrap them into a ModelProvider to expose an Interface of type 2
    provider = api.ModelProvider(aas, submodels)

    # now the REST API definded in DAAS-2 can be used
    # the following example will print all submodel references to the console
    # corrosponding to GetAllSubmodelReferences:
    print(provider.getValue("/aas/submodels"))

    # the following will retrieve the submodel identified by
    # https://www.company.com/holzliste/1
    submodel_id = "https://www.company.com/holzliste/1"
    submodel_id_encoded = base64.urlsafe_b64encode(submodel_id.encode()).decode()
    id_short_path_encoded = quote("Preismatrix[0].Preis")
    preis: model.Property = provider.getValue(
        f"/aas/submodels/{submodel_id_encoded}/submodel/submodelElements/{id_short_path_encoded}"
    )
    print(preis)

    # now create a server instance that will translate incoming S3I-B messages
    # to methods exposed by the ModelProvider

    HolzlisteId = 'https://www.company.com/holzliste/1'
    loop = asyncio.get_event_loop()

    # set last flag to True to enable security
    server = broker_api.S3IBServer(access_token, provider, dzwald_id, loop, "https://www.company.com/security", False)

    # add callable associated with operation 'getHolzpreisbereich' to server
    callable = lambda **kwargs : getHolzpreisbereich(provider, **kwargs)
    operation_path = f"/aas/submodels/{helpers.encode_id(HolzlisteId)}/submodel/submodelElements/getHolzpreisbereich"
    server.add_callable(operation_path, callable)

    try:
        server.run()
    except KeyboardInterrupt:
        loop.stop()




if __name__ == "__main__":
    main()
    #create_aas()
