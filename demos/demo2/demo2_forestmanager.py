from basyx.aas import model
from s3i import IdentityProvider, TokenType, Directory
import asyncio
import logging
import os, sys, inspect
import base64

import broker_api
import helpers

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
sys.path.insert(0, os.path.dirname(parentdir))

from demos.demo2.demo2_dzwald import create_beobachtung, create_waldweg, \
    create_arbeitsauftrag
from model import enums


# print info logs to console
logger = logging.getLogger("broker_api")
logger.setLevel(logging.INFO)

# These are our HMI's credentials
forestmanager_hmi_id = "s3i:93f22e55-ddc2-4acb-88b6-1d8f8af01ce6"
forestmanager_hmi_secret = "a9lUA7rfZpttwMNFSyy4qatSI6csH5xh"

# The id of forstify's HMI and dz_wald is known beforehand
forstify_hmi_id = "s3i:8a8ee1ab-63d2-42ea-92d1-1ae682a55e7a"
dzwald_id = "s3i:bc30c279-02c5-4918-a2ad-761e927214dd"


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=forestmanager_hmi_id,
        client_secret=forestmanager_hmi_secret,
    )
    return idp


async def main():
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)

    # obtain dzwald's endpoint by checking its entry within the S3I-Directory
    s3i_dir = Directory("https://dir.s3i.vswf.dev/api/2/", access_token)
    dzwald_dir_entry = s3i_dir.queryThingIDBased(dzwald_id)
    s3ib_endpoints = [i for i in dzwald_dir_entry["attributes"]["allEndpoints"] if
                  i.startswith('s3ib')]
    dzwald_endpoint = s3ib_endpoints[0]

    # use async client to access AAS and its submodels through their REST API asynchronously
    client = broker_api.S3IBAsyncClient(access_token, forestmanager_hmi_id, loop)    

    # await access grant from waldbesitzer
    await client.awaitMessage({"text": "Authorized"})

    # Speichern einer Beobachtung
    beobachtung_submodel = create_beobachtung()
    beobachtung_id_encoded = base64.urlsafe_b64encode(beobachtung_submodel.id.encode()).decode()
    task0 = client.setValue(dzwald_id, dzwald_endpoint,
                            f"/aas/submodels/{beobachtung_id_encoded}/submodel", beobachtung_submodel)
    
    # listen on event "Auftragsstatus_Updated"
    events_submodel_id = "https://www.company.com/submodels/events"
    events_submodel_id_encoded = base64.urlsafe_b64encode(events_submodel_id.encode()).decode()
    event: model.BasicEventElement = await client.getValue(dzwald_id, dzwald_endpoint,
                                                           f"/aas/submodels/{events_submodel_id_encoded} \
                                                           /submodel/submodelElements/Auftragsstatus_Updated")
    auftragsstatus_updated = await client.awaitEvent(event.message_topic)

    # Speichern eines Waldweges
    waldweg_submodel = create_waldweg()
    waldweg_id_encoded = base64.urlsafe_b64encode(waldweg_submodel.id.encode()).decode()
    task1 = client.setValue(dzwald_id, dzwald_endpoint,
                            f"/aas/submodels/{waldweg_id_encoded}/submodel", waldweg_submodel)

    # values of the ok-element in the S3I-B messages are returned
    (reply0, reply1) = await asyncio.gather(task0, task1)
    print(f"Response to add beobachtung: {reply0}")
    print(f"Response to add waldweg: {reply1}")

    # Holzeinschlag und Holzaufnahme

    # Speichern des Arbeitsauftrags
    arbeitsauftrag_submodel = create_arbeitsauftrag()
    arbeitsauftrag_id = "https://www.company.com/submodels/arbeitsauftrag"
    arbeitsauftrag_id_encoded = base64.urlsafe_b64encode(arbeitsauftrag_id.encode()).decode()

    reply2 = await client.setValue(dzwald_id, dzwald_endpoint,
                                   f"/aas/submodels/{arbeitsauftrag_id_encoded}/submodel",
                                   arbeitsauftrag_submodel)
    print(f"Response to add arbeitsauftrag: {reply2}")

    # event = client.subscribeToEvent("update_arbeitsauftrag")

    # TODO: SOLL-IST comparison

    # Update Arbeitsauftrag (Status)
    reply3 = await client.setValue(
        dzwald_id,
        dzwald_endpoint,
        f"/aas/submodels/{arbeitsauftrag_id_encoded}/submodel/submodelElements/Auftragsstatus",
        f"{enums.Auftragsstatus.Gesendet.value}"
    )
    print(f"Response to update auftragsstatus: {reply3}")

    # Check if Arbeitsauftrag was updated successfully:
    auftragsstatus: model.Property = await client.getValue(
        dzwald_id,
        dzwald_endpoint,
        f"/aas/submodels/{arbeitsauftrag_id_encoded}/submodel/submodelElements/Auftragsstatus"
    )
    print(auftragsstatus)
    print(
        f"{auftragsstatus.id_short}: {enums.Auftragsstatus(int(auftragsstatus.value))}")
    
    # Invoke Operation 'getHolzpreisbereich'
    HolzlisteId = "https://www.company.com/holzliste/1"
    reply4 = await client.invokeOperation(
        dzwald_id,
        dzwald_endpoint,
        f"/aas/submodels/{helpers.encode_id(HolzlisteId)}/submodel/submodelElements/getHolzpreisbereich",
        {"HolzlisteId": HolzlisteId}
    )
    print(f"result of operation 'getHolzpreisbereich': {reply4}")

    # await event
    await asyncio.wait([auftragsstatus_updated])
    


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        loop.stop()
