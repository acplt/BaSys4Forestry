from typing import Set, Callable
from basyx.aas import model
import base64

from urllib.parse import unquote


AAS = "aas"
SUBMODELS = "submodels"
SUBMODEL = "submodel"
SUBMODELELEMENTS = "submodelElements"


class AASAPI:
    """ This class exposes the Typ 2 technology-independent Interface as defined in DAAS-Part2 to the AssetAdministrationShell.  
    """

    def __init__(self, shell: model.AssetAdministrationShell):
        self.shell = shell

    def get_aas(self):
        return self.shell

    def get_all_submodel_references(self):
        return list(self.shell.submodel)

    def post_submodel_reference(self, submodel_ref: model.ModelReference):
        self.shell.submodel.add(submodel_ref)

    def delete_submodel_reference(self, submodel_id: str):
        submodels_dict = {x.get_identifier(): x for x in self.shell.submodel}
        self.shell.submodel.remove(submodels_dict.get(submodel_id))


class SubmodelAPI:
    """ This class exposes the Typ 2 technology-independent Interface as defined in DAAS-Part2 to the Submodel.  
    """

    def __init__(self, submodel: model.Submodel):
        self.submodel = submodel

    def get_submodel(self):
        return self.submodel

    def put_submodel(self, submodel: model.Submodel):
        return self.submodel.update_from(submodel)

    def get_all_submodel_elements(self):
        return list(self.submodel.submodel_element)

    def get_submodel_element_by_path(self, idshort_path: str):
        submodel_element = self._get_se_by_path(idshort_path)
        return submodel_element

    def set_submodel_element_value_by_path(self, idshort_path: str, value):
        submodel_element = self._get_se_by_path(idshort_path)
        submodel_element.value = value

    def delete_submodel_element_by_path(self, idshort_path: str):
        path_elements = idshort_path.split(".")
        submodel_element = self.submodel.get_referable(path_elements.pop(0))
        last_idshort = path_elements.pop(-1)
        for idshort in path_elements:
            submodel_element = submodel_element.get_referable(idshort)
        if isinstance(submodel_element, model.UniqueIdShortNamespace):
            submodel_element.remove_referable(last_idshort)

    def post_submodel_element(self, submodel_element: model.SubmodelElement):
        self.submodel.submodel_element.add(submodel_element)

    def post_submodel_element_by_path(self, idshort_path: str, submodel_element: model.SubmodelElement):
        se = self._get_se_by_path(idshort_path)
        if isinstance(se, (model.SubmodelElementCollection, model.SubmodelElementList)):
            se.value.add(submodel_element)
    
    def _get_se_by_path(self, idshort_path: str):
        idshort_path = unquote(idshort_path)
        path_elements = idshort_path.split(".")
        if "[" in path_elements[0]:
            submodel_element = self.submodel
        else:
            submodel_element = self.submodel.get_referable(path_elements.pop(0))
        for i, idshort in enumerate(path_elements):
            if "[" in idshort:
                sub_elements = idshort.replace("[", "]").split("]")
                se_list: model.SubmodelElementList = submodel_element.get_referable(sub_elements[0])
                submodel_element = se_list.value[int(sub_elements[1])]
            else:
                submodel_element = submodel_element.get_referable(idshort)
        return submodel_element


class ModelProvider:
    """
    """

    def __init__(self, aas: model.AssetAdministrationShell, submodels: Set[model.Submodel]):
        self.aas_api = AASAPI(aas)
        self.submodel_api = {x.id: SubmodelAPI(x) for x in submodels}
        self._id_to_type = {base64.urlsafe_b64encode(x.id.encode()).decode(): x.__class__.__name__ for x in submodels}
        

    def process_path(self, path: str):
        path_elems = path.split("/")
        path_elems.pop(0)
        if path.endswith("/"):
            path_elems.pop(-1)
        return path_elems

    def getValue(self, path: str):
        path_elems = self.process_path(path)
        if path_elems and path_elems[0] == AAS:
            if len(path_elems) == 1:
                return self.aas_api.get_aas()
            if path_elems[1] == SUBMODELS:
                if len(path_elems) == 2:
                    return self.aas_api.get_all_submodel_references()
                submodel_id = base64.urlsafe_b64decode(path_elems[2]).decode()
                if path_elems[3] == SUBMODEL:
                    if len(path_elems) == 4:
                        return self.submodel_api[submodel_id].get_submodel()
                    if path_elems[4] == SUBMODELELEMENTS:
                        if len(path_elems) == 5:
                            return self.submodel_api[submodel_id].get_all_submodel_elements()
                        else:
                            return self.submodel_api[submodel_id].get_submodel_element_by_path(path_elems[5])

    def createValue(self, path: str, value):
        path_elems = self.process_path(path)
        if path_elems and path_elems[0] == AAS:
            if path_elems[1] == SUBMODELS:
                if len(path_elems) == 2:
                    return self.aas_api.post_submodel_reference(value)
                submodel_id = base64.urlsafe_b64decode(path_elems[2]).decode()
                if path_elems[3] == SUBMODEL:
                    if path_elems[4] == SUBMODELELEMENTS:
                        if len(path_elems) == 5:
                            return self.submodel_api[submodel_id].post_submodel_element(value)
                        else:
                            return self.submodel_api[submodel_id].post_submodel_element_by_path(path_elems[5], value)

    def deleteValue(self, path: str):
        path_elems = self.process_path(path)
        if path_elems and path_elems[0] == AAS:
            if path_elems[1] == SUBMODELS:
                submodel_id = base64.urlsafe_b64decode(path_elems[2]).decode()
                if len(path_elems) == 3:
                    return self.aas_api.delete_submodel_reference(submodel_id)
                if path_elems[3] == SUBMODEL:
                    if path_elems[4] == SUBMODELELEMENTS:
                        return self.submodel_api[submodel_id].delete_submodel_element_by_path(path_elems[5])

    def setValue(self, path: str, value):
        path_elems = self.process_path(path)
        if path_elems and path_elems[0] == AAS:
            if path_elems[1] == SUBMODELS:
                submodel_id = base64.urlsafe_b64decode(path_elems[2]).decode()
                if path_elems[3] == SUBMODEL:
                    if len(path_elems) == 4:
                        # create submodel if it does not exist, may be different from standard
                        if not self.submodel_api.get(submodel_id):
                            self.submodel_api[submodel_id] = SubmodelAPI(value)
                            self.aas_api.post_submodel_reference(model.ModelReference.from_referable(value))
                        else:
                            self.submodel_api[submodel_id].put_submodel(value)
                        return
                    if path_elems[4] == SUBMODELELEMENTS:
                        return self.submodel_api[submodel_id].set_submodel_element_value_by_path(path_elems[5], value)
    
