import json
import logging
import asyncio
from uuid import uuid4

from s3i import IdentityProvider, broker_message, Directory, BrokerAMQP, S3IBMessageError, TokenType

logger = logging.getLogger(__name__)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

# The id of DZWald is known beforehand
dzwald_id = "s3i:6b1c90d0-0234-4ce7-9094-06ec2259dd5c"

# These are our HMI's credentials
forstify_hmi_id = "s3i:8a8ee1ab-63d2-42ea-92d1-1ae682a55e7a"
forstify_hmi_secret = "U9AkkeEXbOZNNFUYQvH5hb3WhbBGaw9a"
forstify_endpoint = "s3ibs://" + forstify_hmi_id

def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=forstify_hmi_id,
        client_secret=forstify_hmi_secret,
    )
    return idp


def get_endpoint(access_token):
    """
    Retrieve dzwald's endpoint(s) from its entry within S3I-Directory. Choose S3I-B based endpoint. 
    """
    # obtain dzwald's endpoint by checking its entry within the S3I-Directory
    s3i_dir = Directory("https://dir.s3i.vswf.dev/api/2/", access_token)
    dzwald_dir_entry = s3i_dir.queryThingIDBased(dzwald_id)
    s3ib_endpoints = [i for i in dzwald_dir_entry["attributes"]["allEndpoints"] if i.startswith('s3ib')]
    dzwald_endpoint = s3ib_endpoints[0]
    return dzwald_endpoint


def prepare_msg(receiver_endpoint):
    """
    To retrieve the holzliste submodel, we will issue an S3I-B GetValueRequest 
    with attribute path set to /aas/submodels/{submodel_id}/submodel (see DAAS Part 2 fmi)
    """
    msg = broker_message.GetValueRequest()
    receiver_endpoint = receiver_endpoint
    my_endpoint = forstify_endpoint
    msg_id = "s3i:" + str(uuid4())
    msg.fillGetValueRequest(
        sender=forstify_hmi_id, 
        receivers=[dzwald_id], 
        message_id=msg_id, 
        attribute_path="/aas/submodels/Holzliste/submodel",
        reply_to_endpoint=my_endpoint
    )
    return msg


def callback(ch, method, properties, body):
        try:
            msg = broker_message.Message(base_msg=body)
            msg_id = msg.base_msg["replyingToMessage"]
            sender = msg.base_msg["sender"]
            message_type = msg.base_msg["messageType"]
            value = msg.base_msg.get("value")

            logger.info(f"[S3I] Received {message_type} from {sender}")

            # Print retrieved value to console
            print(value)

        except S3IBMessageError as e:
            raise Exception(body)
        

def send_message():
    broker.send([dzwald_endpoint], json.dumps(msg.base_msg))


if __name__ == "__main__":
    
    # Retrieve access token
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)

    # Get DZWald's endpoint, to which the GetValueRequest will be sent
    dzwald_endpoint = get_endpoint(access_token) 

    # Create message
    msg = prepare_msg(dzwald_endpoint)
    
    # Send message through S3I-Broker
    loop = asyncio.get_event_loop()
    broker = BrokerAMQP(access_token, forstify_endpoint, callback, loop)
    # The send_message function will be called as soon as the channel is open
    broker.add_on_channel_open_callback(send_message, True)
    broker.connect()  

    # Run asyncio loop 
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info("Disconnect from S3I")
        loop.close()

