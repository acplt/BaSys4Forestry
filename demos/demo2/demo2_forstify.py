from basyx.aas import model
from s3i import IdentityProvider, TokenType, Directory
import asyncio
import logging
import os, sys, inspect

import broker_api

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
sys.path.insert(0, os.path.dirname(parentdir))

from model import enums, models

# print info logs to console
logger = logging.getLogger("broker_api")
logger.setLevel(logging.INFO)

# These are our HMI's credentials
forstify_hmi_id = "s3i:8a8ee1ab-63d2-42ea-92d1-1ae682a55e7a"
forstify_hmi_secret = "U9AkkeEXbOZNNFUYQvH5hb3WhbBGaw9a"

# The id of forsestmanager's HMI and dz_wald is known beforehand
forestmanager_hmi_id = "s3i:e8ef672c-109b-4c36-8999-f4ababa0bffc"
dzwald_id = "s3i:6b1c90d0-0234-4ce7-9094-06ec2259dd5c"


def authenticate():
    idp = IdentityProvider(
        grant_type="client_credentials",
        client_id=forstify_hmi_id,
        client_secret=forstify_hmi_secret,
    )
    return idp


async def main():
    idp = authenticate()
    access_token = idp.get_token(TokenType.ACCESS_TOKEN)

    # obtain dzwald's endpoint by checking its entry within the S3I-Directory
    s3i_dir = Directory("https://dir.s3i.vswf.dev/api/2/", access_token)
    dzwald_dir_entry = s3i_dir.queryThingIDBased(dzwald_id)
    s3ib_endpoints = [i for i in dzwald_dir_entry["attributes"]["allEndpoints"] if i.startswith('s3ib')]
    dzwald_endpoint = s3ib_endpoints[0]

    # use async client to access AAS and its submodels through their REST API asynchronously
    client = broker_api.S3IBAsyncClient(access_token, forestmanager_hmi_id, loop)

    # Holzangebot via Holzhandelsplattform erstellen und einstellen 

    # Get Holzlisten-Daten
    holzliste: model.Submodel = await client.getValue(dzwald_id, dzwald_endpoint, "/aas/submodels/Holzliste/submodel")
    print(f"{holzliste.id_short} contains the following submodel-elements:")
    print(f"    {[*holzliste.submodel_element._backend.values()]}")
    # print(f"    {[*holzliste.submodel_element._backend]}")

    # TODO: Verkaufslose bilden, von Forestmanager bekommen

    # Update Verkaufsstaus
    reply0 = await client.setValue(
        dzwald_id, dzwald_endpoint,
        "/aas/submodels/Verkaufslos/submodel/submodelElements/Verkaufsstatus",
        enums.Verkaufsstatus.Verkauft.value
    )
    print(f"Response to update verkaufsstatus: {reply0}")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        loop.stop()
