package policy

import future.keywords.in
import input

default allow = false

allow {
    path_arr := split(input.path, "/")
    print(path_arr)
    path_id_short := split(path_arr[count(path_arr)-1], ".")
    z := array.slice(path_arr, 0, count(path_arr)-1)
    path_full := array.concat(z, path_id_short)
    print(path_id_short)
    print(path_full)
    some i, _ in path_full
    path_arr_slice := array.slice(path_full, 0, i+1)
    path := concat("/", path_arr_slice)
    print(path)
    print(data.api[path])
    some user in data.api[path][input.method]
    user == input.user
}
