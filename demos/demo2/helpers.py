import base64
from urllib.parse import unquote

from basyx.aas import model
from s3i import Directory

from api import SUBMODELS, SUBMODELELEMENTS, SUBMODEL, AAS


def add_aas_to_dir_entry(aas: model.AssetAdministrationShell, dir: Directory, thing_id: str):
    # get thing entry from directory
    entry = dir.queryThingIDBased(thing_id)
    # add short description of the AAS using the "thingStructure" attribute
    if "thingStructure" not in entry["attributes"]:
        entry["attributes"]["thingStructure"] = {}
    entry["attributes"]["thingStructure"].update({
        "class": "aas.model.AssetAdministrationShell",
        "identifier": aas.id,
        "links": []
    })
    for ref in aas.submodel:
        entry["attributes"]["thingStructure"]["links"].append({
            "association": "aas.model.Reference",
            "target": {
                "class": "aas.model.Submodel",
                "identifier": ref.get_identifier()
            }
        })
    # update entry
    dir.updateThingIDBased(thing_id, entry)
    return


def add_submodel_to_dir_entry(submodel: model.Submodel, dir: Directory, thing_id: str):
    entry = dir.queryThingIDBased(thing_id)
    entry["attributes"]["thingStructure"]["links"].append({
        "association": "aas.model.Reference",
        "target": {
            "class": "aas.model.Submodel",
            "identifier": submodel.id
        }
    })
    dir.updateThingIDBased(thing_id, entry)


def grant_entry_read_permissions(dir: Directory, thing_id, receivers: list):
    policy = dir.queryPolicyIDBased(thing_id)
    for receiver in receivers:
        policy['entries']['observer']['subjects'][f"nginx:{receiver}"] = {'type': 'nginx basic auth client'}
    dir.updatePolicyIDBased(thing_id, policy)
    return


def id_short_path_from_ref(ref: model.ModelReference):
    arr = ["aas"]
    for i, key in enumerate(ref.key):
        if key.value.isnumeric():
            arr[i-1] = f"{arr[i-1]}[{key.value}]"
        elif key.type == model.KeyTypes.SUBMODEL:
            arr.append(base64.urlsafe_b64encode(key.value.encode()).decode())
        elif key.type == model.KeyTypes.ASSET_ADMINISTRATION_SHELL:
            continue
        else:
            arr.append(key.value)
    return ".".join(arr)


def id_short_path_from_path(path: str):
    arr = path.split("/")
    submodel_id_index = arr.index(SUBMODEL) - 1 if SUBMODEL in arr else None 
    id_short_path_index = arr.index(SUBMODELELEMENTS) + 1 if SUBMODELELEMENTS in arr else None
    if id_short_path_index:
        return f"{AAS}.{arr[submodel_id_index]}.{unquote(arr[id_short_path_index])}"
    elif submodel_id_index:
        return f"{AAS}.{arr[submodel_id_index]}"
    else:
        return AAS

def encode_id(id: str):
    return base64.urlsafe_b64encode(id.encode()).decode()




             
                  
            
