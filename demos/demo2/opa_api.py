import requests
import json

class Opa:

    def __init__(self, server_url="http://localhost:8181"):
        self.server_url = server_url
    
    def query(self, user, method, path):
        body = {"input": {"user": user, "method": method, "path": path}}
        url = f"{self.server_url}/v1/data/policy/allow"
        response = requests.post(url=url, data=json.dumps(body))
        result = json.loads(response.text)["result"]
        return result
    
    def get_data(self, path=""):
        url = f"{self.server_url}/v1/data/{path}"
        response = requests.get(url=url)
        result = json.loads(response.text)["result"]
        return result

    def update_data(self, data, path=""):
        url = f"{self.server_url}/v1/data/{path}"
        response = requests.put(url=url, data=json.dumps(data))
        return response.status_code
        #result = json.loads(response.text)["result"]
        #return result
    
