# BaSys4Forestry

## Navigation
- [Allgemeine Infos](#allgemeine-infos)
- [Kommunikation](#kommunikation)
    - [Kontaktdaten Projektpartner](#kontaktdaten-projektpartner)
    - [Mailingsliste](#mailingsliste)
    - [Microsoft Teams](#microsoft-teams)
    - [Regel-Telkos](#regel-telkos)
    - [SharePoint](#sharepoint)
- [GitLab](#gitlab)
    - [Issues](#issues)
    - [Boards](#boards)
- [Arbeitspakete](#arbeitspakete)
    - [Übersicht Balkenplan](#übersicht-bakenplan)
    - [AP 0: Projektmanagement und Dissemination](#ap-0:-projektmanagement-und-dissemination)
    - [AP 1: Definition der Anwendungsszenarien](#ap-1:-definition-der-anwendungsszenarien)
    - [AP 2: Integration von BaSys 4 und WH4.0](#ap-2:-integration-von-basys-4-und-wh4.0)
    - [AP 3: Entwicklung eines DZ Wald](#ap-3:-entwicklung-eines-dz-wald)
    - [AP 4: Integration eines Waldmanagementsystems](#ap-4:-integration-eines-waldmanagementsystems)
    - [AP 5: Integration einer Holzhandelsplattform](#ap-5:-integration-einer-holzhandelsplattform)
    - [AP 6: Entwicklung einer Demo-Applikation für die Abnehmerseite](#ap-6:-entwicklung-einer-demo-applikation-für-die-abnehmerseite)
    - [AP 7: Integration Holzaufnahme-App](#ap-7:-integration-holzaufnahme-app)
    - [AP 8: Integration, Demonstration und Dokumentation der Ergebnisse](#ap-8:-integration,-demonstration-und-dokumentation der ergebnisse)

## Allgemeine Infos
- **Zeitraum:** 01.10.2021 - 30.09.2023
- **Projektträger:** Das Deutsche Zentrum für Luft- und Raumfahrt e.V. (DLR)
    - **Ansprechpersonen:** 
        - Maren Dietrich, +49-30-67055-8102, maren.dietrich@dlr.de
        - Inga Stumpp, +49-30-67055-8134, inga.stumpp@dlr.de

    
## Kommunikation
### Kontaktdaten Projektpartner
- **RWTH:**
    - **PLT:** www.plt.rwth-aachen.de/
        - Igor Garmaev, +49 241 80-97745, i.garmaev@plt.rwth-aachen.de
    - **MMI:** www.wzl.rwth-aachen.de
        - Martin Hoppen, +49 241 80-26105, hoppen@mmi.rwth-aachen.de
    - **WZL:** www.mmi.rwth-aachen.de
        - Christian Fimmers, +49 241 80 28232, c.fimmers@wzl.rwth-aachen.de
- **Weichs Beratungs- und Dienstleistungsgesellschaft:** 
    - Markus von Weichs, weichs.ug@t-online.de
- **rBITech:**
    - Birgit Oylum, birgit.oylum@rbitech.de
- **Fortify:**
    - Christian Kaulich, c.kaulich@forstify.de
    
### Mailingsliste
- **Adresse:** [basys4forestry@lists.rwth-aachen.de](mailto:basys4forestry@lists.rwth-aachen.de)
- **Info:** Alle Projektpartner bekommen ein E-Mail, das Sie an diese Adresse senden
- **Wofür:** Benachrichtigung aller Projektpartnern über das eher **wichtige Info**
- **Verantwortlich:** PLT (Igor Garmaev, [i.garmaev@plt.rwth-aachen.de](mailto:i.garmaev@plt.rwth-aachen.de))

Noch kein Mitglied der Mailingsliste? [Mitglied werden](https://lists.rwth-aachen.de/postorius/lists/basys4forestry.lists.rwth-aachen.de/).

### Microsoft Teams
- **BaSys4Forestry-Team:** [Link zum Team](https://teams.microsoft.com/l/team/19%3a-ME4VEw9zYsNXJ1-Hf1idnMhmIFjTCRFe_EUK7DRu2s1%40thread.tacv2/conversations?groupId=abaf9e09-4e99-4bd6-b71f-a342c2ad4f1b&tenantId=571aa7c2-163d-4fbf-96b9-f626c479b7f8) 
- **Wofür:** Telkos, Chat-Nachrichten (Chat-Nachrichten im Team sollen für weniger wichtiges Info benutzt werden)
- **Verantwortlich:** PLT (Igor Garmaev, [i.garmaev@plt.rwth-aachen.de](mailto:i.garmaev@plt.rwth-aachen.de))

### Regel-Telkos
- **Zeit:** Jeden ersten Donnerstag des Monats um 13 Uhr
- **Dauer:** 1 Stunde
- **Ort:** Microsoft Teams

### SharePoint
- **SharePoint:** [Link zu SharePoint](https://sharepoint.rif-ev.de/rt/projects/basys4forestry/Dokumente/)
- **Verantwortlich:** MMI (Martin Hoppen, [hoppen@mmi.rwth-aachen.de](mailto:hoppen@mmi.rwth-aachen.de))

## GitLab
Das GitLab-Projekt wird für das Projektmanagement benutzt. Vor allem werden issues als Hauptwerkzeug benutzt.
- **Wofür:** Projektmanagement
- **Verantwortlich:** PLT (Igor Garmaev, [i.garmaev@plt.rwth-aachen.de](mailto:i.garmaev@plt.rwth-aachen.de))
### Issues
Beim Anlegen eines neuen Issue können Sie einen Verantwortlicher (Assignee) auswählen, den Frist (Due date) und Arbeitspaket (Milestone)

### Boards


## Arbeitspakete
### Übersicht Bakenplan
![](/balkenplan.PNG?raw=true "Balkenplan")

### AP 0: Projektmanagement und Dissemination
- **AP-Verantwortlich:** PLT
- **Weitere Beteiligte:** MMI, WZL
- **Zielstellung:** Sicherstellen des erfolgreichen Projektablaufs und Transfer der Ergebnisse in die Wirtschaft, sowie Sicherstellen einer nachhaltigen Verwertung zugunsten der deutschen Wirtschaft mit dem Fokus auf KMU
- **Vorgehen:**
    - Aufsetzen von Projektkommunikationsstrukturen, Koordination von Konsortialtreffen, Regelung der Zusammenarbeit 
    - Veröffentlichung der Ergebnisse in Fachzeitschriften, auf Konferenzen und Messen 
    - Öffentlichen Zugang zu den Anwendungsergebnissen • Know-How-Transfer von BaSys 4 und WH4.0 an Projektpartner (z.B. Workshops)
- **Ergebnis:** Erfolgreich verlaufendes Projekt mit öffentlicher Wirkung

### AP 1: Definition der Anwendungsszenarien
- **AP-Verantwortlich:** WZL
- **Weitere Beteiligte:** ALLE
- **Zielstellung:** Detaillierte Definition der Anwendungsszenarien zur Durchführung der weiteren Arbei-ten
- **Vorgehen:**
    - Präzision der beiden Anwendungsszenarien:
        - Szenario 1: Waldbesitzer erfasst Waldbestand; Waldbesitzer lässt Maßnahme durch-führen (Holz fällen); Waldbesitzer lässt Holz erfassen; Waldbesitzer bietet Holz zum Kauf an; Holzeinkäufer kauft Holz
        - Szenario 2: Auf Basis historischen Daten zu Holzverkäufen der Vorjahre stellt ein Ser-vice dem Waldbesitzer Informationen bereit, welche Maßnahmen wirtschaftlich sinn-voll sind.
    - Gemeinsame Workshops mit Anwendern, Integratoren und wiss. Partnern zur Definition der User Storys
    - Ableitung der funktionalen und nicht-funktionalen Anforderungen aus den User-Stories
    - Detaillierte Beschreibung jedes Schrittes, um später das Ableiten der notwendigen Informatio-nen zu ermöglichen
- **Ergebnis:** Ausführlich beschriebene Anwendungsszenarien und User-Stories zur Orientierung und Validierung

### AP 2: Integration von BaSys 4 und WH4.0
- **AP-Verantwortlich:** MMI 
- **Weitere Beteiligte:** PLT
- **Zielstellung:** Abgleich und Harmonisierung der Entwicklungen aus den BaSys-Projekten und WH4.0. WH4.0 basiert bereits auf den Konzepten und Methoden von Industrie 4.0. Dieses AP soll die Grundlage für eine Zusammenführung der aktuellen Entwicklungen schaffen, so dass produzierende Industrie und Wald- und Holzwirtschaft wechselseitig voneinander profitieren. 
- **Vorgehen:** 
    - Abgleich und der Harmonisierung der maßgeblichen Konzepte Digitaler Zwillinge nach WH4.014 und Verwaltungsschale nach VWSiD 
    - Konzeptentwicklung für die Überführung der Forest Modeling Language 4.015 auf das Meta-modell der I4.0-Verwaltungsschale16 
    - Abgleich und Harmonisierung der Konzepte und Methoden der Smart Systems Service Infra-structure (S3I)17 mit der BaSys 4-Infrastruktur 
- **Ergebnis:** Abgeglichene und harmonisierte Konzepte und Technologien für BaSys 4 und WH4.0, dokumentiert und veröffentlicht in einem Standpunkt-Papier „BaSys 4 und WH4.0“.

### AP 3: Entwicklung eines DZ Wald
- **AP-Verantwortlich:** MMI
- **Weitere Beteiligte:** ALLE 
- **Zielstellung:** Entwicklung eines BaSys 4- und WH4.0-konformen Digitalen Zwillings Wald gemäß Anforderungen der Anwendungsszenarien. 
- **Vorgehen:** 
    - Spezifikation der Anforderungen an einen DZ Wald für die betrachteten Szenarien (notwen-dige Merkmale, Submodelle, Datenformate, Operationen …) gemäß AP 1 
    - Definition der Detailebene für den DZ-Wald (Waldeinheit, Baum …) auf Basis der Anforderun-gen • Entwicklung einer Verwaltungsschale mit geeigneten Submodellen für einen solchen DZ Wald auf Grundlage von AP 2 
    - Prototypische Umsetzung des DZ Wald mit BaSys 4- und WH4.0-Technologien 
- **Ergebnis:** Konzept und prototypische Umsetzung eines DZ Wald.

### AP 4: Integration eines Waldmanagementsystems
- **AP-Verantwortlich:** FM 
- **Weitere Beteiligte:** PLT
- **Zielstellung:** Entwicklung eines BaSys 4- und WH4.0-konformen Software-Services zur Integration eines Waldmanagementsystems gemäß Anforderungen der Anwendungsszenarien am Beispiel ForestManager. 
- **Vorgehen:** 
    - Spezifikation der Anforderungen an Services für das Waldmanagement im Hinblick auf die betrachteten Szenarien aus AP 1 (notwendige Merkmale, Submodelle, Datenformate, Opera-tionen …) 
    - Definition der notwendigen Services und einer Service-Struktur
    - Entwicklung einer Verwaltungsschale mit geeigneten Submodellen für diese Services auf Grundlage von AP 2 
    - Prototypische Umsetzung eines Service für den ForestManager mit BaSys 4- und WH4.0-Technologien 
- **Ergebnis:** Konzept und prototypische Umsetzung eines Service für die Integration des Wald-managementsystems ForestManager.

### AP 5: Integration einer Holzhandelsplattform
- **AP-Verantwortlich:** FY 
- **Weitere Beteiligte:** PLT
- **Zielstellung:** Entwicklung eines BaSys 4- und WH4.0-konformen Software-Services zur Integration einer Holzhandelsplattform gemäß Anforderungen der Anwendungsszenarien am Bei-spiel Forstify. 
- **Vorgehen:** 
    - Spezifikation der Anforderungen an die Services für das Szenario Holzvermarktung (notwen-dige Merkmale, Submodelle, Datenformate, Operationen …) gemäß AP 1 
    - Spezifikation der Anforderungen an einen Service für das Szenario optimierter Holzeinschlag (notwendige Merkmale, Submodelle, Datenformate, Operationen, Eingangsdaten …) gemäß AP 1 
    - Entwicklung einer Verwaltungsschale mit geeigneten Submodellen für diese Services auf Grundlage von AP 2 
    - Prototypische Umsetzung eines Holzvermarktungs-Service für Forstify mit BaSys 4- und WH4.0-Technologien 
    - Prototypische Umsetzung eines Service für den optimierten Holzeinschlag auf Basis histori-scher Verkaufsdaten 
- **Ergebnis:** Konzept und prototypische

### AP 6: Entwicklung einer Demo Applikation für die Abnehmerseite
- **AP-Verantwortlich:** FY
- **Weitere Beteiligte:** WE
- **Zielstellung:** Entwicklung einer Demo-Applikation (App) im Sinne einer BaSys 4- und WH4.0-kon-formen Mensch-Maschine-Schnittstelle für die Abnehmerseite. Die Applikation soll stellvertretend für echte Kundensysteme wie beispielsweise InfoData (eingesetzt durch Weichs beim Sägewerk Hegener-Hachmann) stehen, die an den Dienst (AP 5) der Holzhandelsplattform angebunden werden können. 
- **Vorgehen:** 
    - Spezifikation der Anforderungen an eine solche App für das betrachtete Szenario (notwendige Merkmale, Submodelle, Datenformate, Operationen …) gemäß AP 1 
    - Entwicklung einer Verwaltungsschale mit geeigneten Submodellen für diese App auf Grund-lage von AP 2 
    - Entwurf einer intuitiven Mensch-Maschine-Schnittstelle zum Zugriff auf die bereitgestellten Services 
    - Prototypische Umsetzung einer solchen App mit Anbindung an den Forstify-Dienst mit BaSys 4- und WH4.0-Technologien 
- **Ergebnis:** Konzept und prototypische Umsetzung einer Demo-Applikation für die Abnehmer-seite.

### AP 7: Integration Holzaufnahme App
- **AP-Verantwortlich:** FY 
- **Weitere Beteiligte:** FM 
- **Zielstellung:** Integration einer bestehenden App zur Aufnahme von geschlagenem Holz in das BaSys4Forestry-System im Sinne einer BaSys 4- und WH4.0-konformen Mensch-Maschine-Schnittstelle. 
- **Vorgehen:**
    - Spezifikation der Anforderungen an eine solche App für das betrachtete Szenario (notwendige Merkmale, Submodelle, Datenformate, Operationen …) gemäß AP 1 
    - Entwicklung einer Verwaltungsschale mit geeigneten Submodellen für diese App auf Grund-lage von AP 2
    - Entwurf einer intuitiven Mensch-Maschine-Schnittstelle zum Zugriff auf die bereitgestellten Services 
    - Prototypische Anbindung der Forstify Holzaufnahme-App mit BaSys 4- und WH4.0-Technolo-gien 
- **Ergebnis:** Konzept und prototypische Umsetzung einer Anbindung der Forstify Holzaufnahme-App.

### AP 8: Integration, Demonstration und Dokumentation der Ergebnisse
